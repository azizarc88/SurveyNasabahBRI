﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.BHasilSurvey = New System.Windows.Forms.Button()
        Me.BPengolahPertanyaan = New System.Windows.Forms.Button()
        Me.BKembali = New System.Windows.Forms.Button()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'BHasilSurvey
        '
        Me.BHasilSurvey.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BHasilSurvey.BackColor = System.Drawing.Color.White
        Me.BHasilSurvey.CausesValidation = False
        Me.BHasilSurvey.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BHasilSurvey.FlatAppearance.BorderSize = 2
        Me.BHasilSurvey.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BHasilSurvey.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BHasilSurvey.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BHasilSurvey.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BHasilSurvey.Location = New System.Drawing.Point(450, 431)
        Me.BHasilSurvey.Name = "BHasilSurvey"
        Me.BHasilSurvey.Size = New System.Drawing.Size(183, 68)
        Me.BHasilSurvey.TabIndex = 9
        Me.BHasilSurvey.Text = "Hasil Survei ^"
        Me.BHasilSurvey.UseVisualStyleBackColor = False
        '
        'BPengolahPertanyaan
        '
        Me.BPengolahPertanyaan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BPengolahPertanyaan.BackColor = System.Drawing.Color.White
        Me.BPengolahPertanyaan.CausesValidation = False
        Me.BPengolahPertanyaan.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BPengolahPertanyaan.FlatAppearance.BorderSize = 2
        Me.BPengolahPertanyaan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BPengolahPertanyaan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BPengolahPertanyaan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BPengolahPertanyaan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BPengolahPertanyaan.Location = New System.Drawing.Point(246, 431)
        Me.BPengolahPertanyaan.Name = "BPengolahPertanyaan"
        Me.BPengolahPertanyaan.Size = New System.Drawing.Size(183, 68)
        Me.BPengolahPertanyaan.TabIndex = 10
        Me.BPengolahPertanyaan.Text = "Pengolah Pertanyaan >"
        Me.BPengolahPertanyaan.UseVisualStyleBackColor = False
        '
        'BKembali
        '
        Me.BKembali.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BKembali.BackColor = System.Drawing.Color.Gainsboro
        Me.BKembali.CausesValidation = False
        Me.BKembali.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BKembali.FlatAppearance.BorderSize = 2
        Me.BKembali.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BKembali.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BKembali.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BKembali.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BKembali.Location = New System.Drawing.Point(22, 514)
        Me.BKembali.Name = "BKembali"
        Me.BKembali.Size = New System.Drawing.Size(183, 68)
        Me.BKembali.TabIndex = 11
        Me.BKembali.Text = "< Kembali"
        Me.BKembali.UseVisualStyleBackColor = False
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(13, 6)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(955, 81)
        Me.LJudul.TabIndex = 7
        Me.LJudul.Text = "PENGOLAH DATA"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.LJudul.Visible = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Moire", 25.0!)
        Me.Label1.ForeColor = System.Drawing.Color.LightGray
        Me.Label1.Location = New System.Drawing.Point(13, 14)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(955, 488)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "SILAHKAN PILIH MENU DIBAWAH INI"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label1.Visible = False
        '
        'Form_PData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(981, 508)
        Me.Controls.Add(Me.BHasilSurvey)
        Me.Controls.Add(Me.BPengolahPertanyaan)
        Me.Controls.Add(Me.BKembali)
        Me.Controls.Add(Me.LJudul)
        Me.Controls.Add(Me.Label1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_PData"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form_PData"
        Me.TopMost = True
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BHasilSurvey As System.Windows.Forms.Button
    Friend WithEvents BPengolahPertanyaan As System.Windows.Forms.Button
    Friend WithEvents BKembali As System.Windows.Forms.Button
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
