﻿Imports SkinApp.Gambar
Imports AnimasiUI.Class_Animasi
Imports MessageHandle
Imports System.Runtime.CompilerServices

Public Class Form_Tambah_Pertanyaan
    Public ID As String
    Public BelumBerubah As List(Of String) = New List(Of String)
    Public Teks As String()
    Dim b As Integer
    Public pesanwajar As String
    Public jawabandobel As String
    Dim cur As Integer = 0
    Dim done As Boolean = False

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Public Function GenerateID(ByVal last As String) As String
        Dim hasil As String
        If Form_PData_Pertanyaan._Pertanyaan.Count = 0 Then
            hasil = "00001"
            Return hasil
        Else
            Dim has As Integer = CInt(last)
            Dim has2 As String = has + 1
            Dim temp As String = "EROR"
            If has2.Length = 1 Then
                temp = "0000"
            ElseIf has2.Length = 2 Then
                temp = "000"
            ElseIf has2.Length = 3 Then
                temp = "00"
            ElseIf has2.Length = 4 Then
                temp = "0"
            ElseIf has2.Length = 2 Then
                temp = ""
            End If
            hasil = temp + has2
        End If
        Return hasil
    End Function

    Private Function Kapitalkan(teks As String) As String
        Dim hasil As String = ""
        Dim pertama As Boolean = True
        Dim besar As Boolean = False
        For Each kar As Char In teks
            If pertama Then
                hasil += kar.ToString.ToUpper
                pertama = False
                Continue For
            Else
                If kar = " " Then
                    hasil += kar
                    besar = True
                    Continue For
                End If
                If besar = True Then
                    hasil += kar.ToString.ToUpper
                    besar = False
                Else
                    hasil += kar.ToString.ToLower
                End If
            End If
        Next
        Return hasil
    End Function

    Private Function GetNilai(teks As String) As Integer
        Dim nilai As Integer = 15
        If Not teks.ToLower.Contains("tidak") Then
            nilai = 20
            If teks.ToLower.Contains("sangat") Then
                nilai = 25
            End If
        ElseIf teks.ToLower.Contains("buruk") Or teks.ToLower.Contains("cukup") Or teks.ToLower.Contains("tidak") Then
            nilai = 10
            If teks.ToLower.Contains("sangat") Then
                nilai = 5
            End If
        End If

        Return nilai
    End Function

    Private Function CekDobel() As Boolean
        Dim indeks As Integer = 0
        Dim indeks2 As Integer = 0
        For Each item As String In TBPilihanJ.Lines
            indeks2 = 0
            For Each item2 As String In TBPilihanJ.Lines
                If item = item2 And Not indeks2 = indeks Then
                    jawabandobel = item
                    Return False
                End If
                indeks2 += 1
            Next
            indeks += 1
        Next
        Return True
    End Function

    Private Function CekWajar() As Boolean
        Dim nilaiawal As Integer
        Dim cur As Integer = 25

        If Integer.TryParse(TBPilihanJ.Lines(0).ToLower, nilaiawal) = False Then
            For Each item As String In TBPilihanJ.Lines
                If GetNilai(item) > cur Then
                    pesanwajar = item.ToString + " | " + GetNilai(item).ToString + " - " + cur.ToString
                    Return False
                    Exit Function
                End If
                cur = GetNilai(item)
            Next
        Else
            Dim now, skr As Integer
            Dim pertama As Boolean = True
            For Each baris As String In TBPilihanJ.Lines
                If pertama Then
                    Integer.TryParse(baris, now)
                    pertama = False
                Else
                    Integer.TryParse(baris, skr)
                    If skr > now Then
                        Return False
                    End If
                End If
                Integer.TryParse(baris, now)
            Next
        End If
        Return True
    End Function

    Public Function TambahDataPertanyaan() As Boolean
        Dim tbl As List(Of String) = New List(Of String)
        For Each l As String In TBPilihanJ.Lines
            If Not l = "" Then
                tbl.Add(Kapitalkan(l))
            End If
        Next

        If tbl.Count < 2 Then
            NotificationManager.Show(Me, "Harap isi minimal 2 item pada bagian Pilihan Jawaban", Color.Orange, 2000)
            TBPilihanJ.Focus()
            Return False
        End If

        Form_PData_Pertanyaan._Pertanyaan.Add({TBPertanyaan.Text, ID})
        Form_PData_Pertanyaan._PilJawaban.Add(tbl.ToArray)
        Return True
    End Function

    Public Function UbahDataPertanyaan() As Boolean
        Dim tbl As List(Of String) = New List(Of String)
        Dim b As Boolean = False
        Dim a As Integer = 0
        Dim ada As Boolean = False
        For Each l As String In TBPilihanJ.Lines
            If l = BelumBerubah(a) = False Then
                b = True
            End If

            If Not l = "" Then
                tbl.Add(l)
            End If
            a += 1
        Next

        If tbl.Count < 2 Then
            NotificationManager.Show(Me, "Harap isi minimal 2 item pada bagian Pilihan Jawaban", Color.Orange, 2000)
            TBPilihanJ.Focus()
            Return False
        End If

        Try
            Dim daftar As New IO.DirectoryInfo(AlamatAppData + "\Jawaban")
            Dim daftarjawaban As IO.FileInfo() = daftar.GetFiles("*.jb")
            For Each berkas As IO.FileInfo In daftarjawaban
                If berkas.Name.Replace(".jb", "") = ID Then
                    ada = True
                    Exit For
                End If
            Next
        Catch ex As Exception
        End Try

        If b And ada = True Then
            If KotakPesan.Show(Me, "Apa Anda yakin ingin menyimpan perubahan?, pertanyaan yang jawabanya Anda rubah di sini sudah memiliki data jawaban dari partisipan." & vbNewLine & "Jika pilihan jawaban pertanyaan tersebut diubah, maka data jawaban dari partisipan akan ikut terhapus." & vbNewLine & "Ingin melanjutkan?", "Terdapat Data Partisipan", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Peringatan) = KotakPesan.HasilPesan.Tidak Then
                Return False
            End If
            Form_PData_Pertanyaan.daftarhapusberubah.Add(ID)
        End If

        Form_PData_Pertanyaan._Pertanyaan.Item(Form_PData_Pertanyaan.LBPertanyaan.SelectedIndex)(0) = TBPertanyaan.Text
        Form_PData_Pertanyaan._PilJawaban.Item(Form_PData_Pertanyaan.LBPertanyaan.SelectedIndex) = tbl.ToArray

        Return True
    End Function

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub Form_Tambah_Pertanyaan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
        Form_PData_Pertanyaan.Focus()
        Form_PData_Pertanyaan.NonAktifkan(Form_PData_Pertanyaan.BUbah)
        Form_PData_Pertanyaan.NonAktifkan(Form_PData_Pertanyaan.BHapus)
    End Sub

    Private Sub Form_Tambah_Pertanyaan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        BTutupOtomatis.Image = AmbilGambar("centang")
        BBatal.Focus()
        If Not BTambah.Text.Contains("Ubah") Then
            TBPertanyaan.ForeColor = Color.LightGray
            TBPertanyaan.Text = "Masukkan pertanyaan di sini"
            done = False
            TBPilihanJ.ForeColor = Color.LightGray
            TBPilihanJ.Text = "Masukkan pilihan jawaban di sini, dipisahkan dengan enter."
        End If
        done = True
    End Sub

    Public Function ReplaceEnterKeyword(ByVal Value As String, Optional ByVal CharSubstitution As String = "") As String
        If Not String.IsNullOrEmpty(Value) Then
            With Value
                Value = .Replace(Microsoft.VisualBasic.ControlChars.CrLf, CharSubstitution) _
                        .Replace(Microsoft.VisualBasic.ControlChars.Cr, CharSubstitution) _
                        .Replace(Microsoft.VisualBasic.ControlChars.Lf, CharSubstitution)
            End With
        End If
        Return (Value)
    End Function

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        Dim a As String = TBPertanyaan.Text
        TBPertanyaan.Text = ReplaceEnterKeyword(a, ", ")
        If TBPertanyaan.Text = "" Then
            NotificationManager.Show(Me, "Harap isi pada bagian Pertanyaan", Color.Orange, 2000)
            TBPertanyaan.Focus()
            Exit Sub
        ElseIf TBPilihanJ.Text = "" Or TBPilihanJ.Lines.Length < 2 Then
            NotificationManager.Show(Me, "Harap isi minimal 2 item pada bagian Pilihan Jawaban", Color.Orange, 2000)
            TBPilihanJ.Focus()
            Exit Sub
        End If

        If CekWajar() = False Then
            KotakPesan.Show(Me, "Harap masukan pilihan jawaban dari yang terbesar ke terkecil." + Environment.NewLine + pesanwajar, "Salah Susunan Jawaban", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            Exit Sub
        End If

        If CekDobel() = False Then
            KotakPesan.Show(Me, "Kami mendeteksi bahwa Anda memasukkan pilihan jawaban yang sama satu sama lain, harap diubah dahulu." + Environment.NewLine + """" + jawabandobel + """", "Pilihan Jawaban Dobel", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
            Exit Sub
        End If

        If LJudul.Text.Contains("Tambah") Then
            If TambahDataPertanyaan() = False Then
                Exit Sub
            End If
        Else
            If UbahDataPertanyaan() = False Then
                Exit Sub
            End If
        End If

        Form_PData_Pertanyaan.MuatData()
        Form_PData_Pertanyaan.berubah = True
        If BTutupOtomatis.Checked = True Then
            Me.Close()
        Else
            ID = GenerateID(Form_PData_Pertanyaan._Pertanyaan(Form_PData_Pertanyaan._Pertanyaan.Count - 1)(1))
            NotificationManager.Show(Me, "Pertanyaan baru telah ditambahkan", Color.Orange, 2000)
        End If
    End Sub

    Private Sub BTutupOtomatis_CheckedChanged(sender As Object, e As EventArgs) Handles BTutupOtomatis.CheckedChanged
        If BTutupOtomatis.Checked = True Then
            BTutupOtomatis.Image = AmbilGambar("centang")
        Else
            BTutupOtomatis.Image = AmbilGambar("uncentang")
        End If
    End Sub

    Private Sub TBPilihanJ_GotFocus(sender As Object, e As EventArgs) Handles TBPilihanJ.GotFocus
        If cur = 5 Then
            Teks = TBPilihanJ.Lines
        End If
    End Sub

    Private Sub TBPilihanJ_KeyUp(sender As Object, e As KeyEventArgs) Handles TBPilihanJ.KeyUp
        If e.KeyData = System.Windows.Forms.Keys.Enter Then
            If cur = 5 Then
                Teks = TBPilihanJ.Lines
            End If
        End If
    End Sub

    Private Sub TBPilihanJ_MouseClick(sender As Object, e As EventArgs) Handles TBPilihanJ.GotFocus
        If TBPilihanJ.ForeColor = Color.LightGray Then
            TBPilihanJ.Text = ""
            TBPilihanJ.ForeColor = Color.Black
        End If
    End Sub

    Private Sub TBPilihanJ_TextChanged(sender As Object, e As EventArgs) Handles TBPilihanJ.TextChanged
        Dim c As Integer = 0
        Dim a As Integer
        For Each s As String In TBPilihanJ.Lines
            If s.Length > 0 Then
                c += 1
                cur = c
            End If
            a += 1
        Next

        If c > 5 Then
            TBPilihanJ.Lines = Teks
            TBPilihanJ.Select(TBPilihanJ.TextLength, 0)
            TBPilihanJ.ScrollToCaret()
            NotificationManager.Show(Me, "Jumlah Jawaban maksimal 5", Color.Orange, 3000)
            c = 5
        End If
        If done And c >= 1 Then
            LJumlah.Text = c & " Jawaban"
        End If
        If c = 0 Then
            LJumlah.Text = ""
        End If
    End Sub

    Private Sub Form_Tambah_Pertanyaan_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        If BTambah.Text.Contains("Ubah") Then
            For Each item As String In TBPilihanJ.Lines
                BelumBerubah.Add(item)
            Next
        End If
    End Sub

    Private Sub TBPertanyaan_TextChanged(sender As Object, e As EventArgs) Handles TBPertanyaan.GotFocus
        If TBPertanyaan.ForeColor = Color.LightGray Then
            TBPertanyaan.Text = ""
            TBPertanyaan.ForeColor = Color.Black
        End If
    End Sub

    Private Sub TBPertanyaan_LostFocus(sender As Object, e As EventArgs) Handles TBPertanyaan.LostFocus
        If TBPertanyaan.Text = "" Then
            TBPertanyaan.ForeColor = Color.LightGray
            TBPertanyaan.Text = "Masukkan pertanyaan di sini"
        End If
    End Sub

    Private Sub TBPilihanJ_LostFocus(sender As Object, e As EventArgs) Handles TBPilihanJ.LostFocus
        If TBPilihanJ.Text = "" Then
            done = False
            TBPilihanJ.ForeColor = Color.LightGray
            TBPilihanJ.Text = "Masukkan pilihan jawaban di sini, dipisahkan dengan enter."
            done = True
        End If
    End Sub

    Private Sub BTambah_EnabledChanged(sender As Object, e As EventArgs) Handles BTambah.EnabledChanged
        Me.Close()
    End Sub

    Private Sub TBPertanyaan_Click(sender As Object, e As EventArgs) Handles TBPertanyaan.Click, TBPilihanJ.Click
        Form_Back.osk.namaform = Me
        Form_Back.osk.Tampilkan()
    End Sub
End Class