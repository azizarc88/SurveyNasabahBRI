﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Report
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Report))
        Me.LJudul = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LBPertanyaan = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BHapusS = New System.Windows.Forms.Button()
        Me.BHapus = New System.Windows.Forms.Button()
        Me.BTambah = New System.Windows.Forms.Button()
        Me.BTambahS = New System.Windows.Forms.Button()
        Me.LBPertanyaanT = New System.Windows.Forms.ListBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.LPertanyaan = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BProses = New System.Windows.Forms.Button()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.DTDari = New System.Windows.Forms.DateTimePicker()
        Me.DTSampai = New System.Windows.Forms.DateTimePicker()
        Me.PHide = New System.Windows.Forms.Panel()
        Me.LUtama = New System.Windows.Forms.Label()
        Me.LKeterangan = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.GroupBox1.SuspendLayout()
        Me.PHide.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(16, 12)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(711, 47)
        Me.LJudul.TabIndex = 10
        Me.LJudul.Text = "BUAT LAPORAN"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label14
        '
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label14.Location = New System.Drawing.Point(24, 87)
        Me.Label14.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(155, 40)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Dari Tanggal:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label1.Location = New System.Drawing.Point(24, 123)
        Me.Label1.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(187, 40)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Sampai Tanggal:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LBPertanyaan
        '
        Me.LBPertanyaan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.LBPertanyaan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.LBPertanyaan.FormattingEnabled = True
        Me.LBPertanyaan.ItemHeight = 23
        Me.LBPertanyaan.Location = New System.Drawing.Point(10, 61)
        Me.LBPertanyaan.Name = "LBPertanyaan"
        Me.LBPertanyaan.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBPertanyaan.Size = New System.Drawing.Size(301, 188)
        Me.LBPertanyaan.TabIndex = 17
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BHapusS)
        Me.GroupBox1.Controls.Add(Me.BHapus)
        Me.GroupBox1.Controls.Add(Me.BTambah)
        Me.GroupBox1.Controls.Add(Me.BTambahS)
        Me.GroupBox1.Controls.Add(Me.LBPertanyaanT)
        Me.GroupBox1.Controls.Add(Me.LBPertanyaan)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.LPertanyaan)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.GroupBox1.Location = New System.Drawing.Point(22, 167)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(705, 303)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pertanyaan"
        '
        'BHapusS
        '
        Me.BHapusS.BackColor = System.Drawing.Color.White
        Me.BHapusS.CausesValidation = False
        Me.BHapusS.Enabled = False
        Me.BHapusS.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.BHapusS.FlatAppearance.BorderSize = 2
        Me.BHapusS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BHapusS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BHapusS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BHapusS.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BHapusS.ForeColor = System.Drawing.Color.Teal
        Me.BHapusS.Location = New System.Drawing.Point(318, 207)
        Me.BHapusS.Margin = New System.Windows.Forms.Padding(4)
        Me.BHapusS.Name = "BHapusS"
        Me.BHapusS.Size = New System.Drawing.Size(69, 40)
        Me.BHapusS.TabIndex = 26
        Me.BHapusS.Text = "◄"
        Me.BHapusS.UseVisualStyleBackColor = False
        '
        'BHapus
        '
        Me.BHapus.BackColor = System.Drawing.Color.White
        Me.BHapus.CausesValidation = False
        Me.BHapus.Enabled = False
        Me.BHapus.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.BHapus.FlatAppearance.BorderSize = 2
        Me.BHapus.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BHapus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BHapus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BHapus.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BHapus.ForeColor = System.Drawing.Color.Teal
        Me.BHapus.Location = New System.Drawing.Point(318, 159)
        Me.BHapus.Margin = New System.Windows.Forms.Padding(4)
        Me.BHapus.Name = "BHapus"
        Me.BHapus.Size = New System.Drawing.Size(69, 40)
        Me.BHapus.TabIndex = 26
        Me.BHapus.Text = "<"
        Me.BHapus.UseVisualStyleBackColor = False
        '
        'BTambah
        '
        Me.BTambah.BackColor = System.Drawing.Color.White
        Me.BTambah.CausesValidation = False
        Me.BTambah.Enabled = False
        Me.BTambah.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.BTambah.FlatAppearance.BorderSize = 2
        Me.BTambah.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BTambah.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BTambah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTambah.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BTambah.ForeColor = System.Drawing.Color.Teal
        Me.BTambah.Location = New System.Drawing.Point(318, 111)
        Me.BTambah.Margin = New System.Windows.Forms.Padding(4)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Size = New System.Drawing.Size(69, 40)
        Me.BTambah.TabIndex = 26
        Me.BTambah.Text = ">"
        Me.BTambah.UseVisualStyleBackColor = False
        '
        'BTambahS
        '
        Me.BTambahS.BackColor = System.Drawing.Color.White
        Me.BTambahS.CausesValidation = False
        Me.BTambahS.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BTambahS.FlatAppearance.BorderSize = 2
        Me.BTambahS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BTambahS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BTambahS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTambahS.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BTambahS.ForeColor = System.Drawing.Color.Teal
        Me.BTambahS.Location = New System.Drawing.Point(318, 63)
        Me.BTambahS.Margin = New System.Windows.Forms.Padding(4)
        Me.BTambahS.Name = "BTambahS"
        Me.BTambahS.Size = New System.Drawing.Size(69, 40)
        Me.BTambahS.TabIndex = 26
        Me.BTambahS.Text = "►"
        Me.BTambahS.UseVisualStyleBackColor = False
        '
        'LBPertanyaanT
        '
        Me.LBPertanyaanT.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBPertanyaanT.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.LBPertanyaanT.FormattingEnabled = True
        Me.LBPertanyaanT.ItemHeight = 23
        Me.LBPertanyaanT.Location = New System.Drawing.Point(394, 60)
        Me.LBPertanyaanT.Name = "LBPertanyaanT"
        Me.LBPertanyaanT.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBPertanyaanT.Size = New System.Drawing.Size(301, 188)
        Me.LBPertanyaanT.TabIndex = 17
        '
        'Label3
        '
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label3.Location = New System.Drawing.Point(390, 20)
        Me.Label3.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(202, 40)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "Pertanyaan Terpilih:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LPertanyaan
        '
        Me.LPertanyaan.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LPertanyaan.BackColor = System.Drawing.Color.Transparent
        Me.LPertanyaan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.LPertanyaan.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.LPertanyaan.Location = New System.Drawing.Point(8, 255)
        Me.LPertanyaan.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.LPertanyaan.Name = "LPertanyaan"
        Me.LPertanyaan.Size = New System.Drawing.Size(689, 40)
        Me.LPertanyaan.TabIndex = 15
        Me.LPertanyaan.Text = "Tidak ada pertanyaan terpilih..."
        Me.LPertanyaan.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label2.Location = New System.Drawing.Point(8, 20)
        Me.Label2.Margin = New System.Windows.Forms.Padding(5, 0, 5, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(257, 40)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Pertanyaan Tersedia:"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'BProses
        '
        Me.BProses.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BProses.BackColor = System.Drawing.Color.White
        Me.BProses.CausesValidation = False
        Me.BProses.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BProses.FlatAppearance.BorderSize = 2
        Me.BProses.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BProses.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BProses.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BProses.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BProses.Location = New System.Drawing.Point(22, 485)
        Me.BProses.Margin = New System.Windows.Forms.Padding(4)
        Me.BProses.Name = "BProses"
        Me.BProses.Size = New System.Drawing.Size(204, 72)
        Me.BProses.TabIndex = 26
        Me.BProses.Text = "Proses Laporan"
        Me.BProses.UseVisualStyleBackColor = False
        '
        'BBatal
        '
        Me.BBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatal.BackColor = System.Drawing.Color.MistyRose
        Me.BBatal.CausesValidation = False
        Me.BBatal.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatal.FlatAppearance.BorderSize = 2
        Me.BBatal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatal.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatal.Location = New System.Drawing.Point(523, 485)
        Me.BBatal.Margin = New System.Windows.Forms.Padding(4)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(204, 72)
        Me.BBatal.TabIndex = 27
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = False
        '
        'DTDari
        '
        Me.DTDari.Location = New System.Drawing.Point(219, 93)
        Me.DTDari.Name = "DTDari"
        Me.DTDari.Size = New System.Drawing.Size(311, 27)
        Me.DTDari.TabIndex = 28
        '
        'DTSampai
        '
        Me.DTSampai.Location = New System.Drawing.Point(219, 129)
        Me.DTSampai.Name = "DTSampai"
        Me.DTSampai.Size = New System.Drawing.Size(311, 27)
        Me.DTSampai.TabIndex = 28
        '
        'PHide
        '
        Me.PHide.BackColor = System.Drawing.Color.SteelBlue
        Me.PHide.Controls.Add(Me.Button2)
        Me.PHide.Controls.Add(Me.Button1)
        Me.PHide.Controls.Add(Me.LKeterangan)
        Me.PHide.Controls.Add(Me.LUtama)
        Me.PHide.Location = New System.Drawing.Point(800, 0)
        Me.PHide.Name = "PHide"
        Me.PHide.Size = New System.Drawing.Size(743, 576)
        Me.PHide.TabIndex = 29
        Me.PHide.Visible = False
        '
        'LUtama
        '
        Me.LUtama.AutoSize = True
        Me.LUtama.Font = New System.Drawing.Font("Century Gothic", 30.0!)
        Me.LUtama.ForeColor = System.Drawing.Color.White
        Me.LUtama.Location = New System.Drawing.Point(144, 237)
        Me.LUtama.Name = "LUtama"
        Me.LUtama.Size = New System.Drawing.Size(463, 49)
        Me.LUtama.TabIndex = 0
        Me.LUtama.Text = "Memproses Laporan...."
        '
        'LKeterangan
        '
        Me.LKeterangan.AutoSize = True
        Me.LKeterangan.Font = New System.Drawing.Font("Century Gothic", 25.0!)
        Me.LKeterangan.ForeColor = System.Drawing.Color.White
        Me.LKeterangan.Location = New System.Drawing.Point(222, 291)
        Me.LKeterangan.Name = "LKeterangan"
        Me.LKeterangan.Size = New System.Drawing.Size(300, 40)
        Me.LKeterangan.TabIndex = 0
        Me.LKeterangan.Text = "Mohon tunggu...."
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button1.CausesValidation = False
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.Button1.FlatAppearance.BorderSize = 2
        Me.Button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.Button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Button1.ForeColor = System.Drawing.Color.White
        Me.Button1.Location = New System.Drawing.Point(13, 491)
        Me.Button1.Margin = New System.Windows.Forms.Padding(4)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(204, 72)
        Me.Button1.TabIndex = 27
        Me.Button1.Text = "< Kembali"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button2.BackColor = System.Drawing.Color.DodgerBlue
        Me.Button2.CausesValidation = False
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue
        Me.Button2.FlatAppearance.BorderSize = 2
        Me.Button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.Button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Button2.ForeColor = System.Drawing.Color.White
        Me.Button2.Location = New System.Drawing.Point(526, 492)
        Me.Button2.Margin = New System.Windows.Forms.Padding(4)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(204, 72)
        Me.Button2.TabIndex = 27
        Me.Button2.Text = "Buka Laporan dan Tutup Program"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-37, 572)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(816, 13)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 24
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-37, -9)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(816, 13)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 25
        Me.PictureBox1.TabStop = False
        '
        'Form_Report
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(743, 576)
        Me.Controls.Add(Me.PHide)
        Me.Controls.Add(Me.DTSampai)
        Me.Controls.Add(Me.DTDari)
        Me.Controls.Add(Me.BProses)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.LJudul)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_Report"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Report"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.PHide.ResumeLayout(False)
        Me.PHide.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LBPertanyaan As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LBPertanyaanT As System.Windows.Forms.ListBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents LPertanyaan As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents BHapusS As System.Windows.Forms.Button
    Friend WithEvents BHapus As System.Windows.Forms.Button
    Friend WithEvents BTambah As System.Windows.Forms.Button
    Friend WithEvents BTambahS As System.Windows.Forms.Button
    Friend WithEvents BProses As System.Windows.Forms.Button
    Friend WithEvents BBatal As System.Windows.Forms.Button
    Friend WithEvents DTDari As System.Windows.Forms.DateTimePicker
    Friend WithEvents DTSampai As System.Windows.Forms.DateTimePicker
    Friend WithEvents PHide As System.Windows.Forms.Panel
    Friend WithEvents LKeterangan As System.Windows.Forms.Label
    Friend WithEvents LUtama As System.Windows.Forms.Label
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
