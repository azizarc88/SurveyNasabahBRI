﻿Imports AnimasiUI.Class_Animasi

Public Class Form_PData
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Public Sub Inisialisasikan()
        BHasilSurvey.Location = New Point((Me.Size.Width / 2) - (BHasilSurvey.Size.Width / 2), Me.Size.Height)
        BPengolahPertanyaan.Location = New Point(Me.Size.Width - BPengolahPertanyaan.Size.Width - 20, Me.Size.Height)
    End Sub

    Private Sub Form_PData_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Horizontal_Kanan, OpsiGaya.Tutup)
        Form_MenuUtama.Focus()
        Me.Dispose()
    End Sub

    Private Sub Form_PData_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.BackColor = Color.White
        Me.Location = New Point(0, 0)
        Me.Size = Screen.PrimaryScreen.Bounds.Size
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Horizontal_Kiri, OpsiGaya.Buka)
        Inisialisasikan()
    End Sub

    Private Sub Form_PData_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Animasikan(LJudul.Handle, 500, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        GeserKontrol(BKembali, Me.Size.Height - (BKembali.Size.Height + 20), Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BHasilSurvey, Me.Size.Height - (BHasilSurvey.Size.Height + 20), Pengubah.Dikurangi, Orientasi.Vertikal)
        GeserKontrol(BPengolahPertanyaan, Me.Size.Height - (BPengolahPertanyaan.Size.Height + 20), Pengubah.Dikurangi, Orientasi.Vertikal)
        Animasikan(Label1.Handle, 500, GayaAnimasi.Tengah, OpsiGaya.Buka)
    End Sub

    Private Sub BKembali_MouseClick(sender As Object, e As MouseEventArgs) Handles BKembali.MouseClick
        Me.Close()
        If Pengaturan.animmenuutama = True Then
            Try
                Form_MenuUtama.thmain.Resume()
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub BPengolahPertanyaan_Click(sender As Object, e As EventArgs) Handles BPengolahPertanyaan.Click
        Form_PData_Pertanyaan.Show()
    End Sub

    Private Sub BHasilSurvey_Click(sender As Object, e As EventArgs) Handles BHasilSurvey.Click
        Form_PData_HasilSurvey.Show()
    End Sub
End Class
