﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PData_HasilSurvey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.DGVJawaban = New System.Windows.Forms.DataGridView()
        Me.CID_Pertanyaan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CPertanyaan = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CWaktu = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CJawaban = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.BTambah = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.LPil2 = New System.Windows.Forms.Label()
        Me.LJ1 = New System.Windows.Forms.Label()
        Me.LPil1 = New System.Windows.Forms.Label()
        Me.LJ5 = New System.Windows.Forms.Label()
        Me.LJ2 = New System.Windows.Forms.Label()
        Me.LPil5 = New System.Windows.Forms.Label()
        Me.LJ4 = New System.Windows.Forms.Label()
        Me.LPil3 = New System.Windows.Forms.Label()
        Me.LJ3 = New System.Windows.Forms.Label()
        Me.LPil4 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LPertanyaan = New System.Windows.Forms.Label()
        Me.BReport = New System.Windows.Forms.Button()
        Me.BAturUlang = New System.Windows.Forms.Button()
        CType(Me.DGVJawaban, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(13, 6)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(975, 81)
        Me.LJudul.TabIndex = 10
        Me.LJudul.Text = "PENGOLAH HASIL SURVEI"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'DGVJawaban
        '
        Me.DGVJawaban.AllowUserToAddRows = False
        Me.DGVJawaban.AllowUserToDeleteRows = False
        Me.DGVJawaban.AllowUserToResizeRows = False
        Me.DGVJawaban.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DGVJawaban.BackgroundColor = System.Drawing.Color.White
        Me.DGVJawaban.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.DGVJawaban.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText
        Me.DGVJawaban.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DGVJawaban.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.DGVJawaban.ColumnHeadersHeight = 50
        Me.DGVJawaban.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DGVJawaban.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CID_Pertanyaan, Me.CPertanyaan, Me.CWaktu, Me.CJawaban})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DGVJawaban.DefaultCellStyle = DataGridViewCellStyle2
        Me.DGVJawaban.EnableHeadersVisualStyles = False
        Me.DGVJawaban.Location = New System.Drawing.Point(20, 108)
        Me.DGVJawaban.Margin = New System.Windows.Forms.Padding(4)
        Me.DGVJawaban.Name = "DGVJawaban"
        Me.DGVJawaban.ReadOnly = True
        Me.DGVJawaban.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.DGVJawaban.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.[Single]
        Me.DGVJawaban.RowHeadersVisible = False
        Me.DGVJawaban.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.DGVJawaban.RowTemplate.Height = 28
        Me.DGVJawaban.RowTemplate.ReadOnly = True
        Me.DGVJawaban.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.DGVJawaban.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DGVJawaban.ShowCellErrors = False
        Me.DGVJawaban.ShowCellToolTips = False
        Me.DGVJawaban.ShowEditingIcon = False
        Me.DGVJawaban.ShowRowErrors = False
        Me.DGVJawaban.Size = New System.Drawing.Size(965, 396)
        Me.DGVJawaban.TabIndex = 11
        '
        'CID_Pertanyaan
        '
        Me.CID_Pertanyaan.FillWeight = 20.0!
        Me.CID_Pertanyaan.HeaderText = "ID Pertanyaan"
        Me.CID_Pertanyaan.Name = "CID_Pertanyaan"
        Me.CID_Pertanyaan.ReadOnly = True
        Me.CID_Pertanyaan.Width = 120
        '
        'CPertanyaan
        '
        Me.CPertanyaan.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CPertanyaan.FillWeight = 120.0!
        Me.CPertanyaan.HeaderText = "Pertanyaan"
        Me.CPertanyaan.Name = "CPertanyaan"
        Me.CPertanyaan.ReadOnly = True
        '
        'CWaktu
        '
        Me.CWaktu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CWaktu.FillWeight = 80.0!
        Me.CWaktu.HeaderText = "Waktu"
        Me.CWaktu.Name = "CWaktu"
        Me.CWaktu.ReadOnly = True
        '
        'CJawaban
        '
        Me.CJawaban.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.CJawaban.FillWeight = 50.0!
        Me.CJawaban.HeaderText = "Jawaban"
        Me.CJawaban.Name = "CJawaban"
        Me.CJawaban.ReadOnly = True
        '
        'BTambah
        '
        Me.BTambah.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTambah.BackColor = System.Drawing.Color.Gainsboro
        Me.BTambah.CausesValidation = False
        Me.BTambah.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BTambah.FlatAppearance.BorderSize = 2
        Me.BTambah.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BTambah.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BTambah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTambah.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BTambah.Location = New System.Drawing.Point(22, 661)
        Me.BTambah.Margin = New System.Windows.Forms.Padding(4)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Size = New System.Drawing.Size(183, 68)
        Me.BTambah.TabIndex = 12
        Me.BTambah.Text = " V Kembali"
        Me.BTambah.UseVisualStyleBackColor = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel2)
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Location = New System.Drawing.Point(12, 511)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(981, 130)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Statistik Pertanyaan Terpilih"
        '
        'Label14
        '
        Me.Label14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Location = New System.Drawing.Point(9, 59)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(963, 25)
        Me.Label14.TabIndex = 2
        Me.Label14.Text = "Pilihan Jawaban:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 10
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.03509!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.964912!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.03509!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.964912!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.03509!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.964912!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.03509!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.964912!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.03509!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.964912!))
        Me.TableLayoutPanel2.Controls.Add(Me.LPil2, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LJ1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LPil1, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LJ5, 7, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LJ2, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LPil5, 6, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LJ4, 5, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LPil3, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LJ3, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LPil4, 4, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(1, 93)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(978, 25)
        Me.TableLayoutPanel2.TabIndex = 1
        '
        'LPil2
        '
        Me.LPil2.BackColor = System.Drawing.Color.Transparent
        Me.LPil2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LPil2.Location = New System.Drawing.Point(198, 0)
        Me.LPil2.Name = "LPil2"
        Me.LPil2.Size = New System.Drawing.Size(131, 25)
        Me.LPil2.TabIndex = 2
        Me.LPil2.Text = " "
        Me.LPil2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LJ1
        '
        Me.LJ1.BackColor = System.Drawing.Color.Transparent
        Me.LJ1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LJ1.Location = New System.Drawing.Point(140, 0)
        Me.LJ1.Name = "LJ1"
        Me.LJ1.Size = New System.Drawing.Size(52, 25)
        Me.LJ1.TabIndex = 1
        Me.LJ1.Text = " "
        Me.LJ1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LPil1
        '
        Me.LPil1.BackColor = System.Drawing.Color.Transparent
        Me.LPil1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LPil1.Location = New System.Drawing.Point(3, 0)
        Me.LPil1.Name = "LPil1"
        Me.LPil1.Size = New System.Drawing.Size(131, 25)
        Me.LPil1.TabIndex = 0
        Me.LPil1.Text = " "
        Me.LPil1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LJ5
        '
        Me.LJ5.BackColor = System.Drawing.Color.Transparent
        Me.LJ5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LJ5.Location = New System.Drawing.Point(920, 0)
        Me.LJ5.Name = "LJ5"
        Me.LJ5.Size = New System.Drawing.Size(55, 25)
        Me.LJ5.TabIndex = 0
        Me.LJ5.Text = " "
        Me.LJ5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LJ2
        '
        Me.LJ2.AutoEllipsis = True
        Me.LJ2.BackColor = System.Drawing.Color.Transparent
        Me.LJ2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LJ2.Location = New System.Drawing.Point(335, 0)
        Me.LJ2.Name = "LJ2"
        Me.LJ2.Size = New System.Drawing.Size(52, 25)
        Me.LJ2.TabIndex = 0
        Me.LJ2.Text = " "
        Me.LJ2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LPil5
        '
        Me.LPil5.BackColor = System.Drawing.Color.Transparent
        Me.LPil5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LPil5.Location = New System.Drawing.Point(783, 0)
        Me.LPil5.Name = "LPil5"
        Me.LPil5.Size = New System.Drawing.Size(131, 25)
        Me.LPil5.TabIndex = 0
        Me.LPil5.Text = " "
        Me.LPil5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LJ4
        '
        Me.LJ4.BackColor = System.Drawing.Color.Transparent
        Me.LJ4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LJ4.Location = New System.Drawing.Point(725, 0)
        Me.LJ4.Name = "LJ4"
        Me.LJ4.Size = New System.Drawing.Size(52, 25)
        Me.LJ4.TabIndex = 0
        Me.LJ4.Text = " "
        Me.LJ4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LPil3
        '
        Me.LPil3.BackColor = System.Drawing.Color.Transparent
        Me.LPil3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LPil3.Location = New System.Drawing.Point(393, 0)
        Me.LPil3.Name = "LPil3"
        Me.LPil3.Size = New System.Drawing.Size(131, 25)
        Me.LPil3.TabIndex = 0
        Me.LPil3.Text = " "
        Me.LPil3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LJ3
        '
        Me.LJ3.BackColor = System.Drawing.Color.Transparent
        Me.LJ3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LJ3.Location = New System.Drawing.Point(530, 0)
        Me.LJ3.Name = "LJ3"
        Me.LJ3.Size = New System.Drawing.Size(52, 25)
        Me.LJ3.TabIndex = 0
        Me.LJ3.Text = " "
        Me.LJ3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LPil4
        '
        Me.LPil4.BackColor = System.Drawing.Color.Transparent
        Me.LPil4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LPil4.Location = New System.Drawing.Point(588, 0)
        Me.LPil4.Name = "LPil4"
        Me.LPil4.Size = New System.Drawing.Size(131, 25)
        Me.LPil4.TabIndex = 0
        Me.LPil4.Text = " "
        Me.LPil4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LPertanyaan, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1, 27)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(978, 25)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Location = New System.Drawing.Point(3, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(114, 25)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Pertanyaan:"
        '
        'LPertanyaan
        '
        Me.LPertanyaan.AutoEllipsis = True
        Me.LPertanyaan.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LPertanyaan.Location = New System.Drawing.Point(123, 0)
        Me.LPertanyaan.Name = "LPertanyaan"
        Me.LPertanyaan.Size = New System.Drawing.Size(852, 25)
        Me.LPertanyaan.TabIndex = 0
        Me.LPertanyaan.Text = "[Tidak ada pertanyaan yang terpilih]"
        '
        'BReport
        '
        Me.BReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BReport.BackColor = System.Drawing.Color.Gainsboro
        Me.BReport.CausesValidation = False
        Me.BReport.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BReport.FlatAppearance.BorderSize = 2
        Me.BReport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BReport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BReport.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BReport.Location = New System.Drawing.Point(802, 661)
        Me.BReport.Margin = New System.Windows.Forms.Padding(4)
        Me.BReport.Name = "BReport"
        Me.BReport.Size = New System.Drawing.Size(183, 68)
        Me.BReport.TabIndex = 12
        Me.BReport.Text = "Atur Laporan"
        Me.BReport.UseVisualStyleBackColor = False
        '
        'BAturUlang
        '
        Me.BAturUlang.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.BAturUlang.BackColor = System.Drawing.Color.RosyBrown
        Me.BAturUlang.CausesValidation = False
        Me.BAturUlang.Enabled = False
        Me.BAturUlang.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BAturUlang.FlatAppearance.BorderSize = 2
        Me.BAturUlang.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BAturUlang.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BAturUlang.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BAturUlang.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BAturUlang.ForeColor = System.Drawing.Color.Maroon
        Me.BAturUlang.Location = New System.Drawing.Point(411, 661)
        Me.BAturUlang.Margin = New System.Windows.Forms.Padding(4)
        Me.BAturUlang.Name = "BAturUlang"
        Me.BAturUlang.Size = New System.Drawing.Size(183, 68)
        Me.BAturUlang.TabIndex = 12
        Me.BAturUlang.Text = "ATUR ULANG"
        Me.BAturUlang.UseVisualStyleBackColor = False
        '
        'Form_PData_HasilSurvey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1005, 750)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.BReport)
        Me.Controls.Add(Me.BAturUlang)
        Me.Controls.Add(Me.BTambah)
        Me.Controls.Add(Me.DGVJawaban)
        Me.Controls.Add(Me.LJudul)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_PData_HasilSurvey"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form_PD_HasilSurvey"
        Me.TopMost = True
        CType(Me.DGVJawaban, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents DGVJawaban As System.Windows.Forms.DataGridView
    Friend WithEvents BTambah As System.Windows.Forms.Button
    Friend WithEvents CID_Pertanyaan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CPertanyaan As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CWaktu As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CJawaban As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LPil2 As System.Windows.Forms.Label
    Friend WithEvents LJ1 As System.Windows.Forms.Label
    Friend WithEvents LPil1 As System.Windows.Forms.Label
    Friend WithEvents LJ5 As System.Windows.Forms.Label
    Friend WithEvents LJ2 As System.Windows.Forms.Label
    Friend WithEvents LPil5 As System.Windows.Forms.Label
    Friend WithEvents LJ4 As System.Windows.Forms.Label
    Friend WithEvents LPil3 As System.Windows.Forms.Label
    Friend WithEvents LJ3 As System.Windows.Forms.Label
    Friend WithEvents LPil4 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LPertanyaan As System.Windows.Forms.Label
    Friend WithEvents BReport As System.Windows.Forms.Button
    Friend WithEvents BAturUlang As System.Windows.Forms.Button
End Class
