﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Pengaturan_Pemilik
    Dim open As Boolean = False
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Inisialisasi()
        TBNama.Text = Pengaturan.nama
        TBKeterangan.Text = Pengaturan.keterangan
        TBAlamat.Text = Pengaturan.alamat
        TBNoTelp.Text = Pengaturan.notelp
        TBEmail.Text = Pengaturan.email
        TBWebsite.Text = Pengaturan.website
    End Sub

    Private Sub Form_Pengaturan_Pemilik_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
        Form_Pengaturan.Focus()
    End Sub

    Private Sub Form_Pengaturan_Pemilik_Load(sender As Object, e As EventArgs) Handles Me.Load
        Inisialisasi()
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        If Form_MenuUtama.pengaturanfirst = True Then
            NotificationManager.Show(Me, "Tidak dapat membatalkan.", Color.Orange, 5000)
            Exit Sub
        End If
        Me.Close()
    End Sub

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BOk.Click
        If TBAlamat.Text = "" Or TBEmail.Text = "" Or TBNama.Text = "" Or TBNoTelp.Text = "" Or TBWebsite.Text = "" Then
            NotificationManager.Show(Me, "Isian belum lengkap, silakan isi kembali", Color.Orange, 5000)
            Exit Sub
        End If

        Pengaturan.nama = TBNama.Text
        Pengaturan.keterangan = TBKeterangan.Text
        Pengaturan.alamat = TBAlamat.Text
        Pengaturan.notelp = TBNoTelp.Text
        Pengaturan.email = TBEmail.Text
        Pengaturan.website = TBWebsite.Text
        Me.Close()
    End Sub

    Private Sub TBNama_TextChanged(sender As Object, e As EventArgs) Handles TBWebsite.Click, TBNoTelp.Click, TBNama.Click, TBKeterangan.Click, TBEmail.Click, TBAlamat.Click
        Form_Back.osk.namaform = Me
        Form_Back.osk.Tampilkan()
    End Sub
End Class