﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Pengaturan_Survey
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Inisialisasi()
        CBTOtoPass.Checked = Pengaturan.ototutuppass
        CBTOtoSurvey.Checked = Pengaturan.ototutupsurvey
        CBTrans.SelectedIndex = Pengaturan.gayatrans
        TBWTPass.Text = Pengaturan.wototutuppass
        TBWTSurvey.Text = Pengaturan.wototutupsurvey
        TBDurasiTrans.Text = Pengaturan.wgayatrans
    End Sub

    Private Sub Form_Pengaturan_Survey_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
        Form_Pengaturan.Focus()
    End Sub

    Private Sub Form_Pengaturan_Survey_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inisialisasi()
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
    End Sub

    Private Sub CBTOtoSurvey_CheckedChanged(sender As Object, e As EventArgs) Handles CBTOtoSurvey.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub BOk_Click(sender As Object, e As EventArgs) Handles BOk.Click
        Pengaturan.ototutuppass = CBTOtoPass.Checked
        Pengaturan.ototutupsurvey = CBTOtoSurvey.Checked
        Pengaturan.gayatrans = CBTrans.SelectedIndex
        Pengaturan.wototutuppass = TBWTPass.Text
        Pengaturan.wototutupsurvey = TBWTSurvey.Text
        Pengaturan.wgayatrans = TBDurasiTrans.Text
        Me.Close()
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub CBTOtoPass_CheckedChanged(sender As Object, e As EventArgs) Handles CBTOtoPass.CheckedChanged
        Form_Pengaturan.SetCheck(sender)

    End Sub

    Private Sub TBWTPass_Click(sender As Object, e As EventArgs) Handles TBWTPass.Click, TBWTSurvey.Click, TBDurasiTrans.Click
        Form_Back.osk.namaform = Me
        Form_Back.osk.Tampilkan()
    End Sub
End Class