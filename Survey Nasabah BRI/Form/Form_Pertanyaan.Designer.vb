﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pertanyaan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TIdle = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.LInformasi = New System.Windows.Forms.Label()
        Me.LSubInfo = New System.Windows.Forms.Label()
        Me.TTanggal = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.LPosisi = New System.Windows.Forms.Label()
        Me.LTanggal = New System.Windows.Forms.Label()
        Me.LInstansi = New System.Windows.Forms.Label()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.B9 = New System.Windows.Forms.Button()
        Me.B8 = New System.Windows.Forms.Button()
        Me.B7 = New System.Windows.Forms.Button()
        Me.B10 = New System.Windows.Forms.Button()
        Me.B6 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.B4 = New System.Windows.Forms.Button()
        Me.B3 = New System.Windows.Forms.Button()
        Me.B2 = New System.Windows.Forms.Button()
        Me.B5 = New System.Windows.Forms.Button()
        Me.B1 = New System.Windows.Forms.Button()
        Me.LPertanyaan = New System.Windows.Forms.Label()
        Me.THideInfo = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TIdle
        '
        Me.TIdle.Interval = 30000
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel5.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.LInformasi, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.LSubInfo, 0, 1)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(12, 12)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 2
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(814, 560)
        Me.TableLayoutPanel5.TabIndex = 14
        Me.TableLayoutPanel5.Visible = False
        '
        'LInformasi
        '
        Me.LInformasi.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LInformasi.BackColor = System.Drawing.Color.Transparent
        Me.LInformasi.Font = New System.Drawing.Font("Century Gothic", 30.0!)
        Me.LInformasi.ForeColor = System.Drawing.Color.White
        Me.LInformasi.Location = New System.Drawing.Point(3, 0)
        Me.LInformasi.Name = "LInformasi"
        Me.LInformasi.Size = New System.Drawing.Size(808, 364)
        Me.LInformasi.TabIndex = 11
        Me.LInformasi.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'LSubInfo
        '
        Me.LSubInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LSubInfo.BackColor = System.Drawing.Color.Transparent
        Me.LSubInfo.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.LSubInfo.ForeColor = System.Drawing.Color.White
        Me.LSubInfo.Location = New System.Drawing.Point(3, 364)
        Me.LSubInfo.Name = "LSubInfo"
        Me.LSubInfo.Size = New System.Drawing.Size(808, 196)
        Me.LSubInfo.TabIndex = 11
        Me.LSubInfo.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'TTanggal
        '
        Me.TTanggal.Enabled = True
        Me.TTanggal.Interval = 50
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel4.ColumnCount = 1
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.LJudul, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.TableLayoutPanel1, 0, 7)
        Me.TableLayoutPanel4.Controls.Add(Me.TableLayoutPanel3, 0, 5)
        Me.TableLayoutPanel4.Controls.Add(Me.TableLayoutPanel2, 0, 4)
        Me.TableLayoutPanel4.Controls.Add(Me.LPertanyaan, 0, 3)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 8
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 55.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(838, 584)
        Me.TableLayoutPanel4.TabIndex = 13
        '
        'LJudul
        '
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LJudul.Font = New System.Drawing.Font("Moire", 25.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.White
        Me.LJudul.Location = New System.Drawing.Point(3, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(832, 55)
        Me.LJudul.TabIndex = 11
        Me.LJudul.Text = "SURVEI NASABAH"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 320.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 330.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.LPosisi, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LTanggal, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.LInstansi, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 532)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(832, 49)
        Me.TableLayoutPanel1.TabIndex = 8
        '
        'LPosisi
        '
        Me.LPosisi.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LPosisi.BackColor = System.Drawing.Color.Transparent
        Me.LPosisi.Font = New System.Drawing.Font("Moire", 15.0!)
        Me.LPosisi.ForeColor = System.Drawing.Color.MidnightBlue
        Me.LPosisi.Location = New System.Drawing.Point(323, 0)
        Me.LPosisi.Name = "LPosisi"
        Me.LPosisi.Size = New System.Drawing.Size(176, 49)
        Me.LPosisi.TabIndex = 13
        Me.LPosisi.Text = "◄ 3 dari 5 Pertanyaan ►"
        Me.LPosisi.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LTanggal
        '
        Me.LTanggal.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LTanggal.AutoEllipsis = True
        Me.LTanggal.BackColor = System.Drawing.Color.Transparent
        Me.LTanggal.Font = New System.Drawing.Font("Moire", 12.5!)
        Me.LTanggal.ForeColor = System.Drawing.Color.MidnightBlue
        Me.LTanggal.Location = New System.Drawing.Point(505, 0)
        Me.LTanggal.Name = "LTanggal"
        Me.LTanggal.Size = New System.Drawing.Size(324, 49)
        Me.LTanggal.TabIndex = 14
        Me.LTanggal.Text = "30 Januari 2015, 06:30 PM"
        Me.LTanggal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'LInstansi
        '
        Me.LInstansi.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LInstansi.BackColor = System.Drawing.Color.Transparent
        Me.LInstansi.Font = New System.Drawing.Font("Moire", 15.0!)
        Me.LInstansi.ForeColor = System.Drawing.Color.MidnightBlue
        Me.LInstansi.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LInstansi.Location = New System.Drawing.Point(3, 0)
        Me.LInstansi.Name = "LInstansi"
        Me.LInstansi.Size = New System.Drawing.Size(314, 49)
        Me.LInstansi.TabIndex = 12
        Me.LInstansi.Text = "BANK RAKYAT INDONESIA"
        Me.LInstansi.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel3.ColumnCount = 7
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.13592!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.13592!))
        Me.TableLayoutPanel3.Controls.Add(Me.B9, 4, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.B8, 3, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.B7, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.B10, 5, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.B6, 1, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 386)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(832, 83)
        Me.TableLayoutPanel3.TabIndex = 10
        '
        'B9
        '
        Me.B9.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B9.CausesValidation = False
        Me.B9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B9.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B9.FlatAppearance.BorderSize = 2
        Me.B9.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B9.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B9.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B9.Location = New System.Drawing.Point(488, 10)
        Me.B9.Margin = New System.Windows.Forms.Padding(10)
        Me.B9.Name = "B9"
        Me.B9.Size = New System.Drawing.Size(106, 63)
        Me.B9.TabIndex = 8
        Me.B9.Text = "Buruk"
        Me.B9.UseVisualStyleBackColor = False
        Me.B9.Visible = False
        '
        'B8
        '
        Me.B8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B8.CausesValidation = False
        Me.B8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B8.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B8.FlatAppearance.BorderSize = 2
        Me.B8.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B8.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B8.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B8.Location = New System.Drawing.Point(362, 10)
        Me.B8.Margin = New System.Windows.Forms.Padding(10)
        Me.B8.Name = "B8"
        Me.B8.Size = New System.Drawing.Size(106, 63)
        Me.B8.TabIndex = 7
        Me.B8.Text = "Cukup"
        Me.B8.UseVisualStyleBackColor = False
        Me.B8.Visible = False
        '
        'B7
        '
        Me.B7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B7.CausesValidation = False
        Me.B7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B7.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B7.FlatAppearance.BorderSize = 2
        Me.B7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B7.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B7.Location = New System.Drawing.Point(236, 10)
        Me.B7.Margin = New System.Windows.Forms.Padding(10)
        Me.B7.Name = "B7"
        Me.B7.Size = New System.Drawing.Size(106, 63)
        Me.B7.TabIndex = 6
        Me.B7.Text = "Baik"
        Me.B7.UseVisualStyleBackColor = False
        Me.B7.Visible = False
        '
        'B10
        '
        Me.B10.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B10.CausesValidation = False
        Me.B10.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B10.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B10.FlatAppearance.BorderSize = 2
        Me.B10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B10.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B10.Location = New System.Drawing.Point(614, 10)
        Me.B10.Margin = New System.Windows.Forms.Padding(10)
        Me.B10.Name = "B10"
        Me.B10.Size = New System.Drawing.Size(106, 63)
        Me.B10.TabIndex = 9
        Me.B10.Text = "Sangat Buruk"
        Me.B10.UseVisualStyleBackColor = False
        Me.B10.Visible = False
        '
        'B6
        '
        Me.B6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B6.CausesValidation = False
        Me.B6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B6.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B6.FlatAppearance.BorderSize = 2
        Me.B6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B6.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B6.Location = New System.Drawing.Point(110, 10)
        Me.B6.Margin = New System.Windows.Forms.Padding(10)
        Me.B6.Name = "B6"
        Me.B6.Size = New System.Drawing.Size(106, 63)
        Me.B6.TabIndex = 5
        Me.B6.Text = "Sangat Baik"
        Me.B6.UseVisualStyleBackColor = False
        Me.B6.Visible = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel2.ColumnCount = 7
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.13592!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.14563!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.13592!))
        Me.TableLayoutPanel2.Controls.Add(Me.B4, 4, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.B3, 3, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.B2, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.B5, 5, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.B1, 1, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 295)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(832, 84)
        Me.TableLayoutPanel2.TabIndex = 10
        '
        'B4
        '
        Me.B4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B4.CausesValidation = False
        Me.B4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B4.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B4.FlatAppearance.BorderSize = 2
        Me.B4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B4.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B4.Location = New System.Drawing.Point(488, 10)
        Me.B4.Margin = New System.Windows.Forms.Padding(10)
        Me.B4.Name = "B4"
        Me.B4.Size = New System.Drawing.Size(106, 64)
        Me.B4.TabIndex = 3
        Me.B4.Text = "Buruk"
        Me.B4.UseVisualStyleBackColor = False
        Me.B4.Visible = False
        '
        'B3
        '
        Me.B3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B3.CausesValidation = False
        Me.B3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B3.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B3.FlatAppearance.BorderSize = 2
        Me.B3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B3.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B3.Location = New System.Drawing.Point(362, 10)
        Me.B3.Margin = New System.Windows.Forms.Padding(10)
        Me.B3.Name = "B3"
        Me.B3.Size = New System.Drawing.Size(106, 64)
        Me.B3.TabIndex = 2
        Me.B3.Text = "Cukup"
        Me.B3.UseVisualStyleBackColor = False
        Me.B3.Visible = False
        '
        'B2
        '
        Me.B2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B2.CausesValidation = False
        Me.B2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B2.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B2.FlatAppearance.BorderSize = 2
        Me.B2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B2.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B2.Location = New System.Drawing.Point(236, 10)
        Me.B2.Margin = New System.Windows.Forms.Padding(10)
        Me.B2.Name = "B2"
        Me.B2.Size = New System.Drawing.Size(106, 64)
        Me.B2.TabIndex = 1
        Me.B2.Text = "Baik"
        Me.B2.UseVisualStyleBackColor = False
        Me.B2.Visible = False
        '
        'B5
        '
        Me.B5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B5.CausesValidation = False
        Me.B5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B5.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B5.FlatAppearance.BorderSize = 2
        Me.B5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B5.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B5.Location = New System.Drawing.Point(614, 10)
        Me.B5.Margin = New System.Windows.Forms.Padding(10)
        Me.B5.Name = "B5"
        Me.B5.Size = New System.Drawing.Size(106, 64)
        Me.B5.TabIndex = 4
        Me.B5.Text = "Sangat Buruk"
        Me.B5.UseVisualStyleBackColor = False
        Me.B5.Visible = False
        '
        'B1
        '
        Me.B1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.B1.CausesValidation = False
        Me.B1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.B1.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.B1.FlatAppearance.BorderSize = 2
        Me.B1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Peru
        Me.B1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Wheat
        Me.B1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B1.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.B1.Location = New System.Drawing.Point(110, 10)
        Me.B1.Margin = New System.Windows.Forms.Padding(10)
        Me.B1.Name = "B1"
        Me.B1.Size = New System.Drawing.Size(106, 64)
        Me.B1.TabIndex = 0
        Me.B1.Text = "Sangat Baik"
        Me.B1.UseVisualStyleBackColor = False
        Me.B1.Visible = False
        '
        'LPertanyaan
        '
        Me.LPertanyaan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LPertanyaan.BackColor = System.Drawing.Color.Transparent
        Me.LPertanyaan.Font = New System.Drawing.Font("Century Gothic", 20.0!)
        Me.LPertanyaan.ForeColor = System.Drawing.Color.White
        Me.LPertanyaan.Location = New System.Drawing.Point(3, 202)
        Me.LPertanyaan.Name = "LPertanyaan"
        Me.LPertanyaan.Size = New System.Drawing.Size(832, 90)
        Me.LPertanyaan.TabIndex = 10
        Me.LPertanyaan.Text = "Bagaimana pelayanan kami menurut Anda ?"
        Me.LPertanyaan.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'THideInfo
        '
        Me.THideInfo.Interval = 10000
        '
        'Form_Pertanyaan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(838, 584)
        Me.Controls.Add(Me.TableLayoutPanel4)
        Me.Controls.Add(Me.TableLayoutPanel5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_Pertanyaan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form_Pertanyaan"
        Me.TopMost = True
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TIdle As System.Windows.Forms.Timer
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LInformasi As System.Windows.Forms.Label
    Friend WithEvents LSubInfo As System.Windows.Forms.Label
    Friend WithEvents TTanggal As System.Windows.Forms.Timer
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents LPosisi As System.Windows.Forms.Label
    Friend WithEvents LTanggal As System.Windows.Forms.Label
    Friend WithEvents LInstansi As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents B9 As System.Windows.Forms.Button
    Friend WithEvents B8 As System.Windows.Forms.Button
    Friend WithEvents B7 As System.Windows.Forms.Button
    Friend WithEvents B10 As System.Windows.Forms.Button
    Friend WithEvents B6 As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents B4 As System.Windows.Forms.Button
    Friend WithEvents B3 As System.Windows.Forms.Button
    Friend WithEvents B2 As System.Windows.Forms.Button
    Friend WithEvents B5 As System.Windows.Forms.Button
    Friend WithEvents B1 As System.Windows.Forms.Button
    Friend WithEvents THideInfo As System.Windows.Forms.Timer
    Friend WithEvents LPertanyaan As System.Windows.Forms.Label
End Class
