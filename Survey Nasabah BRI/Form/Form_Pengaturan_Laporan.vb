﻿Imports AnimasiUI.Class_Animasi
Imports System.Security.AccessControl
Imports System.IO
Imports System.Security.Principal

Public Class Form_Pengaturan_Laporan
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Inisialisasi()
        CBStatistikJawaban.Checked = Pengaturan.statistik
        TBAlamat.Text = Pengaturan.alamatlaporan
        CBModelTabel.SelectedIndex = Pengaturan.modeltabel
        CBPerataan.SelectedIndex = Pengaturan.modelisi
    End Sub

    Private Sub Form_Pengaturan_Laporan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
        Form_Pengaturan.Focus()
    End Sub

    Private Sub Form_Pengaturan_Laporan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inisialisasi()
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
    End Sub

    Private Sub CBStatistikJawaban_CheckedChanged(sender As Object, e As EventArgs) Handles CBStatistikJawaban.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub BJelajag_Click(sender As Object, e As EventArgs) Handles BJelajag.Click
        Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei")
        Dim sec As DirectorySecurity = Directory.GetAccessControl(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei")
        ' Using this instead of the "Everyone" string means we work on non-English systems.
        Dim everyone As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
        sec.AddAccessRule(New FileSystemAccessRule(everyone, FileSystemRights.Modify Or FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow))
        Directory.SetAccessControl(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei", sec)

        SaveFileDialog1.AddExtension = True
        SaveFileDialog1.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei"
        If SaveFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
            TBAlamat.Text = SaveFileDialog1.FileName
        End If
    End Sub

    Private Sub BOk_Click(sender As Object, e As EventArgs) Handles BOk.Click
        Pengaturan.statistik = CBStatistikJawaban.Checked
        Pengaturan.alamatlaporan = TBAlamat.Text
        Pengaturan.modeltabel = CBModelTabel.SelectedIndex
        Pengaturan.modelisi = CBPerataan.SelectedIndex
        Me.Close()
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub TBAlamat_Click(sender As Object, e As EventArgs) Handles TBAlamat.Click
        Form_Back.osk.namaform = Me
        Form_Back.osk.Tampilkan()
    End Sub
End Class