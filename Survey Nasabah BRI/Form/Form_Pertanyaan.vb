﻿Imports SkinApp.Gambar
Imports AnimasiUI.Class_Animasi

Public Class Form_Pertanyaan
    Dim ID As String

    Public Sub AturTombol(ByVal jumlah As Integer)
        If jumlah = 2 Then
            B1.Visible = False
            B1.Name = "N1"

            B2.Visible = True
            B2.Name = "B1"

            B3.Visible = False
            B3.Name = "N3"

            B4.Visible = True
            B4.Name = "B2"

            B5.Visible = False
            B5.Name = "N5"
        ElseIf jumlah = 3 Then
            B1.Visible = False
            B1.Name = "N1"

            B2.Visible = True
            B2.Name = "B1"

            B3.Visible = True
            B3.Name = "B2"

            B4.Visible = True
            B4.Name = "B3"

            B5.Visible = False
            B5.Name = "N5"
        ElseIf jumlah = 4 Then
            B1.Visible = True
            B1.Name = "B1"

            B2.Visible = True
            B2.Name = "B2"

            B3.Visible = False
            B3.Name = "N3"

            B4.Visible = True
            B4.Name = "B3"

            B5.Visible = True
            B5.Name = "B4"
        ElseIf jumlah = 5 Then
            B1.Visible = True
            B1.Name = "B1"

            B2.Visible = True
            B2.Name = "B2"

            B3.Visible = True
            B3.Name = "B3"

            B4.Visible = True
            B4.Name = "B4"

            B5.Visible = True
            B5.Name = "B5"
        End If
    End Sub

    Public Sub RestartTIdle()
        TIdle.Dispose()
        TIdle.Interval = Pengaturan.wototutupsurvey * 1000
        TIdle.Enabled = False
        TIdle.Enabled = Pengaturan.ototutupsurvey
    End Sub

    Public Sub AturTextTombol(ByVal teks As String())
        Dim jum As Integer = teks.Length
        If jum = 2 Then
            B2.Text = teks(0)
            B4.Text = teks(1)
        ElseIf jum = 3 Then
            B2.Text = teks(0)
            B3.Text = teks(1)
            B4.Text = teks(2)
        ElseIf jum = 4 Then
            B1.Text = teks(0)
            B2.Text = teks(1)
            B4.Text = teks(2)
            B5.Text = teks(3)
        ElseIf jum = 5 Then
            B1.Text = teks(0)
            B2.Text = teks(1)
            B3.Text = teks(2)
            B4.Text = teks(3)
            B5.Text = teks(4)
        End If
    End Sub

    Private Sub Form_Pertanyaan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Not daritombol Then
            e.Cancel = True
        Else
            Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        End If
    End Sub

    Public Sub AturPosisi(ByVal cur As Integer, ByVal dari As Integer)
        LPosisi.Text = "◄ " & cur & " dari " & dari & " Pertanyaan ►"
    End Sub

    Private Sub MuatBackground()
        Dim img As Image = AmbilGambar("latar_pertanyaan_besar")
        Me.BackgroundImage = img
        If My.Computer.Screen.Bounds.Size.Width > 1366 Then
            Me.BackgroundImageLayout = ImageLayout.Stretch
        End If
    End Sub

    Private Sub Form_Pertanyaan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Threading.Thread.CurrentThread.Priority = Threading.ThreadPriority.Highest
        Dim t As New Threading.Thread(AddressOf MuatBackground)
        t.Priority = Threading.ThreadPriority.Highest
        t.IsBackground = True
        t.Start()
        Application.DoEvents()

        Dim gmbar As Image = AmbilGambar("logo_bri")
        LInstansi.Image = gmbar
        Application.DoEvents()
        Me.Location = New Point(0, 0)
        Me.Size = My.Computer.Screen.Bounds.Size
        Application.DoEvents()
        AturPosisi(Form_Back.cur, Form_Back.total)
        AturTombol(PilJawaban(Form_Back.cur - 1).Length)
        AturTextTombol(PilJawaban(Form_Back.cur - 1))
        LPertanyaan.Text = Pertanyaan(Form_Back.cur - 1)(0)
        ID = Pertanyaan(Form_Back.cur - 1)(1)
        Application.DoEvents()
        LTanggal.Text = KonversiTanggalInd(Now)
        Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        TIdle.Enabled = Pengaturan.ototutupsurvey
        TIdle.Interval = Pengaturan.wototutupsurvey * 1000
    End Sub

    Private Sub TampilInfo()
        TableLayoutPanel4.Visible = False
        TableLayoutPanel5.Visible = True
        LInformasi.Text = "Terima kasih atas partisipasi Anda," + vbNewLine + "Kami akan terus berusaha dalam meningkatkan pelayanan kami." + vbNewLine + vbNewLine + "BANK RAKYAT INDONESIA"
        LSubInfo.Text = "MELAYANI DENGAN SETULUS HATI"
        THideInfo.Enabled = True
        Jeda(1000)
    End Sub

    Public Sub DoNext(ByVal tombol As Object)
        TambahJawaban(ID + ".jb", LPertanyaan.Text, tombol)
        Animasikan(Me.Handle, Pengaturan.wgayatrans, Pengaturan.gayatrans, OpsiGaya.Tutup)
        Form_Back.cur += 1
        If Form_Back.cur - 1 < Form_Back.total Then
            AturPosisi(Form_Back.cur, Form_Back.total)
            AturTombol(PilJawaban(Form_Back.cur - 1).Length)
            AturTextTombol(PilJawaban(Form_Back.cur - 1))
            LPertanyaan.Text = Pertanyaan(Form_Back.cur - 1)(0)
            ID = Pertanyaan(Form_Back.cur - 1)(1)
        ElseIf Form_Back.cur - 1 = Form_Back.total Then
            TIdle.Enabled = False
            TampilInfo()
        End If
        Application.DoEvents()
        Jeda(100)
        Animasikan(Me.Handle, Pengaturan.wgayatrans, Pengaturan.gayatrans, OpsiGaya.Buka)
    End Sub

    Private Sub B1_Click(sender As Object, e As EventArgs) Handles B1.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B2_Click(sender As Object, e As EventArgs) Handles B2.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B3_Click(sender As Object, e As EventArgs) Handles B3.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B4_Click(sender As Object, e As EventArgs) Handles B4.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B5_Click(sender As Object, e As EventArgs) Handles B5.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B6_Click(sender As Object, e As EventArgs) Handles B6.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B7_Click(sender As Object, e As EventArgs) Handles B7.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B8_Click(sender As Object, e As EventArgs) Handles B8.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B9_Click(sender As Object, e As EventArgs) Handles B9.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub B10_Click(sender As Object, e As EventArgs) Handles B10.Click
        RestartTIdle()
        DoNext(sender)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles TTanggal.Tick
        LTanggal.Text = KonversiTanggalInd(Now)
    End Sub

    Dim daritombol As Boolean

    Private Sub Timer1_Tick_1(sender As Object, e As EventArgs) Handles THideInfo.Tick
        THideInfo.Enabled = False
        daritombol = True
        Me.Close()
        Dispose()
        Form_Back.JalankanSurvey()
    End Sub

    Private Sub TIdle_Tick(sender As Object, e As EventArgs) Handles TIdle.Tick
        TIdle.Enabled = False
        Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Jeda(500)
        TampilInfo()
        Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Buka)
    End Sub
End Class