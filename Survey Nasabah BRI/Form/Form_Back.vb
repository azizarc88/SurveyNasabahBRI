﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Back
    Public firstrun As Boolean = True
    Public cur As Integer = 0
    Public total As Integer = 0
    Public p As Process
    Public osk As New OSK()

    <Flags()> _
    Public Enum NamaTombol
        Bantuan = 0
        Keluar = 1
        Pengaturan = 2
        Data = 3
    End Enum

    <Flags()> _
    Public Enum StatusTombol
        Aktiv = 0
        Nonaktiv = 1
        MouseDown = 2
        MouseUp = 3
    End Enum

    <Flags()> _
    Public Enum StatusMouse
        DidalamTombol = 0
        DiluarTombol = 1
    End Enum

    <Flags()> _
    Public Enum StatusKlik
        KlikDown = 0
        KlikUp = 1
        Idle = 2
    End Enum

    Public Function KonversiTombol(namatombol As NamaTombol) As PictureBox
        If namatombol = Form_Back.NamaTombol.Bantuan Then
            Return Form_MenuUtama.PBBantuan
        ElseIf namatombol = Form_Back.NamaTombol.Data Then
            Return Form_MenuUtama.PBData
        ElseIf namatombol = Form_Back.NamaTombol.Keluar Then
            Return Form_MenuUtama.PBKeluar
        Else
            Return Form_MenuUtama.PBPengaturan
        End If
    End Function

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Form_Back_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Size = Screen.PrimaryScreen.Bounds.Size
        Me.Location = New Point(0, 0)
        Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        TMenuUtama.Enabled = True
    End Sub

    Private Sub TMenuUtama_Tick(sender As Object, e As EventArgs) Handles TMenuUtama.Tick
        Form_MenuUtama.Show()
        TMenuUtama.Enabled = False
    End Sub

    Public Sub JalankanSurvey()
        Form_SiapSurvey.Show()
    End Sub

    Public Sub JalankanSurvey2()
        cur = 1
        total = Pertanyaan.Count
        Form_Pertanyaan.Show()
    End Sub

    Public Sub TampilPertanyaan()
        Form_Pertanyaan.Show()
    End Sub

    Dim done As Boolean = False

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim proses As List(Of String) = New List(Of String)
        Dim ada As Boolean = False
        For Each p As Process In Process.GetProcesses()
            If p.ProcessName.ToString.ToLower = "osk" Then
                ada = True
                Exit For
            End If
        Next
        osk.ditampilkan = ada
        If Not ada Then
            done = False
        End If
    End Sub

    Dim counter As Integer = 0

    Private Sub Timer2_Tick(sender As Object, e As EventArgs) Handles Timer2.Tick
        Timer2.Enabled = False
        If done = False And osk.ditampilkan = True Then
            Try
                Dim x, y As String
                Dim data As New List(Of String)
                Dim objReader As New System.IO.StreamReader(AlamatAppData + "\key.ini")
                Do While Not objReader.EndOfStream
                    data.Add(objReader.ReadLine())
                Loop
                objReader.Close()

                x = data(0)
                y = data(1)

                Dim na As Integer
                If osk.namaform.Bottom > y Then
                    na = osk.namaform.Location.Y - ((osk.namaform.Bottom - y) + 5)
                    If na < 0 Then
                        na = 0
                    End If

                    GeserKontrol(osk.namaform, na, Pengubah.Dikurangi, Orientasi.Vertikal)
                End If

                done = True
            Catch ex As Exception
            End Try
        End If
        Timer2.Enabled = True
    End Sub
End Class
