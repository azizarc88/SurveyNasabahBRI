﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Pengaturan_Keamanan
    Private kuncienk, password As String
    Private warna As Boolean
    Private pass As Boolean
    Private kunci As Boolean

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Inisialisasi()
        kuncienk = Pengaturan.kuncienk
        password = Pengaturan.password
        CBAcak.Checked = True
        CBIdent.Checked = True
        If Form_MenuUtama.pengaturanfirst = True Then
            TBKunciLama.Text = Pengaturan.kuncienk
            TBPassLama.Text = Pengaturan.password
        End If
    End Sub

    Private Sub Bersihkan()
        TBKunciBaru.Clear()
        If Form_MenuUtama.pengaturanfirst = False Then
            TBKunciLama.Clear()
            TBPassLama.Clear()
        End If
        TBUlangiKunci.Clear()
        TBPassBaru.Clear()
        TBPassUlangi.Clear()
    End Sub

    Private Sub Form_Pengaturan_Keamanan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
        Form_Pengaturan.Focus()
    End Sub

    Private Sub Form_Pengaturan_Keamanan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inisialisasi()
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        If Form_MenuUtama.pengaturanfirst = True Then
            kunci = True
            pass = True
            Timer1.Enabled = True
            If Form_Pengaturan.passsudah = False Then
                TBPassLama.Enabled = False
            End If
            If Form_Pengaturan.kuncisudah = False Then
                TBKunciLama.Enabled = False
            End If
        End If

        If Form_MenuUtama.pengaturanfirst = False Or Form_Pengaturan.kuncisudah = True Then
            TBKunciLama.Enabled = True
        End If
        If Form_MenuUtama.pengaturanfirst = False Or Form_Pengaturan.passsudah = True Then
            TBPassLama.Enabled = True
        End If
    End Sub

    Private Sub BAturPass_Click(sender As Object, e As EventArgs) Handles BAturPass.Click
        PPassword.Location = New Point(0, 0)
        Animasikan(PPassword.Handle, 150, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        Application.DoEvents()
        PPassword.Visible = True
        Application.DoEvents()

        pass = False
        BAturPass.FlatAppearance.BorderColor = Color.SteelBlue
        BAturPass.FlatAppearance.BorderSize = 2
    End Sub

    Private Sub CBIdent_CheckedChanged(sender As Object, e As EventArgs) Handles CBIdent.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub CBAcak_CheckedChanged(sender As Object, e As EventArgs) Handles CBAcak.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub BOk_Click(sender As Object, e As EventArgs) Handles BOk.Click
        Pengaturan.kuncienk = kuncienk
        Pengaturan.password = password
        Me.Close()
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        If Form_MenuUtama.pengaturanfirst = True Then
            NotificationManager.Show(Me, "Tidak dapat membatalkan karena alasan keamanan.", Color.Red, 3000)
            Exit Sub
        End If
        Me.Close()
    End Sub

    Private Sub BAturEnkripsi_Click_1(sender As Object, e As EventArgs) Handles BAturEnkripsi.Click
        PKunci.Location = New Point(0, 0)
        Animasikan(PKunci.Handle, 150, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        Application.DoEvents()
        PKunci.Visible = True
        Application.DoEvents()

        kunci = False
        BAturEnkripsi.FlatAppearance.BorderColor = Color.SteelBlue
        BAturEnkripsi.FlatAppearance.BorderSize = 2
    End Sub

    Private Sub BOkKunci_Click(sender As Object, e As EventArgs) Handles BOkKunci.Click
        If Not TBKunciLama.Text = Pengaturan.kuncienk Then
            NotificationManager.Show(PKunci, "Kunci lama tidak sesuai", Color.Red, 5000)
        ElseIf Not TBKunciBaru.Text = TBUlangiKunci.Text Then
            NotificationManager.Show(PKunci, "Anda mengulangi pemasukan kunci baru yang tidak sama", Color.Orange, 5000)
        ElseIf TBKunciBaru.Text = "" Or TBUlangiKunci.Text = "" Then
            NotificationManager.Show(PKunci, "Isian belum lengkap, silakan isi kembali", Color.Orange, 5000)
        Else
            kuncienk = TBKunciBaru.Text
            Animasikan(PKunci.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
            Form_Pengaturan.kuncisudah = True
        End If
    End Sub

    Private Sub BBatalKunci_Click(sender As Object, e As EventArgs) Handles BBatalKunci.Click
        If Form_MenuUtama.pengaturanfirst = True Then
            NotificationManager.Show(PKunci, "Tidak dapat membatalkan karena alasan keamanan.", Color.Red, 3000)
            Exit Sub
        End If
        Bersihkan()
        Animasikan(PKunci.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
    End Sub

    Private Sub BOkPass_Click(sender As Object, e As EventArgs) Handles BOkPass.Click
        If Not TBPassLama.Text = Pengaturan.password Then
            NotificationManager.Show(PPassword, "Password lama tidak sesuai", Color.Red, 5000)
        ElseIf Not TBPassBaru.Text = TBPassUlangi.Text Then
            NotificationManager.Show(PPassword, "Anda mengulangi pemasukan password baru yang tidak sama", Color.Orange, 5000)
        ElseIf TBPassBaru.Text = "" Or TBPassUlangi.Text = "" Then
            NotificationManager.Show(PKunci, "Isian belum lengkap, silakan isi kembali", Color.Orange, 5000)
        Else
            password = TBPassBaru.Text
            Animasikan(PPassword.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
            Form_Pengaturan.passsudah = True
        End If
    End Sub

    Private Sub BBatalPass_Click(sender As Object, e As EventArgs) Handles BBatalPass.Click
        If Form_MenuUtama.pengaturanfirst = True Then
            NotificationManager.Show(Me, "Tidak dapat membatalkan karena alasan keamanan.", Color.Red, 3000)
            Exit Sub
        End If
        Bersihkan()
        Animasikan(PPassword.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
    End Sub

    Private Sub TBKunciBaru_GotFocus(sender As Object, e As EventArgs) Handles TBKunciBaru.GotFocus
        TBKunciBaru.SelectAll()
    End Sub

    Private Sub TBKunciLama_GotFocus(sender As Object, e As EventArgs) Handles TBKunciLama.GotFocus
        TBKunciLama.SelectAll()
    End Sub

    Private Sub TBPassBaru_HandleCreated(sender As Object, e As EventArgs) Handles TBPassBaru.GotFocus
        TBPassBaru.SelectAll()
    End Sub

    Private Sub TBPassLama_GotFocus(sender As Object, e As EventArgs) Handles TBPassLama.GotFocus
        TBPassLama.SelectAll()
    End Sub

    Private Sub TBPassUlangi_GotFocus(sender As Object, e As EventArgs) Handles TBPassUlangi.GotFocus
        TBPassUlangi.SelectAll()
    End Sub

    Private Sub TBUlangiKunci_GotFocus(sender As Object, e As EventArgs) Handles TBUlangiKunci.GotFocus
        TBUlangiKunci.SelectAll()
    End Sub

    Private Sub TBPassLama_Click(sender As Object, e As EventArgs) Handles TBPassLama.Click, TBUlangiKunci.Click, TBPassUlangi.Click, TBPassBaru.Click, TBKunciLama.Click, TBKunciBaru.Click
        Form_Back.osk.namaform = Me
        Form_Back.osk.Tampilkan()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If warna = False Then
            If pass = True Then
                BAturPass.FlatAppearance.BorderColor = Color.Green
                BAturPass.FlatAppearance.BorderSize = 4
            End If
            If kunci = True Then
                BAturEnkripsi.FlatAppearance.BorderColor = Color.Green
                BAturEnkripsi.FlatAppearance.BorderSize = 4
            End If
            warna = True
        Else
            If pass = True Then
                BAturPass.FlatAppearance.BorderColor = Color.SteelBlue
                BAturPass.FlatAppearance.BorderSize = 2
            End If
            If kunci = True Then
                BAturEnkripsi.FlatAppearance.BorderColor = Color.SteelBlue
                BAturEnkripsi.FlatAppearance.BorderSize = 2
            End If
            warna = False
        End If
        If (pass Or kunci) = False Then
            Timer1.Enabled = False
            Form_Pengaturan.keamanandone = True
        End If
    End Sub
End Class