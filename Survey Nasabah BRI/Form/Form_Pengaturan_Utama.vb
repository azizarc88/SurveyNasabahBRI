﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Pengaturan_Utama
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Inisialisasi()
        CBTeksSelamat.Checked = Pengaturan.tekspembuka
        CBTMenuUtama.Checked = Pengaturan.animmenuutama
        CBTMenuSurvet.Checked = Pengaturan.animkemsurvey
        CBTPertanyaan.Checked = Pengaturan.animkempertanyaan
    End Sub

    Private Sub Form_Pengaturan_Utama_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
        Form_Pengaturan.Focus()
    End Sub

    Private Sub Form_Pengaturan_Utama_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Inisialisasi()
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub CBTeksSelamat_CheckedChanged(sender As Object, e As EventArgs) Handles CBTeksSelamat.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub CBTMenuUtama_CheckedChanged(sender As Object, e As EventArgs) Handles CBTMenuUtama.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub CBTMenuSurvet_CheckedChanged(sender As Object, e As EventArgs) Handles CBTMenuSurvet.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub CBTPertanyaan_CheckedChanged(sender As Object, e As EventArgs) Handles CBTPertanyaan.CheckedChanged
        Form_Pengaturan.SetCheck(sender)
    End Sub

    Private Sub BOk_Click(sender As Object, e As EventArgs) Handles BOk.Click
        Pengaturan.tekspembuka = CBTeksSelamat.Checked
        Pengaturan.animmenuutama = CBTMenuUtama.Checked
        Pengaturan.animkemsurvey = CBTMenuSurvet.Checked
        Pengaturan.animkempertanyaan = CBTPertanyaan.Checked
        Me.Close()
    End Sub
End Class