﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pengaturan_Survey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Pengaturan_Survey))
        Me.LJudul = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TBWTSurvey = New System.Windows.Forms.TextBox()
        Me.TBWTPass = New System.Windows.Forms.TextBox()
        Me.CBTOtoSurvey = New System.Windows.Forms.CheckBox()
        Me.CBTOtoPass = New System.Windows.Forms.CheckBox()
        Me.CBTrans = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TBDurasiTrans = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.BOk = New System.Windows.Forms.Button()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(17, 11)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(8, 0, 8, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(647, 50)
        Me.LJudul.TabIndex = 29
        Me.LJudul.Text = "PENGATURAN SURVEI"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 14.0!)
        Me.Label2.Location = New System.Drawing.Point(595, 152)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 22)
        Me.Label2.TabIndex = 35
        Me.Label2.Text = "Detik"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 14.0!)
        Me.Label1.Location = New System.Drawing.Point(595, 95)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(56, 22)
        Me.Label1.TabIndex = 36
        Me.Label1.Text = "Detik"
        '
        'TBWTSurvey
        '
        Me.TBWTSurvey.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBWTSurvey.Location = New System.Drawing.Point(532, 144)
        Me.TBWTSurvey.Name = "TBWTSurvey"
        Me.TBWTSurvey.Size = New System.Drawing.Size(57, 37)
        Me.TBWTSurvey.TabIndex = 33
        Me.TBWTSurvey.Text = "5"
        Me.TBWTSurvey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'TBWTPass
        '
        Me.TBWTPass.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBWTPass.Location = New System.Drawing.Point(532, 87)
        Me.TBWTPass.Name = "TBWTPass"
        Me.TBWTPass.Size = New System.Drawing.Size(57, 37)
        Me.TBWTPass.TabIndex = 34
        Me.TBWTPass.Text = "5"
        Me.TBWTPass.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'CBTOtoSurvey
        '
        Me.CBTOtoSurvey.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBTOtoSurvey.BackColor = System.Drawing.Color.White
        Me.CBTOtoSurvey.Checked = True
        Me.CBTOtoSurvey.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBTOtoSurvey.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBTOtoSurvey.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBTOtoSurvey.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBTOtoSurvey.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTOtoSurvey.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBTOtoSurvey.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBTOtoSurvey.Location = New System.Drawing.Point(23, 142)
        Me.CBTOtoSurvey.Margin = New System.Windows.Forms.Padding(8)
        Me.CBTOtoSurvey.Name = "CBTOtoSurvey"
        Me.CBTOtoSurvey.Size = New System.Drawing.Size(498, 41)
        Me.CBTOtoSurvey.TabIndex = 31
        Me.CBTOtoSurvey.Text = "Selesaikan otomatis survei ketika diam selama"
        Me.CBTOtoSurvey.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBTOtoSurvey.UseVisualStyleBackColor = False
        '
        'CBTOtoPass
        '
        Me.CBTOtoPass.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBTOtoPass.BackColor = System.Drawing.Color.White
        Me.CBTOtoPass.Checked = True
        Me.CBTOtoPass.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBTOtoPass.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBTOtoPass.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBTOtoPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBTOtoPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTOtoPass.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBTOtoPass.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBTOtoPass.Location = New System.Drawing.Point(23, 85)
        Me.CBTOtoPass.Margin = New System.Windows.Forms.Padding(8)
        Me.CBTOtoPass.Name = "CBTOtoPass"
        Me.CBTOtoPass.Size = New System.Drawing.Size(498, 41)
        Me.CBTOtoPass.TabIndex = 32
        Me.CBTOtoPass.Text = "Tutup otomatis jendela Password ketika diam selama"
        Me.CBTOtoPass.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBTOtoPass.UseVisualStyleBackColor = False
        '
        'CBTrans
        '
        Me.CBTrans.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBTrans.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTrans.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.CBTrans.FormattingEnabled = True
        Me.CBTrans.Items.AddRange(New Object() {"Geser Horizontal Kiri", "Geser Horizontal Kanan", "Geser Vertikal Atas", "Geser Vertikal Bawah", "Perluas Horizontal Kiri", "Perluar Horizontal Kanan", "Perluas Vertikal Atas", "Perluas Vertikal Bawah", "Tengah", "Menyatu"})
        Me.CBTrans.Location = New System.Drawing.Point(169, 194)
        Me.CBTrans.Name = "CBTrans"
        Me.CBTrans.Size = New System.Drawing.Size(272, 38)
        Me.CBTrans.TabIndex = 37
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label3.Location = New System.Drawing.Point(19, 203)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(144, 23)
        Me.Label3.TabIndex = 38
        Me.Label3.Text = "Gaya transisi :"
        '
        'TBDurasiTrans
        '
        Me.TBDurasiTrans.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBDurasiTrans.Location = New System.Drawing.Point(532, 195)
        Me.TBDurasiTrans.Name = "TBDurasiTrans"
        Me.TBDurasiTrans.Size = New System.Drawing.Size(57, 37)
        Me.TBDurasiTrans.TabIndex = 33
        Me.TBDurasiTrans.Text = "5"
        Me.TBDurasiTrans.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.Label4.Location = New System.Drawing.Point(595, 203)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(69, 19)
        Me.Label4.TabIndex = 35
        Me.Label4.Text = "Mili Detik"
        '
        'BOk
        '
        Me.BOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BOk.BackColor = System.Drawing.Color.White
        Me.BOk.CausesValidation = False
        Me.BOk.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BOk.FlatAppearance.BorderSize = 2
        Me.BOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BOk.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BOk.Location = New System.Drawing.Point(17, 247)
        Me.BOk.Margin = New System.Windows.Forms.Padding(8)
        Me.BOk.Name = "BOk"
        Me.BOk.Size = New System.Drawing.Size(204, 72)
        Me.BOk.TabIndex = 39
        Me.BOk.Text = "OK"
        Me.BOk.UseVisualStyleBackColor = False
        '
        'BBatal
        '
        Me.BBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatal.BackColor = System.Drawing.Color.MistyRose
        Me.BBatal.CausesValidation = False
        Me.BBatal.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatal.FlatAppearance.BorderSize = 2
        Me.BBatal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatal.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatal.Location = New System.Drawing.Point(460, 247)
        Me.BBatal.Margin = New System.Windows.Forms.Padding(8)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(204, 72)
        Me.BBatal.TabIndex = 39
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label5.Location = New System.Drawing.Point(447, 203)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(79, 23)
        Me.Label5.TabIndex = 38
        Me.Label5.Text = "Durasi :"
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-31, 333)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 27
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-31, -11)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 28
        Me.PictureBox1.TabStop = False
        '
        'Form_Pengaturan_Survey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(681, 338)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BOk)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CBTrans)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TBDurasiTrans)
        Me.Controls.Add(Me.TBWTSurvey)
        Me.Controls.Add(Me.TBWTPass)
        Me.Controls.Add(Me.CBTOtoSurvey)
        Me.Controls.Add(Me.CBTOtoPass)
        Me.Controls.Add(Me.LJudul)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_Pengaturan_Survey"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Pengaturan_Survey"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TBWTSurvey As System.Windows.Forms.TextBox
    Friend WithEvents TBWTPass As System.Windows.Forms.TextBox
    Friend WithEvents CBTOtoSurvey As System.Windows.Forms.CheckBox
    Friend WithEvents CBTOtoPass As System.Windows.Forms.CheckBox
    Friend WithEvents CBTrans As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TBDurasiTrans As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents BOk As System.Windows.Forms.Button
    Friend WithEvents BBatal As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
