﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Fade
    Public NamaForm As Form

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW
            Return Result
        End Get
    End Property

    Private Sub Form_Fade_MouseDown(sender As Object, e As MouseEventArgs) Handles Me.MouseDown
        If Form_Back.osk.ditampilkan Then
            Form_Back.osk.Sembunyikan()
            oo = True
        Else
            oo = False
        End If
        Animasikan(NamaForm.Handle, 200, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
    End Sub

    Private oo As Boolean

    Private Sub Form_Fade_Click(sender As Object, e As EventArgs) Handles Me.MouseUp
        If oo Then
            Timer1.Enabled = True
        End If
        Animasikan(NamaForm.Handle, 150, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        NamaForm.Focus()
    End Sub

    Private Sub Form_Fade_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Tutup)
    End Sub

    Private Sub Form_Fade_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Size = My.Computer.Screen.Bounds.Size
        Me.Location = New Point(0, 0)
        Animasikan(Me.Handle, 150, GayaAnimasi.Tengah, OpsiGaya.Buka)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Not Form_Back.osk.ditampilkan Then
            Form_Back.osk.Tampilkan()
            oo = False
            Timer1.Enabled = False
        End If
    End Sub
End Class