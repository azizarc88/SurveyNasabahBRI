﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Password
    Public Fungsi As String
    Public Form As Form

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Form_Password_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
    End Sub

    Private Sub Form_Password_Load(sender As Object, e As EventArgs) Handles Me.Load
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
        Form_SiapSurvey.Focus()
    End Sub

    Private Sub BProses_Click(sender As Object, e As EventArgs) Handles BProses.Click
        Application.DoEvents()
        If TBPass.Text = Pengaturan.password Then
            If Fungsi = "Tutup" Then
                Me.Close()
                Form_SiapSurvey.Focus()
                Application.DoEvents()
                Form_SiapSurvey.daritombol = True
                Form_SiapSurvey.Close()
                Application.DoEvents()
                Jeda(500)
                Application.DoEvents()
                Form_Back.Focus()
                Form_MenuUtama.Show()
                Application.DoEvents()
            ElseIf Fungsi = "Validasi" Then
                Me.Close()
                Form_PData_HasilSurvey.Focus()
                Application.DoEvents()
                Form_PData_HasilSurvey.Reset()
            End If
        Else
            NotificationManager.Show(Me, "Password salah", Color.Red, 3000)
            TBPass.Focus()
        End If
    End Sub

    Private Sub TBPass_TextChanged(sender As Object, e As EventArgs) Handles TBPass.TextChanged
        Form_SiapSurvey.TPass.Stop()
        Form_SiapSurvey.TPass.Start()
    End Sub

    Private Sub TBPass_Click(sender As Object, e As EventArgs) Handles TBPass.Click
        Form_Back.osk.namaform = Me
        Form_Back.osk.Tampilkan()
    End Sub
End Class