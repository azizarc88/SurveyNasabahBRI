﻿Imports MessageHandle
Imports AnimasiUI.Class_Animasi

Public Class Form_PData_Pertanyaan
    Public berubah As Boolean = False
    Public _Pertanyaan As List(Of String()) = New List(Of String())
    Public _PilJawaban As List(Of String()) = New List(Of String())
    Public pilberubah As Boolean = False
    Public daftarhapusberubah As List(Of String) = New List(Of String)
    Dim warna As Boolean
    Dim tambahsudah As Boolean

    Protected Overrides ReadOnly Property CreateParams() _
          As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Public Sub MuatData()
        LBPertanyaan.Items.Clear()
        LBJawaban.Items.Clear()

        For Each p As String() In _Pertanyaan
            LBPertanyaan.Items.Add(p(0))
        Next
    End Sub

    Public Sub NonAktifkan(ByVal tombol As Button)
        tombol.Enabled = False
        tombol.BackColor = Color.RosyBrown
    End Sub

    Public Sub Aktifkan(ByVal tombol As Button)
        tombol.Enabled = True
        tombol.BackColor = Color.White
    End Sub

    Private Sub HapusTidakAda()
        Try
            Dim daftar As New IO.DirectoryInfo(AlamatAppData + "\Jawaban")
            Dim daftarjawaban As IO.FileInfo() = daftar.GetFiles("*.jb")
            Dim ada As Boolean = False
            For Each i As IO.FileInfo In daftarjawaban
                ada = False
                For Each s As String() In _Pertanyaan
                    If i.Name.Replace(".jb", "") = s(1) Then
                        ada = True
                    End If
                Next
                If ada = False Then
                    IO.File.Delete(AlamatAppData + "\Jawaban\" + i.Name)
                End If
            Next
        Catch ex As Exception
        End Try
    End Sub

    Private Sub HapusBerubah()
        For Each item As String In daftarhapusberubah
            Try
                IO.File.Delete(AlamatAppData + "\Jawaban\" + item + ".jb")
            Catch ex As Exception
            End Try
        Next
    End Sub

    Private Function CekGalat() As Boolean
        If Not IO.Directory.Exists(AlamatAppData + "\Jawaban") Then
            Return True
        End If

        Dim ada As Boolean = True
        Dim daftar As New IO.DirectoryInfo(AlamatAppData + "\Jawaban")
        Dim daftarjawaban As IO.FileInfo() = daftar.GetFiles("*.jb")
        For Each i As IO.FileInfo In daftarjawaban
            ada = False
            For Each s As String() In _Pertanyaan
                If i.Name.Replace(".jb", "") = s(1) Then
                    ada = True
                End If
            Next
            If ada = False Then
                Exit For
            End If
        Next
        If ada = False Then
            If KotakPesan.Show(Me, "Apa Anda yakin ingin menyimpan perubahan?, terdapat pertanyaan yang memiliki data jawaban dari partisipan." & vbNewLine & "Jika pertanyaan tersebut dihapus, maka data jawaban dari partisipan akan ikut terhapus." & vbNewLine & "Ingin melanjutkan?", "Terdapat Data Partisipan", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Peringatan) = KotakPesan.HasilPesan.Tidak Then
                berubah = False
                Return False
            End If
        End If
        Return True
    End Function

    Public Function Hapus(ByVal koleksi As ListBox.SelectedIndexCollection) As Boolean
        Dim perubah As Integer = 0
        Try
            For Each i As Integer In koleksi
                _Pertanyaan.RemoveAt(i - perubah)
                _PilJawaban.RemoveAt(i - perubah)
                perubah += 1
            Next
        Catch ex As Exception
            KotakPesan.Show(Me, ex.ToString, "Galat saat Menghapus", KotakPesan.TombolPesan.Ulangi_Batal, KotakPesan.Warna.Galat)
            Return False
        End Try
        Return True
    End Function

    Private Sub Form_PData_Pertanyaan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Not daritombol Then
            BKembali.PerformClick()
        Else
            daritombol = False
            Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Horizontal_Kanan, OpsiGaya.Tutup)
            Me.Dispose()
        End If
    End Sub

    Dim daritombol As Boolean

    Private Sub Form_PData_Pertanyaan_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.BackColor = Color.White
        Me.Location = New Point(0, 0)
        Me.Size = Screen.PrimaryScreen.Bounds.Size
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Horizontal_Kiri, OpsiGaya.Buka)

        For Each i As String() In Pertanyaan
            _Pertanyaan.Add({i(0), i(1)})
        Next
        Dim a As Integer = 0

        Do Until a = PilJawaban.Count
            _PilJawaban.Add(PilJawaban(a))
            a += 1
        Loop
        MuatData()

        If Pengaturan.firstrun = True Then
            KotakPesan.Show(Me, "Selamat datang di bagian Pertanyaan" + Environment.NewLine + "Ijinkan kami untuk membantu Anda membuat daftar pertanyaan disini. Untuk memulai, silahkan tekan tombol OK", "Selamat Datang", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub BKembali_Click(sender As Object, e As EventArgs) Handles BKembali.Click
        If berubah = True Then
            Dim j As KotakPesan.HasilPesan = KotakPesan.Show(Me, "Daftar pertanyaan telah dirubah, Apakah Anda ingin menyimpanya terlebih dahulu?", "Daftar Berubah", KotakPesan.TombolPesan.Ya_Tidak_Batal, KotakPesan.Warna.Pertanyaan)
            If j = KotakPesan.HasilPesan.Ya Then
                If CekGalat() = False Then
                    Exit Sub
                End If
                HapusTidakAda()
                HapusBerubah()

                SimpanPertanyaan(_Pertanyaan, _PilJawaban)
                berubah = False

                BacaPertanyaan()
                BacaJawaban(Me)
                daritombol = True
                Me.Close()
                Form_PData.Focus()
            ElseIf j = KotakPesan.HasilPesan.Tidak Then
                daritombol = True
                Me.Close()
                Form_PData.Focus()
            End If
        Else
            daritombol = True
            Me.Close()
            Form_PData.Focus()
        End If
    End Sub

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        Dim f As New Form_Tambah_Pertanyaan
        If _Pertanyaan.Count > 0 Then
            f.ID = f.GenerateID(_Pertanyaan(_Pertanyaan.Count - 1)(1))
        Else
            f.ID = f.GenerateID("00000")
        End If
        Form_Fade.Show()
        f.Show()
        Form_Fade.NamaForm = f
        tambahsudah = True
    End Sub

    Private Sub LBPertanyaan_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBPertanyaan.SelectedIndexChanged
        LBJawaban.Items.Clear()
        Try
            For Each item As String In _PilJawaban(LBPertanyaan.SelectedIndex)
                LBJawaban.Items.Add(item)
            Next
            Aktifkan(BUbah)
            Aktifkan(BHapus)
        Catch ex As Exception
        End Try
        LJumlah.Text = LBJawaban.Items.Count & " Jawaban"
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        If Not berubah = True Then
            KotakPesan.Show(Me, "Tidak ada perubahan yang perlu disimpan")
            Exit Sub
        End If
        If CekGalat() = False Then
            Exit Sub
        End If
        HapusTidakAda()
        HapusBerubah()
        SimpanPertanyaan(_Pertanyaan, _PilJawaban)
        BacaJawaban(Me)
        berubah = False

        BacaPertanyaan()
        NotificationManager.Show(Me, "Daftar pertanyaan telah disimpan", Color.SteelBlue, 3000)
    End Sub

    Private Sub BUbah_Click(sender As Object, e As EventArgs) Handles BUbah.Click
        Dim f As New Form_Tambah_Pertanyaan
        Dim cur As Integer = 1
        Dim jumlah As Integer = _PilJawaban(LBPertanyaan.SelectedIndex).Length
        Form_Fade.Show()
        f.LJudul.Text = "Ubah Pertanyaan"
        f.TBPertanyaan.Text = _Pertanyaan(LBPertanyaan.SelectedIndex)(0)
        f.ID = _Pertanyaan(LBPertanyaan.SelectedIndex)(1)

        For Each item As String In _PilJawaban(LBPertanyaan.SelectedIndex)
            f.BelumBerubah.Add(item)
            f.TBPilihanJ.Text += item
            If cur < jumlah Then
                f.TBPilihanJ.Text += vbNewLine
            End If
            cur += 1
        Next
        f.BTambah.Text = "Ubah Pertanyaan"
        f.Show()
        Form_Fade.NamaForm = f
    End Sub

    Private Sub BHapus_Click(sender As Object, e As EventArgs) Handles BHapus.Click
        If KotakPesan.Show(Me, "Apa Anda yakin ingin menghapus Pertanyaan yang terpilih ?", "Hapus Pertanyaan", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) = KotakPesan.HasilPesan.Ya Then
            If Not Hapus(LBPertanyaan.SelectedIndices) Then
                Exit Sub
            End If
            MuatData()
            NonAktifkan(BUbah)
            NonAktifkan(BHapus)
            berubah = True
            NotificationManager.Show(Me, "Pertanyaan terpilih telah dihapus", Color.Blue, 2000)
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If warna = False Then
            If tambahsudah = False Then
                BTambah.FlatAppearance.BorderColor = Color.Green
                BTambah.FlatAppearance.BorderSize = 4
            End If
            warna = True
        Else
            If tambahsudah = False Then
                BTambah.FlatAppearance.BorderColor = Color.SteelBlue
                BTambah.FlatAppearance.BorderSize = 2
            End If
            warna = False
        End If

        If tambahsudah Then
            BTambah.FlatAppearance.BorderColor = Color.SteelBlue
            BTambah.FlatAppearance.BorderSize = 2
        End If

        If tambahsudah = True Then
            Timer1.Enabled = False
        End If
    End Sub
End Class