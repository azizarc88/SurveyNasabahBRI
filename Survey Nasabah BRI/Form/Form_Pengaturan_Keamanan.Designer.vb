﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pengaturan_Keamanan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Pengaturan_Keamanan))
        Me.BBatal = New System.Windows.Forms.Button()
        Me.BOk = New System.Windows.Forms.Button()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.BAturEnkripsi = New System.Windows.Forms.Button()
        Me.CBIdent = New System.Windows.Forms.CheckBox()
        Me.CBAcak = New System.Windows.Forms.CheckBox()
        Me.BAturPass = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PKunci = New System.Windows.Forms.Panel()
        Me.BBatalKunci = New System.Windows.Forms.Button()
        Me.BOkKunci = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TBKunciLama = New System.Windows.Forms.TextBox()
        Me.TBKunciBaru = New System.Windows.Forms.TextBox()
        Me.TBUlangiKunci = New System.Windows.Forms.TextBox()
        Me.PPassword = New System.Windows.Forms.Panel()
        Me.BBatalPass = New System.Windows.Forms.Button()
        Me.BOkPass = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TBPassLama = New System.Windows.Forms.TextBox()
        Me.TBPassBaru = New System.Windows.Forms.TextBox()
        Me.TBPassUlangi = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PKunci.SuspendLayout()
        Me.PPassword.SuspendLayout()
        Me.SuspendLayout()
        '
        'BBatal
        '
        Me.BBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatal.BackColor = System.Drawing.Color.MistyRose
        Me.BBatal.CausesValidation = False
        Me.BBatal.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatal.FlatAppearance.BorderSize = 2
        Me.BBatal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatal.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatal.Location = New System.Drawing.Point(460, 268)
        Me.BBatal.Margin = New System.Windows.Forms.Padding(8)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(204, 72)
        Me.BBatal.TabIndex = 44
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = False
        '
        'BOk
        '
        Me.BOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BOk.BackColor = System.Drawing.Color.White
        Me.BOk.CausesValidation = False
        Me.BOk.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BOk.FlatAppearance.BorderSize = 2
        Me.BOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BOk.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BOk.Location = New System.Drawing.Point(17, 268)
        Me.BOk.Margin = New System.Windows.Forms.Padding(8)
        Me.BOk.Name = "BOk"
        Me.BOk.Size = New System.Drawing.Size(204, 72)
        Me.BOk.TabIndex = 45
        Me.BOk.Text = "OK"
        Me.BOk.UseVisualStyleBackColor = False
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(17, 11)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(8, 0, 8, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(647, 44)
        Me.LJudul.TabIndex = 46
        Me.LJudul.Text = "PENGATURAN KEAMANAN"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'BAturEnkripsi
        '
        Me.BAturEnkripsi.BackColor = System.Drawing.Color.White
        Me.BAturEnkripsi.CausesValidation = False
        Me.BAturEnkripsi.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BAturEnkripsi.FlatAppearance.BorderSize = 2
        Me.BAturEnkripsi.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BAturEnkripsi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BAturEnkripsi.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BAturEnkripsi.Font = New System.Drawing.Font("Century Gothic", 14.0!)
        Me.BAturEnkripsi.Location = New System.Drawing.Point(17, 78)
        Me.BAturEnkripsi.Margin = New System.Windows.Forms.Padding(8)
        Me.BAturEnkripsi.Name = "BAturEnkripsi"
        Me.BAturEnkripsi.Size = New System.Drawing.Size(308, 60)
        Me.BAturEnkripsi.TabIndex = 45
        Me.BAturEnkripsi.Text = "Atur Kunci Enkripsi"
        Me.BAturEnkripsi.UseVisualStyleBackColor = False
        '
        'CBIdent
        '
        Me.CBIdent.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBIdent.BackColor = System.Drawing.Color.White
        Me.CBIdent.Checked = True
        Me.CBIdent.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBIdent.Enabled = False
        Me.CBIdent.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBIdent.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBIdent.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBIdent.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBIdent.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBIdent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBIdent.Location = New System.Drawing.Point(17, 154)
        Me.CBIdent.Margin = New System.Windows.Forms.Padding(8)
        Me.CBIdent.Name = "CBIdent"
        Me.CBIdent.Size = New System.Drawing.Size(647, 41)
        Me.CBIdent.TabIndex = 47
        Me.CBIdent.Text = "Aktifkan identitas berkas jawaban (menghindari pengeditan jawaban)"
        Me.CBIdent.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBIdent.UseVisualStyleBackColor = False
        '
        'CBAcak
        '
        Me.CBAcak.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBAcak.BackColor = System.Drawing.Color.White
        Me.CBAcak.Checked = True
        Me.CBAcak.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBAcak.Enabled = False
        Me.CBAcak.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBAcak.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBAcak.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBAcak.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBAcak.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBAcak.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBAcak.Location = New System.Drawing.Point(17, 211)
        Me.CBAcak.Margin = New System.Windows.Forms.Padding(8)
        Me.CBAcak.Name = "CBAcak"
        Me.CBAcak.Size = New System.Drawing.Size(647, 41)
        Me.CBAcak.TabIndex = 47
        Me.CBAcak.Text = "Aktifkan pengacakan karakter enkripsi (program = ############)"
        Me.CBAcak.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBAcak.UseVisualStyleBackColor = False
        '
        'BAturPass
        '
        Me.BAturPass.BackColor = System.Drawing.Color.White
        Me.BAturPass.CausesValidation = False
        Me.BAturPass.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BAturPass.FlatAppearance.BorderSize = 2
        Me.BAturPass.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BAturPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BAturPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BAturPass.Font = New System.Drawing.Font("Century Gothic", 14.0!)
        Me.BAturPass.Location = New System.Drawing.Point(356, 78)
        Me.BAturPass.Margin = New System.Windows.Forms.Padding(8)
        Me.BAturPass.Name = "BAturPass"
        Me.BAturPass.Size = New System.Drawing.Size(308, 60)
        Me.BAturPass.TabIndex = 45
        Me.BAturPass.Text = "Atur Password Utama"
        Me.BAturPass.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-3, 354)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 42
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-3, -11)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 43
        Me.PictureBox1.TabStop = False
        '
        'PKunci
        '
        Me.PKunci.BackColor = System.Drawing.Color.SteelBlue
        Me.PKunci.Controls.Add(Me.BBatalKunci)
        Me.PKunci.Controls.Add(Me.BOkKunci)
        Me.PKunci.Controls.Add(Me.Label1)
        Me.PKunci.Controls.Add(Me.Label4)
        Me.PKunci.Controls.Add(Me.Label3)
        Me.PKunci.Controls.Add(Me.Label2)
        Me.PKunci.Controls.Add(Me.TBKunciLama)
        Me.PKunci.Controls.Add(Me.TBKunciBaru)
        Me.PKunci.Controls.Add(Me.TBUlangiKunci)
        Me.PKunci.Location = New System.Drawing.Point(0, 400)
        Me.PKunci.Name = "PKunci"
        Me.PKunci.Size = New System.Drawing.Size(681, 359)
        Me.PKunci.TabIndex = 48
        Me.PKunci.Visible = False
        '
        'BBatalKunci
        '
        Me.BBatalKunci.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatalKunci.BackColor = System.Drawing.Color.White
        Me.BBatalKunci.CausesValidation = False
        Me.BBatalKunci.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatalKunci.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatalKunci.FlatAppearance.BorderSize = 2
        Me.BBatalKunci.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatalKunci.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatalKunci.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatalKunci.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatalKunci.Location = New System.Drawing.Point(435, 255)
        Me.BBatalKunci.Margin = New System.Windows.Forms.Padding(4)
        Me.BBatalKunci.Name = "BBatalKunci"
        Me.BBatalKunci.Size = New System.Drawing.Size(137, 52)
        Me.BBatalKunci.TabIndex = 33
        Me.BBatalKunci.Text = "Batal"
        Me.BBatalKunci.UseVisualStyleBackColor = False
        '
        'BOkKunci
        '
        Me.BOkKunci.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BOkKunci.BackColor = System.Drawing.Color.White
        Me.BOkKunci.CausesValidation = False
        Me.BOkKunci.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BOkKunci.FlatAppearance.BorderSize = 2
        Me.BOkKunci.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BOkKunci.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BOkKunci.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BOkKunci.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BOkKunci.Location = New System.Drawing.Point(124, 255)
        Me.BOkKunci.Margin = New System.Windows.Forms.Padding(4)
        Me.BOkKunci.Name = "BOkKunci"
        Me.BOkKunci.Size = New System.Drawing.Size(137, 52)
        Me.BOkKunci.TabIndex = 34
        Me.BOkKunci.Text = "Ok"
        Me.BOkKunci.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(118, 32)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(454, 47)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Atur Kunci Enkripsi"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label4
        '
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(120, 102)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(136, 30)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "Kunci Lama :"
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(120, 146)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(136, 30)
        Me.Label3.TabIndex = 31
        Me.Label3.Text = "Kunci Baru:"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(120, 191)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(136, 30)
        Me.Label2.TabIndex = 31
        Me.Label2.Text = "Ulangi Kunci :"
        '
        'TBKunciLama
        '
        Me.TBKunciLama.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBKunciLama.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBKunciLama.Location = New System.Drawing.Point(264, 100)
        Me.TBKunciLama.Margin = New System.Windows.Forms.Padding(4)
        Me.TBKunciLama.Name = "TBKunciLama"
        Me.TBKunciLama.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TBKunciLama.Size = New System.Drawing.Size(308, 32)
        Me.TBKunciLama.TabIndex = 30
        '
        'TBKunciBaru
        '
        Me.TBKunciBaru.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBKunciBaru.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBKunciBaru.Location = New System.Drawing.Point(264, 144)
        Me.TBKunciBaru.Margin = New System.Windows.Forms.Padding(4)
        Me.TBKunciBaru.Name = "TBKunciBaru"
        Me.TBKunciBaru.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TBKunciBaru.Size = New System.Drawing.Size(308, 32)
        Me.TBKunciBaru.TabIndex = 30
        '
        'TBUlangiKunci
        '
        Me.TBUlangiKunci.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBUlangiKunci.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBUlangiKunci.Location = New System.Drawing.Point(264, 189)
        Me.TBUlangiKunci.Margin = New System.Windows.Forms.Padding(4)
        Me.TBUlangiKunci.Name = "TBUlangiKunci"
        Me.TBUlangiKunci.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TBUlangiKunci.Size = New System.Drawing.Size(308, 32)
        Me.TBUlangiKunci.TabIndex = 30
        '
        'PPassword
        '
        Me.PPassword.BackColor = System.Drawing.Color.SteelBlue
        Me.PPassword.Controls.Add(Me.BBatalPass)
        Me.PPassword.Controls.Add(Me.BOkPass)
        Me.PPassword.Controls.Add(Me.Label5)
        Me.PPassword.Controls.Add(Me.Label6)
        Me.PPassword.Controls.Add(Me.Label7)
        Me.PPassword.Controls.Add(Me.Label8)
        Me.PPassword.Controls.Add(Me.TBPassLama)
        Me.PPassword.Controls.Add(Me.TBPassBaru)
        Me.PPassword.Controls.Add(Me.TBPassUlangi)
        Me.PPassword.Location = New System.Drawing.Point(0, 400)
        Me.PPassword.Name = "PPassword"
        Me.PPassword.Size = New System.Drawing.Size(681, 359)
        Me.PPassword.TabIndex = 49
        Me.PPassword.Visible = False
        '
        'BBatalPass
        '
        Me.BBatalPass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatalPass.BackColor = System.Drawing.Color.White
        Me.BBatalPass.CausesValidation = False
        Me.BBatalPass.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BBatalPass.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatalPass.FlatAppearance.BorderSize = 2
        Me.BBatalPass.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatalPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatalPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatalPass.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatalPass.Location = New System.Drawing.Point(435, 255)
        Me.BBatalPass.Margin = New System.Windows.Forms.Padding(4)
        Me.BBatalPass.Name = "BBatalPass"
        Me.BBatalPass.Size = New System.Drawing.Size(137, 52)
        Me.BBatalPass.TabIndex = 33
        Me.BBatalPass.Text = "Batal"
        Me.BBatalPass.UseVisualStyleBackColor = False
        '
        'BOkPass
        '
        Me.BOkPass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BOkPass.BackColor = System.Drawing.Color.White
        Me.BOkPass.CausesValidation = False
        Me.BOkPass.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BOkPass.FlatAppearance.BorderSize = 2
        Me.BOkPass.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BOkPass.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BOkPass.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BOkPass.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BOkPass.Location = New System.Drawing.Point(124, 255)
        Me.BOkPass.Margin = New System.Windows.Forms.Padding(4)
        Me.BOkPass.Name = "BOkPass"
        Me.BOkPass.Size = New System.Drawing.Size(137, 52)
        Me.BOkPass.TabIndex = 34
        Me.BOkPass.Text = "Ok"
        Me.BOkPass.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label5.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(118, 32)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(454, 47)
        Me.Label5.TabIndex = 32
        Me.Label5.Text = "Atur Password Utama"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label6
        '
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(120, 102)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(181, 30)
        Me.Label6.TabIndex = 31
        Me.Label6.Text = "Password Lama :"
        '
        'Label7
        '
        Me.Label7.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(120, 146)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(181, 30)
        Me.Label7.TabIndex = 31
        Me.Label7.Text = "Password Baru :"
        '
        'Label8
        '
        Me.Label8.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(120, 191)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(181, 30)
        Me.Label8.TabIndex = 31
        Me.Label8.Text = "Ulangi Password :"
        '
        'TBPassLama
        '
        Me.TBPassLama.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPassLama.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBPassLama.Location = New System.Drawing.Point(309, 100)
        Me.TBPassLama.Margin = New System.Windows.Forms.Padding(4)
        Me.TBPassLama.Name = "TBPassLama"
        Me.TBPassLama.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TBPassLama.Size = New System.Drawing.Size(263, 32)
        Me.TBPassLama.TabIndex = 30
        '
        'TBPassBaru
        '
        Me.TBPassBaru.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPassBaru.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBPassBaru.Location = New System.Drawing.Point(309, 144)
        Me.TBPassBaru.Margin = New System.Windows.Forms.Padding(4)
        Me.TBPassBaru.Name = "TBPassBaru"
        Me.TBPassBaru.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TBPassBaru.Size = New System.Drawing.Size(263, 32)
        Me.TBPassBaru.TabIndex = 30
        '
        'TBPassUlangi
        '
        Me.TBPassUlangi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPassUlangi.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBPassUlangi.Location = New System.Drawing.Point(309, 189)
        Me.TBPassUlangi.Margin = New System.Windows.Forms.Padding(4)
        Me.TBPassUlangi.Name = "TBPassUlangi"
        Me.TBPassUlangi.PasswordChar = Global.Microsoft.VisualBasic.ChrW(9679)
        Me.TBPassUlangi.Size = New System.Drawing.Size(263, 32)
        Me.TBPassUlangi.TabIndex = 30
        '
        'Timer1
        '
        Me.Timer1.Interval = 800
        '
        'Form_Pengaturan_Keamanan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(681, 359)
        Me.Controls.Add(Me.PPassword)
        Me.Controls.Add(Me.PKunci)
        Me.Controls.Add(Me.CBAcak)
        Me.Controls.Add(Me.CBIdent)
        Me.Controls.Add(Me.LJudul)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BAturPass)
        Me.Controls.Add(Me.BAturEnkripsi)
        Me.Controls.Add(Me.BOk)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_Pengaturan_Keamanan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Pengaturan_Keamanan"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PKunci.ResumeLayout(False)
        Me.PKunci.PerformLayout()
        Me.PPassword.ResumeLayout(False)
        Me.PPassword.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BBatal As System.Windows.Forms.Button
    Friend WithEvents BOk As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents BAturEnkripsi As System.Windows.Forms.Button
    Friend WithEvents CBIdent As System.Windows.Forms.CheckBox
    Friend WithEvents CBAcak As System.Windows.Forms.CheckBox
    Friend WithEvents BAturPass As System.Windows.Forms.Button
    Friend WithEvents PKunci As System.Windows.Forms.Panel
    Friend WithEvents BBatalKunci As System.Windows.Forms.Button
    Friend WithEvents BOkKunci As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TBKunciLama As System.Windows.Forms.TextBox
    Friend WithEvents TBKunciBaru As System.Windows.Forms.TextBox
    Friend WithEvents TBUlangiKunci As System.Windows.Forms.TextBox
    Friend WithEvents PPassword As System.Windows.Forms.Panel
    Friend WithEvents BBatalPass As System.Windows.Forms.Button
    Friend WithEvents BOkPass As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TBPassLama As System.Windows.Forms.TextBox
    Friend WithEvents TBPassBaru As System.Windows.Forms.TextBox
    Friend WithEvents TBPassUlangi As System.Windows.Forms.TextBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
