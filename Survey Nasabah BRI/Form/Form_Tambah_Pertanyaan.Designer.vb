﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Tambah_Pertanyaan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Tambah_Pertanyaan))
        Me.LJumlah = New System.Windows.Forms.Label()
        Me.BTutupOtomatis = New System.Windows.Forms.CheckBox()
        Me.BTambah = New System.Windows.Forms.Button()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.TBPilihanJ = New System.Windows.Forms.TextBox()
        Me.TBPertanyaan = New System.Windows.Forms.TextBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJumlah
        '
        Me.LJumlah.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJumlah.BackColor = System.Drawing.Color.Transparent
        Me.LJumlah.Location = New System.Drawing.Point(17, 316)
        Me.LJumlah.Name = "LJumlah"
        Me.LJumlah.Size = New System.Drawing.Size(172, 36)
        Me.LJumlah.TabIndex = 28
        '
        'BTutupOtomatis
        '
        Me.BTutupOtomatis.Appearance = System.Windows.Forms.Appearance.Button
        Me.BTutupOtomatis.BackColor = System.Drawing.Color.White
        Me.BTutupOtomatis.Checked = True
        Me.BTutupOtomatis.CheckState = System.Windows.Forms.CheckState.Checked
        Me.BTutupOtomatis.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.BTutupOtomatis.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.BTutupOtomatis.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BTutupOtomatis.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTutupOtomatis.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.BTutupOtomatis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BTutupOtomatis.Location = New System.Drawing.Point(251, 441)
        Me.BTutupOtomatis.Margin = New System.Windows.Forms.Padding(4)
        Me.BTutupOtomatis.Name = "BTutupOtomatis"
        Me.BTutupOtomatis.Size = New System.Drawing.Size(240, 41)
        Me.BTutupOtomatis.TabIndex = 27
        Me.BTutupOtomatis.Text = "Tutup Otomatis"
        Me.BTutupOtomatis.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BTutupOtomatis.UseVisualStyleBackColor = False
        '
        'BTambah
        '
        Me.BTambah.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTambah.BackColor = System.Drawing.Color.White
        Me.BTambah.CausesValidation = False
        Me.BTambah.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BTambah.FlatAppearance.BorderSize = 2
        Me.BTambah.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BTambah.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BTambah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTambah.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BTambah.Location = New System.Drawing.Point(16, 425)
        Me.BTambah.Margin = New System.Windows.Forms.Padding(4)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Size = New System.Drawing.Size(204, 72)
        Me.BTambah.TabIndex = 20
        Me.BTambah.Text = "Tambah Pertanyaan"
        Me.BTambah.UseVisualStyleBackColor = False
        '
        'BBatal
        '
        Me.BBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatal.BackColor = System.Drawing.Color.MistyRose
        Me.BBatal.CausesValidation = False
        Me.BBatal.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatal.FlatAppearance.BorderSize = 2
        Me.BBatal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatal.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatal.Location = New System.Drawing.Point(523, 425)
        Me.BBatal.Margin = New System.Windows.Forms.Padding(4)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(204, 72)
        Me.BBatal.TabIndex = 21
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label3.Location = New System.Drawing.Point(16, 288)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(225, 75)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Pilihan Jawaban:"
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label2.Location = New System.Drawing.Point(16, 94)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(225, 30)
        Me.Label2.TabIndex = 26
        Me.Label2.Text = "Pertanyaan:"
        '
        'LJudul
        '
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.Location = New System.Drawing.Point(16, 12)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(711, 47)
        Me.LJudul.TabIndex = 24
        Me.LJudul.Text = "Tambah Pertanyaan"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TBPilihanJ
        '
        Me.TBPilihanJ.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPilihanJ.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBPilihanJ.Location = New System.Drawing.Point(249, 288)
        Me.TBPilihanJ.Margin = New System.Windows.Forms.Padding(4)
        Me.TBPilihanJ.Multiline = True
        Me.TBPilihanJ.Name = "TBPilihanJ"
        Me.TBPilihanJ.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBPilihanJ.Size = New System.Drawing.Size(475, 116)
        Me.TBPilihanJ.TabIndex = 19
        '
        'TBPertanyaan
        '
        Me.TBPertanyaan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.TBPertanyaan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBPertanyaan.Location = New System.Drawing.Point(249, 89)
        Me.TBPertanyaan.Margin = New System.Windows.Forms.Padding(4)
        Me.TBPertanyaan.Multiline = True
        Me.TBPertanyaan.Name = "TBPertanyaan"
        Me.TBPertanyaan.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.TBPertanyaan.Size = New System.Drawing.Size(475, 172)
        Me.TBPertanyaan.TabIndex = 18
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-5, 514)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(816, 13)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 22
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-5, -8)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(816, 13)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 23
        Me.PictureBox1.TabStop = False
        '
        'Form_Tambah_Pertanyaan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 17.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(743, 519)
        Me.Controls.Add(Me.LJumlah)
        Me.Controls.Add(Me.BTutupOtomatis)
        Me.Controls.Add(Me.BTambah)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LJudul)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.TBPilihanJ)
        Me.Controls.Add(Me.TBPertanyaan)
        Me.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "Form_Tambah_Pertanyaan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Tambah_Pertanyaan"
        Me.TopMost = True
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LJumlah As System.Windows.Forms.Label
    Friend WithEvents BTutupOtomatis As System.Windows.Forms.CheckBox
    Friend WithEvents BTambah As System.Windows.Forms.Button
    Friend WithEvents BBatal As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents TBPilihanJ As System.Windows.Forms.TextBox
    Friend WithEvents TBPertanyaan As System.Windows.Forms.TextBox
End Class
