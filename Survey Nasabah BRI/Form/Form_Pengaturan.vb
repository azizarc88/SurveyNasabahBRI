﻿Imports AnimasiUI.Class_Animasi
Imports Transitions
Imports SkinApp.Gambar
Imports MessageHandle

Public Class Form_Pengaturan
    Private tombol As Button = Nothing
    Private a As String = Chr(13)
    Private warna As Boolean
    Private pemilik As Boolean
    Public keamanandone As Boolean = False
    Public passsudah As Boolean = False
    Public kuncisudah As Boolean = False
    Private tersimpan As Boolean = False

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Trans(teks As String, kontrol As Object)
        Dim t As Transition = New Transition(New TransitionType_Linear(200))
        t.add(kontrol, "Text", teks)
        t.run()
    End Sub

    Public Sub SetCheck(sender As Object)
        If sender.Checked = True Then
            sender.Image = AmbilGambar("centang")
        Else
            sender.Image = AmbilGambar("uncentang")
        End If
    End Sub

    Private Sub Form_Pengaturan_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Horizontal_Kiri, OpsiGaya.Tutup)
        Form_MenuUtama.Focus()
        Me.Dispose()
    End Sub

    Private Sub Form_Pengaturan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.BackColor = Color.White
        Me.Location = New Point(0, 0)
        Me.Size = Screen.PrimaryScreen.Bounds.Size
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Horizontal_Kanan, OpsiGaya.Buka)
        If Form_MenuUtama.pengaturanfirst = True Then
            pemilik = True
            Timer1.Enabled = True
        End If
    End Sub

    Private Sub BPUtama_Click(sender As Object, e As EventArgs) Handles BPUtama.Click
        If tombol IsNot BPUtama Then
            Trans("Pengaturan Utama berisi opsi opsi setelan tentang program secara keseluruhan, seperti tingkah laku program ketika pertama kali dijalankan dan ketika dioperasikan." + Environment.NewLine + "Opsi opsi:" + a _
                  + "- Tampilkan teks selamat datang" + a _
                  + "  Jika opsi ini aktif, maka program akan menampilkan teks selamat datang ketika program pertama kali dijalankan" + a _
                  + "- Fungsikan animasi tombol di Menu Utama" + a _
                  + "  Jika opsi ini aktif, maka tombol tombol yang ada di Menu Utama akan memiliki animasi ketika ditekan / dihover / diklik" + a _
                  + "- Fungsikan animasi tombol ke bagian Menu Survei" + a _
                  + "  Jika opsi ini aktif, maka pada tombol yang berfungsi untuk pergi ke layar Menu Survei akan memiliki animasi" + a _
                  + "- Fungsikan animasi tombol ke bagian Pertanyaan" + a _
                  + "  Jika opsi ini aktif, maka pada tombol Mulai Survei yang berfungsi untuk memulai survei akan memiliki animasi" + a, LKeterangan)
            tombol = BPUtama
            Exit Sub
        End If

        Form_Fade.Show()
        Form_Pengaturan_Utama.Show()
        Form_Fade.NamaForm = Form_Pengaturan_Utama
    End Sub

    Private Sub BPKeamanan_Click(sender As Object, e As EventArgs) Handles BPKeamanan.Click
        If tombol IsNot BPKeamanan Then
            Trans("Pengaturan Keamanan berisi opsi opsi setelan tentang keamanan program, seperti keamanan database dan program." + Environment.NewLine + "Opsi opsi:" + a _
                  + "- Kunci Enkripsi" + a _
                  + "  Opsi ini berfungsi untuk mengatur kata kunci yang digunakan program untuk mengengkripsi database" + a _
                  + "- Password Utama" + a _
                  + "  Opsi ini berfungsi untuk mengatur Password utama program, yang digunakan untuk menutup layar survei, mereset ulang hasil survei, dll." + a _
                  + "- Aktifkan identitas berkas jawaban" + a _
                  + "  Jika opsi ini aktif, maka berkas database akan memiliki tanda tangan sendiri, sehingga tidak akan dimuat datanya jika berkas telah diubah" + a _
                  + "- Aktifkan pengacakan karakter enkripsi" + a _
                  + "  Jika opsi ini aktif, maka hasil dari algoritma pengenkripsian data akan jauh berbeda dengan data asli" + a, LKeterangan)
            tombol = BPKeamanan
            Exit Sub
        End If
        BPKeamanan.FlatAppearance.BorderColor = Color.SteelBlue
        BPKeamanan.FlatAppearance.BorderSize = 2
        Form_Fade.Show()
        Form_Pengaturan_Keamanan.Show()
        Form_Fade.NamaForm = Form_Pengaturan_Keamanan
    End Sub

    Private Sub BPSurvey_Click(sender As Object, e As EventArgs) Handles BPSurvey.Click
        If tombol IsNot BPSurvey Then
            Trans("Pengaturan Survei berisi opsi opsi setelan tentang tingkah laku program saat menjalankan survei." + Environment.NewLine + "Opsi opsi:" + a _
                  + "- Tutup otomatis jendela Password ketika diam selama beberapa detik" + a _
                  + "  Opsi ini berfungsi untuk mengatur lama jendela Password akan ditampilkan ketika Menu Survei akan ditutup" + a _
                  + "- Selesaikan otomatis survei ketika diam selama beberapa detik" + a _
                  + "  Opsi ini berfungsi untuk mengotomatisasikan apakah program akan kembali ke Menu Survei jika survei yang sedang dilakukan tidak mengalami respon dari responden selama beberapa detik" + a _
                  + "- Gaya transisi" + a _
                  + "  Pilihan ini berfungsi untuk mengatur gaya animasi antar pertanyaan ketika survei dijalankan" + a, LKeterangan)
            tombol = BPSurvey
            Exit Sub
        End If

        Form_Fade.Show()
        Form_Pengaturan_Survey.Show()
        Form_Fade.NamaForm = Form_Pengaturan_Survey
    End Sub

    Private Sub BPLaporan_Click(sender As Object, e As EventArgs) Handles BPLaporan.Click
        If tombol IsNot BPLaporan Then
            Trans("Pengaturan Laporan berisi opsi opsi setelan tentang hasil survei yang akan disimpan, seperti tampilan hasil laporan, alamat penyimpanan." + Environment.NewLine + "Opsi opsi:" + a _
                  + "- Aktifkan statistik jawaban untuk semua pertanyaan" + a _
                  + "  Jika opsi ini aktif, maka program akan menyertakan statistik jumlah jawaban untuk tiap tiap pertanyaan yang dipilih" + a _
                  + "- Alamat" + a _
                  + "  Opsi ini berfungsi untuk mengatur di mana alamat berkas laporan akan disimpan" + a _
                  + "- Model tabel laporan" + a _
                  + "  Pilihan ini berisi opsi seperti apa tabel yang akan ditampilkan di dalam laporan" + a _
                  + "- Perataan isi tabel" + a _
                  + "  Opsi ini berfungsi untuk mengatur perataan isi dari tabel" + a, LKeterangan)
            tombol = BPLaporan
            Exit Sub
        End If

        Form_Fade.Show()
        Form_Pengaturan_Laporan.Show()
        Form_Fade.NamaForm = Form_Pengaturan_Laporan
    End Sub

    Private Sub BPPemilik_Click(sender As Object, e As EventArgs) Handles BPPemilik.Click
        If tombol IsNot BPPemilik Then
            Trans("Pengaturan Pemilik berisi opsi opsi tentang keterangan pemilik program ini, seperti nama, alamat, no telp Bank." + Environment.NewLine + "Opsi opsi:" + a _
                  + "- Nama Bank" + a _
                  + "  Opsi untuk mengatur nama Bank (BANK RAKYAT INDONESIA)" + a _
                  + "- Keterangan Bank" + a _
                  + "  Opsi untuk mengatur keterangan Bank (KCP Pusat)" + a _
                  + "- Alamat Bank" + a _
                  + "  Opsi untuk mengatur alamat Bank" + a _
                  + "- No Telepon" + a _
                  + "  Opsi untuk mengatur nomor telepon Bank" + a _
                  + "- Alamat Email" + a _
                  + "  Opsi untuk mengatur alamat email Bank" + a _
                  + "- Alamat Website" + a _
                  + "  Opsi untuk mengatur alamat website Bank" + a _
                  + "NB: Opsi opsi ini akan ditampilkan pada bagian kop Laporan.", LKeterangan)
            tombol = BPPemilik
            Exit Sub
        End If
        pemilik = False
        BPPemilik.FlatAppearance.BorderColor = Color.SteelBlue
        BPPemilik.FlatAppearance.BorderSize = 2
        Form_Fade.Show()
        Form_Pengaturan_Pemilik.Show()
        Form_Fade.NamaForm = Form_Pengaturan_Pemilik
    End Sub

    Private Sub BSimpan_Click(sender As Object, e As EventArgs) Handles BSimpan.Click
        Pengaturan.SimpanPengaturan()
        NotificationManager.Show(Me, "Pengaturan telah tersimpan", Color.SteelBlue, 4000)
        If Timer1.Enabled = False Then
            tersimpan = True
        End If
    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        If Timer1.Enabled = True Then
            NotificationManager.Show(Me, "Tidak dapat membatalkan karena alasan keamanan, silakan atur dahulu bagian yang masih berwarna hijau.", Color.Red, 4000)
            Exit Sub
        End If
        If tersimpan = False And Form_MenuUtama.pengaturanfirst = True Then
            NotificationManager.Show(Me, "Tidak dapat membatalkan karena alasan keamanan, silakan simpan pengaturan terlebih dahulu.", Color.Red, 3000)
            Exit Sub
        End If
        Pengaturan.BacaPengaturan()
        Me.Close()
        If Pengaturan.animmenuutama = True Then
            Try
                Form_MenuUtama.thmain.Resume()
            Catch ex As Exception
            End Try
        End If

        Form_MenuUtama.pengaturanfirst = False

        If Form_MenuUtama.pengaturanfirst = True Then
            KotakPesan.Show(Me, "Anda belum membuat daftar pertanyaan yang akan digunakan untuk melakukan survei" + Environment.NewLine + "Silakan buat terlebih dahulu melalui menu Pengolah Data.", "Tidak Ada Pertanyaan", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If warna = False Then
            If kuncisudah = False Or passsudah = False Then
                BPKeamanan.FlatAppearance.BorderColor = Color.Green
                BPKeamanan.FlatAppearance.BorderSize = 4
            End If
            If pemilik = True Then
                BPPemilik.FlatAppearance.BorderColor = Color.Green
                BPPemilik.FlatAppearance.BorderSize = 4
            End If
            warna = True
        Else
            If kuncisudah = False Or passsudah = False Then
                BPKeamanan.FlatAppearance.BorderColor = Color.SteelBlue
                BPKeamanan.FlatAppearance.BorderSize = 2
            End If
            If pemilik = True Then
                BPPemilik.FlatAppearance.BorderColor = Color.SteelBlue
                BPPemilik.FlatAppearance.BorderSize = 2
            End If
            warna = False
        End If

        If kuncisudah And passsudah Then
            BPKeamanan.FlatAppearance.BorderColor = Color.SteelBlue
            BPKeamanan.FlatAppearance.BorderSize = 2
        End If

        If ((kuncisudah = False Or passsudah = False) Or pemilik) = False Then
            Timer1.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        KotakPesan.Show(Me, "Survei Nasabah Bank Versi " + Application.ProductVersion + Environment.NewLine + Environment.NewLine + "Dibuat Oleh: Aziz Nur Ariffianto" + Environment.NewLine + Environment.NewLine + "az_izarc@windowslive.com" + Environment.NewLine + "085726411955", "Info Aplikasi", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
    End Sub
End Class