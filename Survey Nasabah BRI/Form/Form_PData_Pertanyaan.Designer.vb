﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_PData_Pertanyaan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LBPertanyaan = New System.Windows.Forms.ListBox()
        Me.LBJawaban = New System.Windows.Forms.ListBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LJumlah = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BUbah = New System.Windows.Forms.Button()
        Me.BHapus = New System.Windows.Forms.Button()
        Me.BTambah = New System.Windows.Forms.Button()
        Me.BSimpan = New System.Windows.Forms.Button()
        Me.BKembali = New System.Windows.Forms.Button()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'LBPertanyaan
        '
        Me.LBPertanyaan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBPertanyaan.BackColor = System.Drawing.Color.White
        Me.LBPertanyaan.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBPertanyaan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.LBPertanyaan.FormattingEnabled = True
        Me.LBPertanyaan.HorizontalScrollbar = True
        Me.LBPertanyaan.IntegralHeight = False
        Me.LBPertanyaan.ItemHeight = 23
        Me.LBPertanyaan.Location = New System.Drawing.Point(15, 26)
        Me.LBPertanyaan.Name = "LBPertanyaan"
        Me.LBPertanyaan.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
        Me.LBPertanyaan.Size = New System.Drawing.Size(845, 162)
        Me.LBPertanyaan.TabIndex = 0
        '
        'LBJawaban
        '
        Me.LBJawaban.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LBJawaban.BackColor = System.Drawing.Color.White
        Me.LBJawaban.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.LBJawaban.ColumnWidth = 500
        Me.LBJawaban.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.LBJawaban.FormattingEnabled = True
        Me.LBJawaban.IntegralHeight = False
        Me.LBJawaban.ItemHeight = 23
        Me.LBJawaban.Location = New System.Drawing.Point(14, 26)
        Me.LBJawaban.Name = "LBJawaban"
        Me.LBJawaban.Size = New System.Drawing.Size(824, 118)
        Me.LBJawaban.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.LJumlah)
        Me.GroupBox1.Controls.Add(Me.LBJawaban)
        Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox1.Font = New System.Drawing.Font("Moire", 12.0!)
        Me.GroupBox1.Location = New System.Drawing.Point(15, 195)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(851, 159)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Pilihan Jawaban Pertanyaan Terpilih"
        '
        'LJumlah
        '
        Me.LJumlah.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJumlah.Location = New System.Drawing.Point(638, 27)
        Me.LJumlah.Name = "LJumlah"
        Me.LJumlah.Size = New System.Drawing.Size(199, 116)
        Me.LJumlah.TabIndex = 1
        Me.LJumlah.TextAlign = System.Drawing.ContentAlignment.TopRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox2.Controls.Add(Me.LBPertanyaan)
        Me.GroupBox2.Controls.Add(Me.BUbah)
        Me.GroupBox2.Controls.Add(Me.BHapus)
        Me.GroupBox2.Controls.Add(Me.BTambah)
        Me.GroupBox2.Controls.Add(Me.GroupBox1)
        Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.GroupBox2.Font = New System.Drawing.Font("Moire", 12.0!)
        Me.GroupBox2.Location = New System.Drawing.Point(37, 103)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(875, 448)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Pertanyaan"
        '
        'BUbah
        '
        Me.BUbah.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.BUbah.BackColor = System.Drawing.Color.RosyBrown
        Me.BUbah.CausesValidation = False
        Me.BUbah.Enabled = False
        Me.BUbah.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BUbah.FlatAppearance.BorderSize = 2
        Me.BUbah.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BUbah.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BUbah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BUbah.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.BUbah.Location = New System.Drawing.Point(363, 373)
        Me.BUbah.Name = "BUbah"
        Me.BUbah.Size = New System.Drawing.Size(153, 55)
        Me.BUbah.TabIndex = 5
        Me.BUbah.Text = "Ubah Pertanyaan"
        Me.BUbah.UseVisualStyleBackColor = False
        '
        'BHapus
        '
        Me.BHapus.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BHapus.BackColor = System.Drawing.Color.RosyBrown
        Me.BHapus.CausesValidation = False
        Me.BHapus.Enabled = False
        Me.BHapus.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BHapus.FlatAppearance.BorderSize = 2
        Me.BHapus.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BHapus.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BHapus.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BHapus.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.BHapus.Location = New System.Drawing.Point(700, 373)
        Me.BHapus.Name = "BHapus"
        Me.BHapus.Size = New System.Drawing.Size(153, 55)
        Me.BHapus.TabIndex = 5
        Me.BHapus.Text = "Hapus Pertanyaan"
        Me.BHapus.UseVisualStyleBackColor = False
        '
        'BTambah
        '
        Me.BTambah.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTambah.BackColor = System.Drawing.Color.White
        Me.BTambah.CausesValidation = False
        Me.BTambah.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BTambah.FlatAppearance.BorderSize = 2
        Me.BTambah.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BTambah.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BTambah.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTambah.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.BTambah.Location = New System.Drawing.Point(24, 373)
        Me.BTambah.Name = "BTambah"
        Me.BTambah.Size = New System.Drawing.Size(153, 55)
        Me.BTambah.TabIndex = 5
        Me.BTambah.Text = "Tambah Pertanyaan"
        Me.BTambah.UseVisualStyleBackColor = False
        '
        'BSimpan
        '
        Me.BSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BSimpan.BackColor = System.Drawing.Color.White
        Me.BSimpan.CausesValidation = False
        Me.BSimpan.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BSimpan.FlatAppearance.BorderSize = 2
        Me.BSimpan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BSimpan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BSimpan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BSimpan.Location = New System.Drawing.Point(740, 570)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Size = New System.Drawing.Size(183, 68)
        Me.BSimpan.TabIndex = 10
        Me.BSimpan.Text = "Simpan Perubahan"
        Me.BSimpan.UseVisualStyleBackColor = False
        '
        'BKembali
        '
        Me.BKembali.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BKembali.BackColor = System.Drawing.Color.Gainsboro
        Me.BKembali.CausesValidation = False
        Me.BKembali.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BKembali.FlatAppearance.BorderSize = 2
        Me.BKembali.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BKembali.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BKembali.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BKembali.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BKembali.Location = New System.Drawing.Point(22, 570)
        Me.BKembali.Name = "BKembali"
        Me.BKembali.Size = New System.Drawing.Size(183, 68)
        Me.BKembali.TabIndex = 11
        Me.BKembali.Text = "< Kembali"
        Me.BKembali.UseVisualStyleBackColor = False
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(12, 10)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(919, 81)
        Me.LJudul.TabIndex = 9
        Me.LJudul.Text = "PENGOLAH PERTANYAAN"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Form_PData_Pertanyaan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(943, 658)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.BSimpan)
        Me.Controls.Add(Me.BKembali)
        Me.Controls.Add(Me.LJudul)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_PData_Pertanyaan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form_PData_Pertanyaan"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LBPertanyaan As System.Windows.Forms.ListBox
    Friend WithEvents LBJawaban As System.Windows.Forms.ListBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LJumlah As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents BUbah As System.Windows.Forms.Button
    Friend WithEvents BHapus As System.Windows.Forms.Button
    Friend WithEvents BTambah As System.Windows.Forms.Button
    Friend WithEvents BSimpan As System.Windows.Forms.Button
    Friend WithEvents BKembali As System.Windows.Forms.Button
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
End Class
