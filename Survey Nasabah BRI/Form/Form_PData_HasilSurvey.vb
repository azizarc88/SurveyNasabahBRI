﻿Imports AnimasiUI.Class_Animasi
Imports MessageHandle

Public Class Form_PData_HasilSurvey
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub Form_PData_HasilSurvey_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
        Me.Dispose()
    End Sub

    Private Sub UbahWarna(ByVal rows As DataGridViewRow, ByVal warna As Color)
        rows.Cells(0).Style.BackColor = warna
        rows.Cells(1).Style.BackColor = warna
        rows.Cells(2).Style.BackColor = warna
        rows.Cells(3).Style.BackColor = warna
    End Sub

    Public Sub Reset()
        If KotakPesan.Show(Me, "Apakah Anda ingin mereset ulang jawaban yang tersimpan ?, jika Anda melakukanya, semua jawaban dari partisipan akan terhapus secara permanen." + vbNewLine + vbNewLine + "Ingin melanjutkan ?", "Atur Ulang Jawaban", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Peringatan) = KotakPesan.HasilPesan.Ya Then
            IO.Directory.Delete(AlamatAppData + "\Jawaban", True)
            Threading.Thread.Sleep(500)
            Database.BacaJawaban(Me)
            MuatData()
            LPertanyaan.Text = "[Tidak ada pertanyaan yang terpilih]"
            LPil1.Text = ""
            LJ1.Text = ""
            LPil2.Text = ""
            LJ2.Text = ""
            LPil3.Text = ""
            LJ3.Text = ""
            LPil4.Text = ""
            LJ4.Text = ""
            LPil5.Text = ""
            LJ5.Text = ""
        End If
    End Sub

    Public Function GetTextTombol(ByVal text As String, ByVal idpertanyaan As String, Optional ByVal semua As Boolean = True) As String()
        Dim hasil As String() = Nothing
        Dim a As Integer = 0
        Dim p As String = "EROR"
        For Each s As String() In Pertanyaan
            If s(1) = idpertanyaan Then
                p = s(0)
                Exit For
            End If
            a += 1
        Next

        If semua = True Then
            Dim b As Integer = CInt(text.Replace("B", ""))
            Try
                hasil = {p, PilJawaban(a)(b - 1), PilJawaban(a).Length}
            Catch ex As Exception
                KotakPesan.Show(Me, "Terjadi galat saat memuat pertanyaan dan jawaban yang ada di Database kami.?" & vbNewLine & vbNewLine & "Pesan Galat: " & idpertanyaan & ", " & a & ", " & text, "Terjadi Galat", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror)
                Return {"GALAT"}
            End Try
        ElseIf semua = False Then
            hasil = {PilJawaban(a).Length}
        End If
        Return hasil
    End Function

    Public Function GetTextTombolAll(ByVal id As String) As String()
        Dim hasil As String()
        Dim a As Integer
        For Each s As String() In Pertanyaan
            If s(1) = id Then
                Exit For
            End If
            a += 1
        Next
        hasil = PilJawaban(a)
        Return hasil
    End Function

    Public Sub AturStatistik(ByVal id As String)
        Dim daftar As List(Of String()) = New List(Of String())
        Dim datatexttombol As String() = GetTextTombolAll(id)
        Dim _1, _2, _3, _4, _5, jumlah As Integer
        For Each s As String() In Jawaban
            If id = s(0) Then
                daftar.Add(s)
            End If
        Next

        For Each d As String() In daftar
            If d(2).Contains("1") Then
                _1 += 1
            ElseIf d(2).Contains("2") Then
                _2 += 1
            ElseIf d(2).Contains("3") Then
                _3 += 1
            ElseIf d(2).Contains("4") Then
                _4 += 1
            ElseIf d(2).Contains("5") Then
                _5 += 1
            End If
        Next

        jumlah = GetTextTombol("B1", id, False)(0)
        If jumlah = 2 Then
            LPil1.Text = ""
            LJ1.Text = ""
            LPil2.Text = datatexttombol(0) + " :"
            LJ2.Text = _1
            LPil3.Text = ""
            LJ3.Text = ""
            LPil4.Text = datatexttombol(1) + " :"
            LJ4.Text = _2
            LPil5.Text = ""
            LJ5.Text = ""
        ElseIf jumlah = 3 Then
            LPil1.Text = ""
            LJ1.Text = ""
            LPil2.Text = datatexttombol(0) + " :"
            LJ2.Text = _1
            LPil3.Text = datatexttombol(1) + " :"
            LJ3.Text = _2
            LPil4.Text = datatexttombol(2) + " :"
            LJ4.Text = _3
            LPil5.Text = ""
            LJ5.Text = ""
        ElseIf jumlah = 4 Then
            LPil1.Text = datatexttombol(0) + " :"
            LJ1.Text = _1
            LPil2.Text = datatexttombol(1) + " :"
            LJ2.Text = _2
            LPil3.Text = ""
            LJ3.Text = ""
            LPil4.Text = datatexttombol(2) + " :"
            LJ4.Text = _3
            LPil5.Text = datatexttombol(3) + " :"
            LJ5.Text = _4
        ElseIf jumlah = 5 Then
            LPil1.Text = datatexttombol(0) + " :"
            LJ1.Text = _1
            LPil2.Text = datatexttombol(1) + " :"
            LJ2.Text = _2
            LPil3.Text = datatexttombol(2) + " :"
            LJ3.Text = _3
            LPil4.Text = datatexttombol(3) + " :"
            LJ4.Text = _4
            LPil5.Text = datatexttombol(4) + " :"
            LJ5.Text = _5
        End If
    End Sub

    Private Sub MuatData()
        DGVJawaban.Rows.Clear()
        Dim i, w As String
        Dim j As String()
        Dim a As Integer = 0
        Dim b As Integer = 0
        For Each s As String() In Jawaban
            i = s(0)
            j = GetTextTombol(s(2), s(0))
            If j(0) = "GALAT" Then
                Exit For
            End If
            w = s(1)

            Try
                If BAturUlang.Enabled = False Then
                    BAturUlang.Enabled = True
                End If
                DGVJawaban.Rows.Add({i, j(0), w, j(1)})
                Integer.TryParse(i, b)
                If b Mod 2 = 0 Then
                    UbahWarna(DGVJawaban.Rows(a), Color.White)
                Else
                    UbahWarna(DGVJawaban.Rows(a), Color.WhiteSmoke)
                End If
                a += 1
            Catch ex As Exception
            End Try
        Next
        DGVJawaban.Sort(DGVJawaban.Columns(2), System.ComponentModel.ListSortDirection.Ascending)
    End Sub

    Private Sub Form_PD_HasilSurvey_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Location = New Point(0, 0)
        Me.Size = Screen.PrimaryScreen.Bounds.Size
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        MuatData()
        Database.BacaJawaban(Me)
        Database.BacaPertanyaan()
    End Sub

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        Me.Close()
        Form_PData.Focus()
    End Sub

    Private Sub DGVJawaban_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DGVJawaban.CellMouseClick
        LPertanyaan.Text = DGVJawaban.CurrentRow.Cells(1).Value
        AturStatistik(DGVJawaban.CurrentRow.Cells(0).Value)
    End Sub

    Private Sub BReport_Click(sender As Object, e As EventArgs) Handles BReport.Click
        If DGVJawaban.RowCount > 0 Then
            Form_Fade.Show()
            Form_Report.Show()
            Form_Fade.NamaForm = Form_Report
        Else
            KotakPesan.Show(Me, "Tidak ada jawaban untuk mengisi laporan survei nasabah, silakan mulai survei terlebih dahulu.", "Laporan Survei", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
        End If
    End Sub

    Private Sub BAturUlang_Click(sender As Object, e As EventArgs) Handles BAturUlang.Click
        Form_Fade.Show()
        Form_Password.Show()
        Form_Password.Fungsi = "Validasi"
        Form_Fade.NamaForm = Form_Password
    End Sub
End Class