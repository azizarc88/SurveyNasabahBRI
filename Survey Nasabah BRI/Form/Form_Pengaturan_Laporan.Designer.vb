﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pengaturan_Laporan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Pengaturan_Laporan))
        Me.LJudul = New System.Windows.Forms.Label()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.BOk = New System.Windows.Forms.Button()
        Me.CBStatistikJawaban = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CBModelTabel = New System.Windows.Forms.ComboBox()
        Me.CBPerataan = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TBAlamat = New System.Windows.Forms.TextBox()
        Me.BJelajag = New System.Windows.Forms.Button()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.Label2 = New System.Windows.Forms.Label()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(17, 11)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(8, 0, 8, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(647, 44)
        Me.LJudul.TabIndex = 51
        Me.LJudul.Text = "PENGATURAN LAPORAN"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'BBatal
        '
        Me.BBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatal.BackColor = System.Drawing.Color.MistyRose
        Me.BBatal.CausesValidation = False
        Me.BBatal.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatal.FlatAppearance.BorderSize = 2
        Me.BBatal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatal.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatal.Location = New System.Drawing.Point(460, 267)
        Me.BBatal.Margin = New System.Windows.Forms.Padding(8)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(204, 72)
        Me.BBatal.TabIndex = 49
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = False
        '
        'BOk
        '
        Me.BOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BOk.BackColor = System.Drawing.Color.White
        Me.BOk.CausesValidation = False
        Me.BOk.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BOk.FlatAppearance.BorderSize = 2
        Me.BOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BOk.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BOk.Location = New System.Drawing.Point(17, 267)
        Me.BOk.Margin = New System.Windows.Forms.Padding(8)
        Me.BOk.Name = "BOk"
        Me.BOk.Size = New System.Drawing.Size(204, 72)
        Me.BOk.TabIndex = 50
        Me.BOk.Text = "OK"
        Me.BOk.UseVisualStyleBackColor = False
        '
        'CBStatistikJawaban
        '
        Me.CBStatistikJawaban.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBStatistikJawaban.BackColor = System.Drawing.Color.White
        Me.CBStatistikJawaban.Checked = True
        Me.CBStatistikJawaban.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBStatistikJawaban.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBStatistikJawaban.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBStatistikJawaban.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBStatistikJawaban.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBStatistikJawaban.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBStatistikJawaban.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBStatistikJawaban.Location = New System.Drawing.Point(17, 74)
        Me.CBStatistikJawaban.Margin = New System.Windows.Forms.Padding(8)
        Me.CBStatistikJawaban.Name = "CBStatistikJawaban"
        Me.CBStatistikJawaban.Size = New System.Drawing.Size(647, 41)
        Me.CBStatistikJawaban.TabIndex = 52
        Me.CBStatistikJawaban.Text = "Aktifkan statistik jawaban untuk semua pertanyaan"
        Me.CBStatistikJawaban.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBStatistikJawaban.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label3.Location = New System.Drawing.Point(13, 178)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(226, 23)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Model tabel laporan :"
        '
        'CBModelTabel
        '
        Me.CBModelTabel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBModelTabel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBModelTabel.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.CBModelTabel.FormattingEnabled = True
        Me.CBModelTabel.Items.AddRange(New Object() {"Kotak Penuh", "Garis pada nomor dan judul kolom"})
        Me.CBModelTabel.Location = New System.Drawing.Point(245, 174)
        Me.CBModelTabel.Name = "CBModelTabel"
        Me.CBModelTabel.Size = New System.Drawing.Size(419, 31)
        Me.CBModelTabel.TabIndex = 53
        '
        'CBPerataan
        '
        Me.CBPerataan.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.CBPerataan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBPerataan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.CBPerataan.FormattingEnabled = True
        Me.CBPerataan.Items.AddRange(New Object() {"Kiri", "Tengah", "Kanan"})
        Me.CBPerataan.Location = New System.Drawing.Point(245, 218)
        Me.CBPerataan.Name = "CBPerataan"
        Me.CBPerataan.Size = New System.Drawing.Size(419, 31)
        Me.CBPerataan.TabIndex = 53
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label1.Location = New System.Drawing.Point(13, 222)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(192, 23)
        Me.Label1.TabIndex = 54
        Me.Label1.Text = "Perataan isi tabel :"
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-3, 353)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 47
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-3, -11)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 48
        Me.PictureBox1.TabStop = False
        '
        'TBAlamat
        '
        Me.TBAlamat.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.TBAlamat.Location = New System.Drawing.Point(114, 128)
        Me.TBAlamat.Name = "TBAlamat"
        Me.TBAlamat.Size = New System.Drawing.Size(437, 32)
        Me.TBAlamat.TabIndex = 55
        '
        'BJelajag
        '
        Me.BJelajag.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BJelajag.BackColor = System.Drawing.Color.White
        Me.BJelajag.CausesValidation = False
        Me.BJelajag.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BJelajag.FlatAppearance.BorderSize = 2
        Me.BJelajag.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BJelajag.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BJelajag.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BJelajag.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.BJelajag.Location = New System.Drawing.Point(562, 128)
        Me.BJelajag.Margin = New System.Windows.Forms.Padding(8)
        Me.BJelajag.Name = "BJelajag"
        Me.BJelajag.Size = New System.Drawing.Size(102, 30)
        Me.BJelajag.TabIndex = 50
        Me.BJelajag.Text = "Jelajah"
        Me.BJelajag.UseVisualStyleBackColor = False
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.DefaultExt = "pdf"
        Me.SaveFileDialog1.FileName = "Laporan Survei Nasabah"
        Me.SaveFileDialog1.Filter = "Portable Document File|*.pdf"
        Me.SaveFileDialog1.RestoreDirectory = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label2.Location = New System.Drawing.Point(13, 135)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(95, 23)
        Me.Label2.TabIndex = 54
        Me.Label2.Text = "Alamat :"
        '
        'Form_Pengaturan_Laporan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(681, 358)
        Me.Controls.Add(Me.TBAlamat)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.CBPerataan)
        Me.Controls.Add(Me.CBModelTabel)
        Me.Controls.Add(Me.CBStatistikJawaban)
        Me.Controls.Add(Me.LJudul)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BJelajag)
        Me.Controls.Add(Me.BOk)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_Pengaturan_Laporan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Pengaturan_Laporan"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents BBatal As System.Windows.Forms.Button
    Friend WithEvents BOk As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents CBStatistikJawaban As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents CBModelTabel As System.Windows.Forms.ComboBox
    Friend WithEvents CBPerataan As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TBAlamat As System.Windows.Forms.TextBox
    Friend WithEvents BJelajag As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
