﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pengaturan_Pemilik
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Pengaturan_Pemilik))
        Me.LJudul = New System.Windows.Forms.Label()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.BOk = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TBNama = New System.Windows.Forms.TextBox()
        Me.TBAlamat = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TBKeterangan = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TBNoTelp = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TBEmail = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TBWebsite = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(17, 11)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(8, 0, 8, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(647, 44)
        Me.LJudul.TabIndex = 14
        Me.LJudul.Text = "PENGATURAN PEMILIK"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'BBatal
        '
        Me.BBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatal.BackColor = System.Drawing.Color.MistyRose
        Me.BBatal.CausesValidation = False
        Me.BBatal.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatal.FlatAppearance.BorderSize = 2
        Me.BBatal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatal.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatal.Location = New System.Drawing.Point(460, 394)
        Me.BBatal.Margin = New System.Windows.Forms.Padding(8)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(204, 72)
        Me.BBatal.TabIndex = 7
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = False
        '
        'BOk
        '
        Me.BOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BOk.BackColor = System.Drawing.Color.White
        Me.BOk.CausesValidation = False
        Me.BOk.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BOk.FlatAppearance.BorderSize = 2
        Me.BOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BOk.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BOk.Location = New System.Drawing.Point(17, 394)
        Me.BOk.Margin = New System.Windows.Forms.Padding(8)
        Me.BOk.Name = "BOk"
        Me.BOk.Size = New System.Drawing.Size(204, 72)
        Me.BOk.TabIndex = 6
        Me.BOk.Text = "OK"
        Me.BOk.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-3, 480)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 47
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-3, -11)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 48
        Me.PictureBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label3.Location = New System.Drawing.Point(19, 82)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(145, 23)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "*Nama Bank :"
        '
        'TBNama
        '
        Me.TBNama.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBNama.Location = New System.Drawing.Point(220, 74)
        Me.TBNama.Margin = New System.Windows.Forms.Padding(8)
        Me.TBNama.Name = "TBNama"
        Me.TBNama.Size = New System.Drawing.Size(444, 37)
        Me.TBNama.TabIndex = 0
        '
        'TBAlamat
        '
        Me.TBAlamat.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBAlamat.Location = New System.Drawing.Point(220, 181)
        Me.TBAlamat.Margin = New System.Windows.Forms.Padding(8)
        Me.TBAlamat.Name = "TBAlamat"
        Me.TBAlamat.Size = New System.Drawing.Size(444, 37)
        Me.TBAlamat.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label1.Location = New System.Drawing.Point(19, 189)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(157, 23)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "*Alamat Bank :"
        '
        'TBKeterangan
        '
        Me.TBKeterangan.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBKeterangan.Location = New System.Drawing.Point(220, 127)
        Me.TBKeterangan.Margin = New System.Windows.Forms.Padding(8)
        Me.TBKeterangan.Name = "TBKeterangan"
        Me.TBKeterangan.Size = New System.Drawing.Size(444, 37)
        Me.TBKeterangan.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label2.Location = New System.Drawing.Point(19, 135)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(190, 23)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Keterangan Bank :"
        '
        'TBNoTelp
        '
        Me.TBNoTelp.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBNoTelp.Location = New System.Drawing.Point(220, 234)
        Me.TBNoTelp.Margin = New System.Windows.Forms.Padding(8)
        Me.TBNoTelp.Name = "TBNoTelp"
        Me.TBNoTelp.Size = New System.Drawing.Size(444, 37)
        Me.TBNoTelp.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label4.Location = New System.Drawing.Point(19, 242)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(180, 23)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "*Nomor Telepon :"
        '
        'TBEmail
        '
        Me.TBEmail.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBEmail.Location = New System.Drawing.Point(220, 287)
        Me.TBEmail.Margin = New System.Windows.Forms.Padding(8)
        Me.TBEmail.Name = "TBEmail"
        Me.TBEmail.Size = New System.Drawing.Size(444, 37)
        Me.TBEmail.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label5.Location = New System.Drawing.Point(19, 295)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(162, 23)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "*Alamat Email :"
        '
        'TBWebsite
        '
        Me.TBWebsite.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.TBWebsite.Location = New System.Drawing.Point(220, 340)
        Me.TBWebsite.Margin = New System.Windows.Forms.Padding(8)
        Me.TBWebsite.Name = "TBWebsite"
        Me.TBWebsite.Size = New System.Drawing.Size(444, 37)
        Me.TBWebsite.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label6.Location = New System.Drawing.Point(19, 348)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(185, 23)
        Me.Label6.TabIndex = 13
        Me.Label6.Text = "*Alamat Website :"
        '
        'Form_Pengaturan_Pemilik
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(681, 485)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.TBWebsite)
        Me.Controls.Add(Me.TBEmail)
        Me.Controls.Add(Me.TBNoTelp)
        Me.Controls.Add(Me.TBAlamat)
        Me.Controls.Add(Me.TBKeterangan)
        Me.Controls.Add(Me.TBNama)
        Me.Controls.Add(Me.LJudul)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BOk)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_Pengaturan_Pemilik"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Pengaturan_Pemilik"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents BBatal As System.Windows.Forms.Button
    Friend WithEvents BOk As System.Windows.Forms.Button
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents TBNama As System.Windows.Forms.TextBox
    Friend WithEvents TBAlamat As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TBKeterangan As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TBNoTelp As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents TBEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TBWebsite As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
End Class
