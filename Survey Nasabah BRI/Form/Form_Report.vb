﻿Imports AnimasiUI.Class_Animasi
Imports System.IO

Public Class Form_Report
    Private daftar As List(Of String) = New List(Of String)

    Public HasilSurvey As List(Of String()) = New List(Of String())
    Public DaftarPertanyaan As List(Of String()) = New List(Of String())
    Public tmulai, takhir As DateTime
    Dim ListSementara As List(Of String()) = New List(Of String())

    Dim tterkecil, tterbesar As Date
    Dim jumlah As Integer

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub SeleksiTanggal()
        Dim t As DateTime
        Dim data As String()
        For Each item As DataGridViewRow In Form_PData_HasilSurvey.DGVJawaban.Rows
            t = KonversiTanggalUni(item.Cells(2).Value.ToString)
            If t >= tmulai And t <= takhir Then
                data = {item.Cells(0).Value.ToString, item.Cells(1).Value.ToString, item.Cells(2).Value.ToString, item.Cells(3).Value.ToString}
                ListSementara.Add(data)
            End If
        Next

        jumlah = ListSementara.Count
    End Sub

    Private Sub AturDariSampai()
        Dim min As DateTime = New DateTime(2999, 12, 31, 23, 59, 59, 999)
        Dim tempi As DateTime
        For Each item As DataGridViewRow In Form_PData_HasilSurvey.DGVJawaban.Rows
            tempi = KonversiTanggalUni(item.Cells(2).Value.ToString)
            If tempi < min Then
                min = tempi
            End If
        Next
        DTDari.Value = min
        tterkecil = min

        Dim max As DateTime = New DateTime(1500, 1, 1, 0, 0, 0, 0)
        Dim tempa As DateTime
        For Each item As DataGridViewRow In Form_PData_HasilSurvey.DGVJawaban.Rows
            tempa = KonversiTanggalUni(item.Cells(2).Value.ToString)
            If tempa > max Then
                max = tempa
            End If
        Next
        DTSampai.Value = max
        tterbesar = max
    End Sub

    Private Sub Urutkan()
        Dim ListTerurut As List(Of String()) = New List(Of String())
        Dim min As DateTime
        Dim temp As DateTime
        Dim index As Integer
        Dim data As String() = Nothing
        HasilSurvey.Clear()

        Do While ListTerurut.Count <> jumlah
            min = tterbesar
            For Each item As String() In ListSementara
                temp = KonversiTanggalUni(item(2))
                If temp <= min Then
                    min = temp
                    data = item
                End If
            Next

            Try
                ListTerurut.Add(data)
                ListSementara.Remove(data)
            Catch ex As Exception
                MessageBox.Show(ex.ToString + " - " + index.ToString + " # " + jumlah.ToString)
            End Try
        Loop

        For Each h As String() In ListTerurut
            HasilSurvey.Add(h)
        Next
    End Sub

    Private Sub Form_Report_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Fade.Close()
        Form_PData_HasilSurvey.Focus()
    End Sub

    Private Sub Form_Report_Load(sender As Object, e As EventArgs) Handles Me.Load
        AturDariSampai()
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        For Each item As String() In Pertanyaan
            LBPertanyaan.Items.Add(item(1))
        Next
    End Sub

    Private Sub SegarkanIsi()
        LBPertanyaanT.Items.Clear()
        For Each item As String In daftar
            LBPertanyaanT.Items.Add(item)
        Next
        If LBPertanyaanT.Items.Count = 0 Then
            BHapus.Enabled = False
            BHapusS.Enabled = False
            BHapus.FlatAppearance.BorderColor = Color.DimGray
            BHapusS.FlatAppearance.BorderColor = Color.DimGray
        Else
            BHapusS.Enabled = True
            BHapusS.FlatAppearance.BorderColor = Color.SteelBlue
        End If
    End Sub

    Private Sub BKembali_Click(sender As Object, e As EventArgs)
        Me.Close()
    End Sub

    Private Sub BProses_Click(sender As Object, e As EventArgs) Handles BProses.Click
        If LBPertanyaanT.Items.Count = 0 Then
            MessageHandle.KotakPesan.Show(Me, "Kami tidak bisa melanjutkan jika tidak ada pertanyaan yang terpilih.", "Pertanyaan Belum Terpilih", MessageHandle.KotakPesan.TombolPesan.Ok, MessageHandle.KotakPesan.Warna.Peringatan)
            Exit Sub
        End If

        PHide.Location = New Point(0, 0)
        Button1.Visible = False
        Button2.Visible = False
        LUtama.Text = "Memproses Laporan...."
        LKeterangan.Visible = True
        Animasikan(PHide.Handle, 100, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        Application.DoEvents()
        PHide.Visible = True
        Application.DoEvents()
        System.Threading.Thread.Sleep(500)
        Dim dp As String()
        tmulai = DTDari.Value
        takhir = DTSampai.Value
        DaftarPertanyaan.Clear()
        For Each item As String In LBPertanyaanT.Items
            For Each p As String() In Pertanyaan
                If item = p(1) Then
                    dp = {p(0), p(1)}
                    Exit For
                End If
            Next
            DaftarPertanyaan.Add(dp)
        Next
        SeleksiTanggal()
        Urutkan()

        Reports.BuatPDF()

        Application.DoEvents()
        Animasikan(PHide.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
        Button1.Visible = True
        Button2.Visible = True
        LUtama.Text = "Laporan selesai dibuat"
        LKeterangan.Visible = False
        Application.DoEvents()
        Animasikan(PHide.Handle, 150, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)

        'Using Berkas As New StreamWriter("F:\survey.txt")
        '    Berkas.WriteLine("Pertanyaan")
        '    For Each item As String() In DaftarPertanyaan
        '        Berkas.WriteLine(item(0) + " - " + item(1))
        '    Next

        '    Berkas.WriteLine()

        '    Berkas.WriteLine("Data terurut:")
        '    For Each j As String() In HasilSurvey
        '        Berkas.WriteLine(j(0) + " - " + j(1) + " - " + j(2) + " - " + j(3))
        '    Next
        '    Berkas.WriteLine()

        '    For Each p As String() In DaftarPertanyaan
        '        Berkas.WriteLine("Pertanyaan dengan ID " + p(1))
        '        For Each j As String() In HasilSurvey
        '            If j(0) = p(1) Then
        '                Berkas.WriteLine(j(0) + " - " + j(1) + " - " + j(2) + " - " + j(3))
        '            End If
        '        Next
        '        Berkas.WriteLine()
        '    Next

        '    Berkas.WriteLine("Tersisa")
        '    For Each j As String() In ListSementara
        '        Berkas.WriteLine(j(0) + " - " + j(1) + " - " + j(2) + " - " + j(3))
        '    Next
        'End Using
    End Sub

    Private Sub BBatal_Click(sender As Object, e As EventArgs) Handles BBatal.Click
        Me.Close()
    End Sub

    Private Sub LBPertanyaan_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBPertanyaan.SelectedIndexChanged
        Try
            LPertanyaan.Text = Pertanyaan(LBPertanyaan.SelectedIndex)(0)
        Catch ex As Exception
        End Try
        BTambah.Enabled = True
        BTambah.FlatAppearance.BorderColor = Color.SteelBlue
    End Sub

    Private Sub LBPertanyaanT_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LBPertanyaanT.SelectedIndexChanged
        Try
            LPertanyaan.Text = Pertanyaan(LBPertanyaanT.SelectedIndex)(0)
        Catch ex As Exception
        End Try
        BHapus.Enabled = True
        BHapus.FlatAppearance.BorderColor = Color.SteelBlue
    End Sub

    Private Sub BTambahS_Click(sender As Object, e As EventArgs) Handles BTambahS.Click
        For Each item As String In LBPertanyaan.Items
            If Not LBPertanyaanT.Items.Contains(item) Then
                daftar.Add(item)
            End If
        Next
        SegarkanIsi()
    End Sub

    Private Sub BTambah_Click(sender As Object, e As EventArgs) Handles BTambah.Click
        Dim jumlah As Integer = LBPertanyaan.SelectedItems.Count
        For Each item As String In LBPertanyaan.SelectedItems
            If jumlah = 1 And LBPertanyaanT.Items.Contains(item) Then
                NotificationManager.Show(Me, "Pertanyaan sudah terpilih", Color.Orange, 3000)
                jumlah = 5
            End If
            If Not LBPertanyaanT.Items.Contains(item) Then
                daftar.Add(item)
            End If
        Next
        SegarkanIsi()
    End Sub

    Private Sub BHapus_Click(sender As Object, e As EventArgs) Handles BHapus.Click
        For Each item As String In LBPertanyaanT.SelectedItems
            daftar.Remove(item)
        Next
        SegarkanIsi()
        BHapus.Enabled = False
    End Sub

    Private Sub BHapusS_Click(sender As Object, e As EventArgs) Handles BHapusS.Click
        daftar.Clear()
        SegarkanIsi()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Animasikan(PHide.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Animasikan(PHide.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Tutup)
        Me.Close()
        Form_PData_HasilSurvey.Close()
        Form_PData.Close()
        Form_MenuUtama.Close()
        Animasikan(Form_Back.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Form_Back.Close()
        Process.Start(Pengaturan.alamatlaporan)
    End Sub
End Class