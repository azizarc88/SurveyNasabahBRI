﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_MenuUtama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TMainT = New System.Windows.Forms.Timer(Me.components)
        Me.TFocus = New System.Windows.Forms.Timer(Me.components)
        Me.PMulai = New System.Windows.Forms.PictureBox()
        Me.PMain = New System.Windows.Forms.PictureBox()
        Me.PFooter = New System.Windows.Forms.PictureBox()
        Me.PHeader = New System.Windows.Forms.PictureBox()
        Me.PBData = New System.Windows.Forms.PictureBox()
        Me.PBPengaturan = New System.Windows.Forms.PictureBox()
        Me.PBKeluar = New System.Windows.Forms.PictureBox()
        Me.PBBantuan = New System.Windows.Forms.PictureBox()
        Me.PBack = New System.Windows.Forms.PictureBox()
        CType(Me.PMulai, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PMain, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PFooter, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PHeader, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBData, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBPengaturan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBKeluar, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBBantuan, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PBack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Moire", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(0, 179)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(710, 67)
        Me.Label1.TabIndex = 4
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Label1.Visible = False
        '
        'TMainT
        '
        Me.TMainT.Interval = 500
        '
        'TFocus
        '
        '
        'PMulai
        '
        Me.PMulai.BackColor = System.Drawing.Color.Transparent
        Me.PMulai.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PMulai.Location = New System.Drawing.Point(404, 271)
        Me.PMulai.Name = "PMulai"
        Me.PMulai.Size = New System.Drawing.Size(16, 16)
        Me.PMulai.TabIndex = 5
        Me.PMulai.TabStop = False
        Me.PMulai.Visible = False
        '
        'PMain
        '
        Me.PMain.BackColor = System.Drawing.Color.Transparent
        Me.PMain.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PMain.Location = New System.Drawing.Point(345, 271)
        Me.PMain.Name = "PMain"
        Me.PMain.Size = New System.Drawing.Size(16, 16)
        Me.PMain.TabIndex = 6
        Me.PMain.TabStop = False
        Me.PMain.Visible = False
        '
        'PFooter
        '
        Me.PFooter.BackColor = System.Drawing.Color.Transparent
        Me.PFooter.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PFooter.Location = New System.Drawing.Point(293, 252)
        Me.PFooter.Name = "PFooter"
        Me.PFooter.Size = New System.Drawing.Size(16, 16)
        Me.PFooter.TabIndex = 7
        Me.PFooter.TabStop = False
        Me.PFooter.Visible = False
        '
        'PHeader
        '
        Me.PHeader.BackColor = System.Drawing.Color.Transparent
        Me.PHeader.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PHeader.Location = New System.Drawing.Point(271, 252)
        Me.PHeader.Name = "PHeader"
        Me.PHeader.Size = New System.Drawing.Size(16, 16)
        Me.PHeader.TabIndex = 8
        Me.PHeader.TabStop = False
        Me.PHeader.Visible = False
        '
        'PBData
        '
        Me.PBData.BackColor = System.Drawing.Color.Transparent
        Me.PBData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PBData.Location = New System.Drawing.Point(218, 170)
        Me.PBData.Name = "PBData"
        Me.PBData.Size = New System.Drawing.Size(16, 16)
        Me.PBData.TabIndex = 9
        Me.PBData.TabStop = False
        Me.PBData.Visible = False
        '
        'PBPengaturan
        '
        Me.PBPengaturan.BackColor = System.Drawing.Color.Transparent
        Me.PBPengaturan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PBPengaturan.Location = New System.Drawing.Point(218, 148)
        Me.PBPengaturan.Name = "PBPengaturan"
        Me.PBPengaturan.Size = New System.Drawing.Size(16, 16)
        Me.PBPengaturan.TabIndex = 10
        Me.PBPengaturan.TabStop = False
        Me.PBPengaturan.Visible = False
        '
        'PBKeluar
        '
        Me.PBKeluar.BackColor = System.Drawing.Color.Transparent
        Me.PBKeluar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PBKeluar.Location = New System.Drawing.Point(196, 170)
        Me.PBKeluar.Name = "PBKeluar"
        Me.PBKeluar.Size = New System.Drawing.Size(16, 16)
        Me.PBKeluar.TabIndex = 11
        Me.PBKeluar.TabStop = False
        Me.PBKeluar.Visible = False
        '
        'PBBantuan
        '
        Me.PBBantuan.BackColor = System.Drawing.Color.Transparent
        Me.PBBantuan.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.PBBantuan.Location = New System.Drawing.Point(196, 148)
        Me.PBBantuan.Name = "PBBantuan"
        Me.PBBantuan.Size = New System.Drawing.Size(16, 16)
        Me.PBBantuan.TabIndex = 12
        Me.PBBantuan.TabStop = False
        Me.PBBantuan.Visible = False
        '
        'PBack
        '
        Me.PBack.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PBack.Location = New System.Drawing.Point(0, 0)
        Me.PBack.Name = "PBack"
        Me.PBack.Size = New System.Drawing.Size(710, 438)
        Me.PBack.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PBack.TabIndex = 13
        Me.PBack.TabStop = False
        '
        'Form_MenuUtama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DodgerBlue
        Me.ClientSize = New System.Drawing.Size(710, 438)
        Me.Controls.Add(Me.PMulai)
        Me.Controls.Add(Me.PMain)
        Me.Controls.Add(Me.PFooter)
        Me.Controls.Add(Me.PHeader)
        Me.Controls.Add(Me.PBData)
        Me.Controls.Add(Me.PBPengaturan)
        Me.Controls.Add(Me.PBKeluar)
        Me.Controls.Add(Me.PBBantuan)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.PBack)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_MenuUtama"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form_MenuUtama"
        Me.TopMost = True
        CType(Me.PMulai, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PMain, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PFooter, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PHeader, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBData, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBPengaturan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBKeluar, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBBantuan, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PBack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PMulai As System.Windows.Forms.PictureBox
    Friend WithEvents PMain As System.Windows.Forms.PictureBox
    Friend WithEvents PFooter As System.Windows.Forms.PictureBox
    Friend WithEvents PHeader As System.Windows.Forms.PictureBox
    Friend WithEvents PBData As System.Windows.Forms.PictureBox
    Friend WithEvents PBPengaturan As System.Windows.Forms.PictureBox
    Friend WithEvents PBKeluar As System.Windows.Forms.PictureBox
    Friend WithEvents PBBantuan As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TMainT As System.Windows.Forms.Timer
    Friend WithEvents TFocus As System.Windows.Forms.Timer
    Friend WithEvents PBack As PictureBox
End Class
