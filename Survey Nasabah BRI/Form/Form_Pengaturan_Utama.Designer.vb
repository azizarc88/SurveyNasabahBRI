﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pengaturan_Utama
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form_Pengaturan_Utama))
        Me.LJudul = New System.Windows.Forms.Label()
        Me.CBTeksSelamat = New System.Windows.Forms.CheckBox()
        Me.CBTMenuUtama = New System.Windows.Forms.CheckBox()
        Me.CBTMenuSurvet = New System.Windows.Forms.CheckBox()
        Me.CBTPertanyaan = New System.Windows.Forms.CheckBox()
        Me.BBatal = New System.Windows.Forms.Button()
        Me.BOk = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(23, 11)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(8, 0, 8, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(634, 44)
        Me.LJudul.TabIndex = 11
        Me.LJudul.Text = "PENGATURAN UTAMA"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'CBTeksSelamat
        '
        Me.CBTeksSelamat.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBTeksSelamat.BackColor = System.Drawing.Color.White
        Me.CBTeksSelamat.Checked = True
        Me.CBTeksSelamat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBTeksSelamat.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBTeksSelamat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBTeksSelamat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBTeksSelamat.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTeksSelamat.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBTeksSelamat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBTeksSelamat.Location = New System.Drawing.Point(29, 77)
        Me.CBTeksSelamat.Margin = New System.Windows.Forms.Padding(8)
        Me.CBTeksSelamat.Name = "CBTeksSelamat"
        Me.CBTeksSelamat.Size = New System.Drawing.Size(628, 41)
        Me.CBTeksSelamat.TabIndex = 28
        Me.CBTeksSelamat.Text = "Tampilkan teks selamat datang"
        Me.CBTeksSelamat.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBTeksSelamat.UseVisualStyleBackColor = False
        '
        'CBTMenuUtama
        '
        Me.CBTMenuUtama.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBTMenuUtama.BackColor = System.Drawing.Color.White
        Me.CBTMenuUtama.Checked = True
        Me.CBTMenuUtama.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBTMenuUtama.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBTMenuUtama.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBTMenuUtama.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBTMenuUtama.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTMenuUtama.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBTMenuUtama.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBTMenuUtama.Location = New System.Drawing.Point(29, 134)
        Me.CBTMenuUtama.Margin = New System.Windows.Forms.Padding(8)
        Me.CBTMenuUtama.Name = "CBTMenuUtama"
        Me.CBTMenuUtama.Size = New System.Drawing.Size(628, 41)
        Me.CBTMenuUtama.TabIndex = 28
        Me.CBTMenuUtama.Text = "Fungsikan animasi tombol di menu utama"
        Me.CBTMenuUtama.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBTMenuUtama.UseVisualStyleBackColor = False
        '
        'CBTMenuSurvet
        '
        Me.CBTMenuSurvet.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBTMenuSurvet.BackColor = System.Drawing.Color.White
        Me.CBTMenuSurvet.Checked = True
        Me.CBTMenuSurvet.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBTMenuSurvet.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBTMenuSurvet.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBTMenuSurvet.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBTMenuSurvet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTMenuSurvet.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBTMenuSurvet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBTMenuSurvet.Location = New System.Drawing.Point(29, 191)
        Me.CBTMenuSurvet.Margin = New System.Windows.Forms.Padding(8)
        Me.CBTMenuSurvet.Name = "CBTMenuSurvet"
        Me.CBTMenuSurvet.Size = New System.Drawing.Size(628, 41)
        Me.CBTMenuSurvet.TabIndex = 28
        Me.CBTMenuSurvet.Text = "Fungsikan animasi tombol ke bagian Menu Survei"
        Me.CBTMenuSurvet.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBTMenuSurvet.UseVisualStyleBackColor = False
        '
        'CBTPertanyaan
        '
        Me.CBTPertanyaan.Appearance = System.Windows.Forms.Appearance.Button
        Me.CBTPertanyaan.BackColor = System.Drawing.Color.White
        Me.CBTPertanyaan.Checked = True
        Me.CBTPertanyaan.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CBTPertanyaan.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.CBTPertanyaan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DeepSkyBlue
        Me.CBTPertanyaan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.CBTPertanyaan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.CBTPertanyaan.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.CBTPertanyaan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CBTPertanyaan.Location = New System.Drawing.Point(29, 248)
        Me.CBTPertanyaan.Margin = New System.Windows.Forms.Padding(8)
        Me.CBTPertanyaan.Name = "CBTPertanyaan"
        Me.CBTPertanyaan.Size = New System.Drawing.Size(628, 41)
        Me.CBTPertanyaan.TabIndex = 28
        Me.CBTPertanyaan.Text = "Fungsikan animasi tombol ke bagian Pertanyaan"
        Me.CBTPertanyaan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CBTPertanyaan.UseVisualStyleBackColor = False
        '
        'BBatal
        '
        Me.BBatal.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BBatal.BackColor = System.Drawing.Color.MistyRose
        Me.BBatal.CausesValidation = False
        Me.BBatal.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BBatal.FlatAppearance.BorderSize = 2
        Me.BBatal.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BBatal.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BBatal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BBatal.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BBatal.Location = New System.Drawing.Point(460, 304)
        Me.BBatal.Margin = New System.Windows.Forms.Padding(8)
        Me.BBatal.Name = "BBatal"
        Me.BBatal.Size = New System.Drawing.Size(204, 72)
        Me.BBatal.TabIndex = 40
        Me.BBatal.Text = "Batal"
        Me.BBatal.UseVisualStyleBackColor = False
        '
        'BOk
        '
        Me.BOk.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BOk.BackColor = System.Drawing.Color.White
        Me.BOk.CausesValidation = False
        Me.BOk.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BOk.FlatAppearance.BorderSize = 2
        Me.BOk.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BOk.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BOk.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BOk.Location = New System.Drawing.Point(17, 304)
        Me.BOk.Margin = New System.Windows.Forms.Padding(8)
        Me.BOk.Name = "BOk"
        Me.BOk.Size = New System.Drawing.Size(204, 72)
        Me.BOk.TabIndex = 41
        Me.BOk.Text = "OK"
        Me.BOk.UseVisualStyleBackColor = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-3, 390)
        Me.PictureBox2.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 26
        Me.PictureBox2.TabStop = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-3, -11)
        Me.PictureBox1.Margin = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(742, 16)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 26
        Me.PictureBox1.TabStop = False
        '
        'Form_Pengaturan_Utama
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(681, 395)
        Me.Controls.Add(Me.BBatal)
        Me.Controls.Add(Me.BOk)
        Me.Controls.Add(Me.CBTPertanyaan)
        Me.Controls.Add(Me.CBTMenuSurvet)
        Me.Controls.Add(Me.CBTMenuUtama)
        Me.Controls.Add(Me.CBTeksSelamat)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.LJudul)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_Pengaturan_Utama"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Pengaturan_Utama"
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents CBTeksSelamat As System.Windows.Forms.CheckBox
    Friend WithEvents CBTMenuUtama As System.Windows.Forms.CheckBox
    Friend WithEvents CBTMenuSurvet As System.Windows.Forms.CheckBox
    Friend WithEvents CBTPertanyaan As System.Windows.Forms.CheckBox
    Friend WithEvents BBatal As System.Windows.Forms.Button
    Friend WithEvents BOk As System.Windows.Forms.Button
End Class
