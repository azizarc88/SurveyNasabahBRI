﻿Imports SkinApp.Gambar
Imports AnimasiUI.Class_Animasi

Public Class Form_SiapSurvey
    Dim thmain As New Threading.Thread(AddressOf Animasi)

    Dim siap_survey As List(Of Image) = New List(Of Image)

    Dim darisiap As Boolean = False
    Dim free As Boolean

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Private Sub MuatAnimasi(ByVal nama As String, ByVal var As List(Of Image), ByVal jumlahi As Integer)
        For index = 0 To jumlahi
            var.Add(AmbilGambarAnimasi(nama, index))
        Next
    End Sub

    Public Sub Animasi()
        Dim ab As Integer
        Do Until ab = 3
            For index = 0 To 26
                Application.DoEvents()
                If thmain.ThreadState = 4 Then
                    UbahGambarMain(siap_survey(index))
                End If
                Application.DoEvents()
                Jeda(40)
                Application.DoEvents()
            Next
            Application.DoEvents()
            Jeda(2000)
            Application.DoEvents()
        Loop
    End Sub

    Private Sub UbahGambarMain(ByVal gambar As System.Drawing.Image)
        If PSiapSurvey.InvokeRequired Then
            Try
                PSiapSurvey.Invoke(New Action(Of System.Drawing.Image)(AddressOf UbahGambarMain), gambar)
            Catch ex As Exception
            End Try
        Else
            PSiapSurvey.Image = gambar
        End If
    End Sub

    Private Sub MuatBackground()
        Dim img As Image = AmbilGambar("latar_siap_besar")
        Me.BackgroundImage = img
        If My.Computer.Screen.Bounds.Size.Width > 1366 Then
            Me.BackgroundImageLayout = ImageLayout.Stretch
        End If
    End Sub

    Public daritombol As Boolean

    Private Sub Form_SiapSurvey_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If Not daritombol Then
            BTutup.PerformClick()
            e.Cancel = True
        Else
            daritombol = False
            Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
            Jeda(500)
        End If
    End Sub

    Private Sub Inisialisasikan()
        Me.BackgroundImage = AmbilGambar("latar_siap_besar")

        PSiapSurvey.Image = AmbilGambar("tombol_siap_survey_26")
        PSiapSurvey.SizeMode = PictureBoxSizeMode.AutoSize
        PSiapSurvey.Location = New Point((Me.Size.Width / 2) - (PSiapSurvey.Size.Width / 2), (Me.Size.Height / 2 + 50) - (PSiapSurvey.Size.Height / 2))

        PSiap.Image = AmbilGambar("tombol_siap")
        PSiap.SizeMode = PictureBoxSizeMode.AutoSize
        PSiap.Location = New Point((Me.Size.Width / 2) - (PSiap.Size.Width / 2), (Me.Size.Height / 2 + 50) - (PSiap.Size.Height / 2))

        MuatAnimasi("tombol_siap_survey", siap_survey, 26)
    End Sub

    Private Sub Form_SiapSurvey_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        thmain.IsBackground = True
        thmain.Priority = Threading.ThreadPriority.Highest
        Application.DoEvents()
        Threading.Thread.CurrentThread.Priority = Threading.ThreadPriority.Highest
        Dim t As New Threading.Thread(AddressOf MuatBackground)
        t.Priority = Threading.ThreadPriority.Highest
        t.IsBackground = True
        t.Start()

        Application.DoEvents()
        Me.Location = New Point(0, 0)
        Me.Size = My.Computer.Screen.Bounds.Size
        Inisialisasikan()
        Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        If Pengaturan.animkempertanyaan = True Then
            thmain.Start()
        End If
    End Sub

    Private Sub PSiapSurvey_MouseHover(sender As Object, e As EventArgs) Handles PSiapSurvey.MouseEnter
        darisiap = True
        Animasikan(PSiap.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Buka)
        TSiapT.Enabled = True
    End Sub

    Private Sub Form_SiapSurvey_MouseLeave(sender As Object, e As EventArgs) Handles PSiap.MouseLeave
        Animasikan(PSiap.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Tutup)
    End Sub

    Private Sub Form_SiapSurvey_MouseEnter(sender As Object, e As EventArgs) Handles TableLayoutPanel1.MouseEnter
        free = True
    End Sub

    Private Sub Form_SiapSurvey_Shown(sender As Object, e As EventArgs) Handles Me.Shown
        Animasikan(PSiapSurvey.Handle, 500, GayaAnimasi.Tengah, OpsiGaya.Buka)
        PSiap.BringToFront()
    End Sub

    Private Sub TSiapT_Tick(sender As Object, e As EventArgs) Handles TSiapT.Tick
        If darisiap = True And free = True Then
            free = False
            darisiap = False
            Animasikan(PSiap.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Tutup)
            TSiapT.Enabled = False
        End If
    End Sub

    Private Sub PSiap_Click(sender As Object, e As EventArgs) Handles PSiap.Click
        PSiapSurvey.Visible = False
        Animasikan(PSiap.Handle, 150, GayaAnimasi.Tengah, OpsiGaya.Tutup)
        daritombol = True
        Me.Close()
        thmain.Abort()
        Form_Back.JalankanSurvey2()
    End Sub

    Private Sub BTutup_Click(sender As Object, e As EventArgs) Handles BTutup.Click
        Form_Fade.Show()
        Form_Password.Show()
        Form_Password.Fungsi = "Tutup"
        Form_Fade.NamaForm = Form_Password
        TPass.Enabled = Pengaturan.ototutuppass
        TPass.Interval = Pengaturan.wototutuppass * 1000
    End Sub

    Private Sub TPass_Tick(sender As Object, e As EventArgs) Handles TPass.Tick
        Form_Password.BBatal.PerformClick()
        TPass.Enabled = False
        PSiapSurvey.Focus()
    End Sub
End Class