﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_SiapSurvey
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.L2 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.L1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.PSiapSurvey = New System.Windows.Forms.PictureBox()
        Me.PSiap = New System.Windows.Forms.PictureBox()
        Me.TSiapT = New System.Windows.Forms.Timer(Me.components)
        Me.BTutup = New System.Windows.Forms.Button()
        Me.TPass = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.PSiapSurvey, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PSiap, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LJudul.Font = New System.Drawing.Font("Moire", 25.0!)
        Me.LJudul.ForeColor = System.Drawing.Color.White
        Me.LJudul.Location = New System.Drawing.Point(3, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(841, 55)
        Me.LJudul.TabIndex = 12
        Me.LJudul.Text = "  SURVEI NASABAH"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'L2
        '
        Me.L2.BackColor = System.Drawing.Color.Transparent
        Me.L2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.L2.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.L2.ForeColor = System.Drawing.Color.White
        Me.L2.Location = New System.Drawing.Point(3, 125)
        Me.L2.Name = "L2"
        Me.L2.Size = New System.Drawing.Size(841, 31)
        Me.L2.TabIndex = 12
        Me.L2.Text = "Bantu kami meningkatkan pelayanan dengan cara ikuti survei di sini."
        Me.L2.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.L1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.LJudul, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.L2, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 7)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9.359779!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.06158!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.360165!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.004443!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 51.84054!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.008885!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.004444!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.360165!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(847, 588)
        Me.TableLayoutPanel1.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label1.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 156)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(841, 23)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Untuk memulai, sentuh lingkaran di bawah ini."
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'L1
        '
        Me.L1.BackColor = System.Drawing.Color.Transparent
        Me.L1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.L1.Font = New System.Drawing.Font("Century Gothic", 19.0!)
        Me.L1.ForeColor = System.Drawing.Color.White
        Me.L1.Location = New System.Drawing.Point(3, 55)
        Me.L1.Name = "L1"
        Me.L1.Size = New System.Drawing.Size(841, 70)
        Me.L1.TabIndex = 15
        Me.L1.Text = "Tidak puas dengan pelayanan kami ?"
        Me.L1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'Label2
        '
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Label2.Font = New System.Drawing.Font("Century Gothic", 25.0!)
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(3, 483)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(841, 47)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "                                   Terima Kasih"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 3
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 410.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 556)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(841, 29)
        Me.TableLayoutPanel2.TabIndex = 14
        '
        'PSiapSurvey
        '
        Me.PSiapSurvey.BackColor = System.Drawing.Color.Transparent
        Me.PSiapSurvey.Location = New System.Drawing.Point(20, 228)
        Me.PSiapSurvey.Name = "PSiapSurvey"
        Me.PSiapSurvey.Size = New System.Drawing.Size(35, 31)
        Me.PSiapSurvey.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PSiapSurvey.TabIndex = 15
        Me.PSiapSurvey.TabStop = False
        Me.PSiapSurvey.Visible = False
        '
        'PSiap
        '
        Me.PSiap.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PSiap.BackColor = System.Drawing.Color.Transparent
        Me.PSiap.Location = New System.Drawing.Point(3, 256)
        Me.PSiap.Name = "PSiap"
        Me.PSiap.Size = New System.Drawing.Size(18, 22)
        Me.PSiap.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PSiap.TabIndex = 16
        Me.PSiap.TabStop = False
        Me.PSiap.Visible = False
        '
        'TSiapT
        '
        Me.TSiapT.Interval = 500
        '
        'BTutup
        '
        Me.BTutup.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BTutup.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(135, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.BTutup.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.BTutup.FlatAppearance.MouseDownBackColor = System.Drawing.Color.OrangeRed
        Me.BTutup.FlatAppearance.MouseOverBackColor = System.Drawing.Color.SandyBrown
        Me.BTutup.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BTutup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BTutup.ForeColor = System.Drawing.Color.DarkRed
        Me.BTutup.Location = New System.Drawing.Point(815, 5)
        Me.BTutup.Name = "BTutup"
        Me.BTutup.Size = New System.Drawing.Size(26, 26)
        Me.BTutup.TabIndex = 17
        Me.BTutup.Text = "X"
        Me.BTutup.UseVisualStyleBackColor = False
        '
        'TPass
        '
        Me.TPass.Interval = 15000
        '
        'Form_SiapSurvey
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(847, 588)
        Me.Controls.Add(Me.BTutup)
        Me.Controls.Add(Me.PSiap)
        Me.Controls.Add(Me.PSiapSurvey)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "Form_SiapSurvey"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Form_SiapSurvey"
        Me.TopMost = True
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.PSiapSurvey, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PSiap, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents L2 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents PSiapSurvey As System.Windows.Forms.PictureBox
    Friend WithEvents PSiap As System.Windows.Forms.PictureBox
    Public WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TSiapT As System.Windows.Forms.Timer
    Friend WithEvents BTutup As System.Windows.Forms.Button
    Friend WithEvents TPass As System.Windows.Forms.Timer
    Friend WithEvents L1 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
