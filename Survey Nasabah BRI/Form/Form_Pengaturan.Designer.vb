﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form_Pengaturan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.LJudul = New System.Windows.Forms.Label()
        Me.BPUtama = New System.Windows.Forms.Button()
        Me.BPKeamanan = New System.Windows.Forms.Button()
        Me.BPLaporan = New System.Windows.Forms.Button()
        Me.BPPemilik = New System.Windows.Forms.Button()
        Me.BPSurvey = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.LKeterangan = New System.Windows.Forms.Label()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.BSimpan = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.BackColor = System.Drawing.Color.Transparent
        Me.LJudul.Font = New System.Drawing.Font("Moire", 30.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LJudul.ForeColor = System.Drawing.Color.Black
        Me.LJudul.Location = New System.Drawing.Point(16, 9)
        Me.LJudul.Margin = New System.Windows.Forms.Padding(7, 0, 7, 0)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(962, 75)
        Me.LJudul.TabIndex = 11
        Me.LJudul.Text = "PENGATURAN APLIKASI"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        '
        'BPUtama
        '
        Me.BPUtama.BackColor = System.Drawing.Color.White
        Me.BPUtama.CausesValidation = False
        Me.BPUtama.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BPUtama.FlatAppearance.BorderSize = 2
        Me.BPUtama.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BPUtama.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BPUtama.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BPUtama.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BPUtama.Location = New System.Drawing.Point(12, 4)
        Me.BPUtama.Margin = New System.Windows.Forms.Padding(4)
        Me.BPUtama.Name = "BPUtama"
        Me.BPUtama.Size = New System.Drawing.Size(172, 70)
        Me.BPUtama.TabIndex = 27
        Me.BPUtama.Text = "Pengaturan Utama"
        Me.BPUtama.UseVisualStyleBackColor = False
        '
        'BPKeamanan
        '
        Me.BPKeamanan.BackColor = System.Drawing.Color.White
        Me.BPKeamanan.CausesValidation = False
        Me.BPKeamanan.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BPKeamanan.FlatAppearance.BorderSize = 2
        Me.BPKeamanan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BPKeamanan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BPKeamanan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BPKeamanan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BPKeamanan.Location = New System.Drawing.Point(200, 4)
        Me.BPKeamanan.Margin = New System.Windows.Forms.Padding(4)
        Me.BPKeamanan.Name = "BPKeamanan"
        Me.BPKeamanan.Size = New System.Drawing.Size(172, 70)
        Me.BPKeamanan.TabIndex = 27
        Me.BPKeamanan.Text = "Pengaturan Keamanan"
        Me.BPKeamanan.UseVisualStyleBackColor = False
        '
        'BPLaporan
        '
        Me.BPLaporan.BackColor = System.Drawing.Color.White
        Me.BPLaporan.CausesValidation = False
        Me.BPLaporan.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BPLaporan.FlatAppearance.BorderSize = 2
        Me.BPLaporan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BPLaporan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BPLaporan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BPLaporan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BPLaporan.Location = New System.Drawing.Point(576, 4)
        Me.BPLaporan.Margin = New System.Windows.Forms.Padding(4)
        Me.BPLaporan.Name = "BPLaporan"
        Me.BPLaporan.Size = New System.Drawing.Size(172, 70)
        Me.BPLaporan.TabIndex = 27
        Me.BPLaporan.Text = "Pengaturan Laporan"
        Me.BPLaporan.UseVisualStyleBackColor = False
        '
        'BPPemilik
        '
        Me.BPPemilik.BackColor = System.Drawing.Color.White
        Me.BPPemilik.CausesValidation = False
        Me.BPPemilik.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BPPemilik.FlatAppearance.BorderSize = 2
        Me.BPPemilik.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BPPemilik.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BPPemilik.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BPPemilik.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BPPemilik.Location = New System.Drawing.Point(764, 4)
        Me.BPPemilik.Margin = New System.Windows.Forms.Padding(4)
        Me.BPPemilik.Name = "BPPemilik"
        Me.BPPemilik.Size = New System.Drawing.Size(172, 70)
        Me.BPPemilik.TabIndex = 27
        Me.BPPemilik.Text = "Pengaturan Pemilik"
        Me.BPPemilik.UseVisualStyleBackColor = False
        '
        'BPSurvey
        '
        Me.BPSurvey.BackColor = System.Drawing.Color.White
        Me.BPSurvey.CausesValidation = False
        Me.BPSurvey.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BPSurvey.FlatAppearance.BorderSize = 2
        Me.BPSurvey.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BPSurvey.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BPSurvey.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BPSurvey.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BPSurvey.Location = New System.Drawing.Point(388, 4)
        Me.BPSurvey.Margin = New System.Windows.Forms.Padding(4)
        Me.BPSurvey.Name = "BPSurvey"
        Me.BPSurvey.Size = New System.Drawing.Size(172, 70)
        Me.BPSurvey.TabIndex = 28
        Me.BPSurvey.Text = "Pengaturan Survei"
        Me.BPSurvey.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 11
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me.TableLayoutPanel1.Controls.Add(Me.BPUtama, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BPKeamanan, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BPSurvey, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BPLaporan, 7, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.BPPemilik, 9, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(24, 142)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(953, 99)
        Me.TableLayoutPanel1.TabIndex = 29
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.LKeterangan)
        Me.GroupBox1.Location = New System.Drawing.Point(24, 247)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(954, 179)
        Me.GroupBox1.TabIndex = 30
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Bantuan"
        '
        'LKeterangan
        '
        Me.LKeterangan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LKeterangan.Location = New System.Drawing.Point(9, 23)
        Me.LKeterangan.Name = "LKeterangan"
        Me.LKeterangan.Size = New System.Drawing.Size(937, 146)
        Me.LKeterangan.TabIndex = 0
        Me.LKeterangan.Text = "Silahkan klik salah satu tombol untuk melihat bantuan."
        '
        'Button5
        '
        Me.Button5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button5.BackColor = System.Drawing.Color.Gainsboro
        Me.Button5.CausesValidation = False
        Me.Button5.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.Button5.FlatAppearance.BorderSize = 2
        Me.Button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.Button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.Button5.Location = New System.Drawing.Point(773, 443)
        Me.Button5.Margin = New System.Windows.Forms.Padding(8)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(204, 72)
        Me.Button5.TabIndex = 46
        Me.Button5.Text = "Kembali >"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'BSimpan
        '
        Me.BSimpan.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BSimpan.BackColor = System.Drawing.Color.White
        Me.BSimpan.CausesValidation = False
        Me.BSimpan.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.BSimpan.FlatAppearance.BorderSize = 2
        Me.BSimpan.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.BSimpan.FlatAppearance.MouseOverBackColor = System.Drawing.Color.LightSkyBlue
        Me.BSimpan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BSimpan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.BSimpan.Location = New System.Drawing.Point(17, 443)
        Me.BSimpan.Margin = New System.Windows.Forms.Padding(8)
        Me.BSimpan.Name = "BSimpan"
        Me.BSimpan.Size = New System.Drawing.Size(204, 72)
        Me.BSimpan.TabIndex = 47
        Me.BSimpan.Text = "Simpan Pengaturan"
        Me.BSimpan.UseVisualStyleBackColor = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 800
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(425, 452)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(147, 57)
        Me.Button1.TabIndex = 48
        Me.Button1.Text = "Tentang Pembuat"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form_Pengaturan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(10.0!, 21.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(994, 532)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.BSimpan)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.LJudul)
        Me.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(5)
        Me.Name = "Form_Pengaturan"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form_Pengaturan"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents BPUtama As System.Windows.Forms.Button
    Friend WithEvents BPKeamanan As System.Windows.Forms.Button
    Friend WithEvents BPLaporan As System.Windows.Forms.Button
    Friend WithEvents BPPemilik As System.Windows.Forms.Button
    Friend WithEvents BPSurvey As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents LKeterangan As System.Windows.Forms.Label
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents BSimpan As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Button1 As Button
End Class
