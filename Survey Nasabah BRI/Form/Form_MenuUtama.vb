﻿Imports AnimasiUI.Class_Animasi
Imports SkinApp.Gambar
Imports MessageHandle
Imports Survey_Nasabah_BRI.Form_Back

Public Class Form_MenuUtama
    Dim menu_mulai As List(Of Image) = New List(Of Image)
    Dim tombol_bantuan As List(Of Image) = New List(Of Image)
    Dim tombol_bantuan_non As List(Of Image) = New List(Of Image)
    Dim tombol_keluar As List(Of Image) = New List(Of Image)
    Dim tombol_keluar_non As List(Of Image) = New List(Of Image)
    Dim tombol_pengaturan As List(Of Image) = New List(Of Image)
    Dim tombol_pengaturan_non As List(Of Image) = New List(Of Image)
    Dim tombol_data As List(Of Image) = New List(Of Image)
    Dim tombol_data_non As List(Of Image) = New List(Of Image)

    Public thmain As New Threading.Thread(AddressOf Animasi)
    Dim tback As New Threading.Thread(AddressOf MuatBackground)

    Dim banim As Int16
    Dim posisimouse As Point
    Dim hilang As Boolean = False
    Dim memproses As Boolean = False
    Dim st As StatusTombol = StatusTombol.Nonaktiv
    Dim sm As StatusMouse = StatusMouse.DiluarTombol
    Dim sk As StatusKlik = StatusKlik.Idle
    Dim n As NamaTombol = Nothing
    Dim darimain As Boolean = False
    Dim free As Boolean
    Public BisaFokus As Boolean = True

    Private WithEvents animate As New Animasi
    Public pengaturanfirst As Boolean

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST
            Return Result
        End Get
    End Property

    Public Sub Animasi()
        Dim ab As Integer
        Do Until ab = 3
            For index = 0 To banim
                Application.DoEvents()
                If thmain.ThreadState = 4 Then
                    UbahGambarMain(menu_mulai(index))
                End If
                Application.DoEvents()
                Jeda(40)
                Application.DoEvents()
            Next
            Application.DoEvents()
            Jeda(5000)
            Application.DoEvents()
        Loop
    End Sub

    Private Sub AnimasiTombol(ByVal namatombol As NamaTombol, ByVal maju As Boolean, ByVal data As List(Of Image))
        If memproses = False Then
            If maju = True Then
                memproses = True
                If Pengaturan.animmenuutama = True Then
                    animate.AnimTombolAsync(data, namatombol, True)
                Else
                    Dim a As PictureBox = Form_Back.KonversiTombol(namatombol)
                    a.Image = data(data.Count - 1)
                    memproses = False
                End If
            ElseIf maju = False Then
                memproses = True
                If Pengaturan.animmenuutama = True Then
                    animate.AnimTombolAsync(data, namatombol, False)
                Else
                    Dim a As PictureBox = Form_Back.KonversiTombol(namatombol)
                    a.Image = data(0)
                    memproses = False
                End If
            End If
            n = namatombol
        End If
    End Sub

    Private Sub UbahGambarMain(ByVal gambar As System.Drawing.Bitmap)
        If PMain.InvokeRequired Then
            PMain.Invoke(New Action(Of System.Drawing.Bitmap)(AddressOf UbahGambarMain), gambar)
        Else
            PMain.Image = gambar
        End If
    End Sub


    Private Sub MuatAnimasi(ByVal nama As String, ByVal var As List(Of Image), ByVal jumlahi As Integer)
        For index = 0 To jumlahi
            var.Add(AmbilGambarAnimasi(nama, index))
        Next
    End Sub

    Private Sub MuatBackground()
        Dim img As Image = AmbilGambar("latar_besar")
        PBack.Image = img
        Me.BackgroundImage = img
        If My.Computer.Screen.Bounds.Size.Width > 1366 Then
            Me.BackgroundImageLayout = ImageLayout.Stretch
            PBack.SizeMode = PictureBoxSizeMode.StretchImage
        End If
    End Sub

    Private Sub Inisialisasi()
        PBBantuan.Image = AmbilGambar("tombol_bantuan_aktiv_0")
        PBBantuan.SizeMode = PictureBoxSizeMode.AutoSize
        PBBantuan.Location = New Point(25, offsetatas)

        PHeader.Image = AmbilGambar("header")
        PHeader.SizeMode = PictureBoxSizeMode.AutoSize
        PHeader.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (PHeader.Size.Width / 2), offsetatas)

        PBKeluar.Image = AmbilGambar("tombol_keluar_aktiv_0")
        PBKeluar.SizeMode = PictureBoxSizeMode.AutoSize
        PBKeluar.Location = New Point(My.Computer.Screen.Bounds.Width - (PBKeluar.Size.Width + 25), offsetatas)

        PBPengaturan.Image = AmbilGambar("tombol_pengaturan_aktiv_0")
        PBPengaturan.SizeMode = PictureBoxSizeMode.AutoSize
        PBPengaturan.Location = New Point(25, My.Computer.Screen.Bounds.Height - PBPengaturan.Size.Height + offsetbawah)

        PBData.Image = AmbilGambar("tombol_data_aktiv_0")
        PBData.SizeMode = PictureBoxSizeMode.AutoSize
        PBData.Location = New Point(My.Computer.Screen.Bounds.Width - (PBData.Size.Width + 25), My.Computer.Screen.Bounds.Height - PBData.Size.Height + offsetbawah)

        PFooter.Image = AmbilGambar("footer")
        PFooter.SizeMode = PictureBoxSizeMode.AutoSize
        PFooter.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (PFooter.Size.Width / 2), My.Computer.Screen.Bounds.Height - PFooter.Size.Height + offsetbawah)

        PMain.Image = AmbilGambar("menu_mulai_21")
        PMain.SizeMode = PictureBoxSizeMode.AutoSize
        PMain.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (PMain.Size.Width / 2), (My.Computer.Screen.Bounds.Height / 2) - (PMain.Size.Height / 2))

        PMulai.Image = AmbilGambar("mulai_survey")
        PMulai.SizeMode = PictureBoxSizeMode.StretchImage
        PMulai.Size = New Point(408, 228)
        PMulai.Location = New Point((My.Computer.Screen.Bounds.Width / 2) - (PMulai.Size.Width / 2), (My.Computer.Screen.Bounds.Height / 2) - (PMulai.Size.Height / 2))
        PMulai.BringToFront()

        MuatAnimasi("menu_mulai", menu_mulai, 21)
        MuatAnimasi("tombol_bantuan_aktiv", tombol_bantuan, 7)
        MuatAnimasi("tombol_bantuan_nonaktiv", tombol_bantuan_non, 6)
        MuatAnimasi("tombol_keluar_aktiv", tombol_keluar, 7)
        MuatAnimasi("tombol_keluar_nonaktiv", tombol_keluar_non, 6)
        MuatAnimasi("tombol_pengaturan_aktiv", tombol_pengaturan, 7)
        MuatAnimasi("tombol_pengaturan_nonaktiv", tombol_pengaturan_non, 6)
        MuatAnimasi("tombol_data_aktiv", tombol_data, 7)
        MuatAnimasi("tombol_data_nonaktiv", tombol_data_non, 6)
        Jeda(500)

        BacaPertanyaan()
        BacaJawaban(Me)
    End Sub

    Private Sub Form_MenuUtama_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Crypt.Crypt.aman = "@matganteng"
        Application.DoEvents()
        Threading.Thread.CurrentThread.Priority = Threading.ThreadPriority.Highest
        tback.Priority = Threading.ThreadPriority.Highest
        tback.IsBackground = False
        Application.DoEvents()

        Me.Size = Screen.PrimaryScreen.Bounds.Size
        Me.Location = New Point(0, 0)
        Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        Application.DoEvents()
        Label1.Visible = True
        Pengaturan.BacaPengaturan()
    End Sub

    Private Sub Form_MenuUtama_Shown(sender As Object, e As EventArgs) Handles MyBase.Shown
        tback.Start()
        Application.DoEvents()
        If Form_Back.firstrun = True And Pengaturan.tekspembuka = True Then
            AnimasiHuruf("SELAMAT DATANG", Label1)
        End If
        Inisialisasi()
        tback.Abort()

        thmain.IsBackground = True
        thmain.Priority = Threading.ThreadPriority.Highest

        Application.DoEvents()
        Animasikan(PBBantuan.Handle, 250, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        Application.DoEvents()
        Animasikan(PHeader.Handle, 250, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        Application.DoEvents()
        Animasikan(PBKeluar.Handle, 250, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Buka)
        Application.DoEvents()
        Animasikan(PBPengaturan.Handle, 250, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Buka)
        Application.DoEvents()
        Animasikan(PFooter.Handle, 250, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Buka)
        Application.DoEvents()
        Animasikan(PBData.Handle, 250, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Buka)
        Label1.Visible = False
        Application.DoEvents()
        Animasikan(PMain.Handle, 500, GayaAnimasi.Tengah, OpsiGaya.Buka)
        Application.DoEvents()
        Me.Focus()
        banim = 21


        If Pertanyaan.Count = 0 And Form_Back.firstrun = True And Pengaturan.firstrun = False Then
            KotakPesan.Show(Me, "Anda belum membuat daftar pertanyaan yang akan digunakan untuk melakukan survei" + Environment.NewLine + "Silakan buat terlebih dahulu melalui menu Pengolah Data.", "Tidak Ada Pertanyaan", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Peringatan)
        End If
        Try
            If Pengaturan.animkemsurvey = True Then
                thmain.Start()
            End If
        Catch ex As Exception
        End Try
        Form_Back.firstrun = False

        If Pengaturan.firstrun = True Then
            pengaturanfirst = True
            KotakPesan.Show(Me, "Selamat datang di program Survei Nasabah" + Environment.NewLine + "Untuk memulai, silakan atur beberapa pengaturan penting dahulu seperti kunci enkripsi, kata sandi, identitas pemilik, dll.", "Selamat Datang", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Informasi)
            If Pengaturan.animmenuutama = True Then
                Try
                    thmain.Suspend()
                Catch ex As Exception
                End Try
            End If
            Form_Pengaturan.Show()
        End If
    End Sub

    Dim daritombol As Boolean

    Private Sub Form_Handle_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        If Not daritombol Then
            If KotakPesan.Show(Me, "Apa Anda yakin ingin keluar ke Desktop ?", "Keluar", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) = KotakPesan.HasilPesan.Ya Then
                Try
                    thmain.Abort()
                Catch ex As Exception
                End Try
                Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
                Jeda(500)
                Animasikan(Form_Back.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
                Form_Back.Close()
            Else
                e.Cancel = True
            End If
        Else
            daritombol = False
            Animasikan(Me.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
            Jeda(500)
        End If
    End Sub

    Private Sub tombol_gambarberubah(ByVal sender As Object, ByVal e As GambarTombolBerubahEventArgs) Handles animate.GambarTombolBerubah
        If e.Nama = NamaTombol.Bantuan Then
            Application.DoEvents()
            PBBantuan.Image = e.Gambar
            Application.DoEvents()
        ElseIf e.Nama = NamaTombol.Keluar Then
            Application.DoEvents()
            PBKeluar.Image = e.Gambar
            Application.DoEvents()
        ElseIf e.Nama = NamaTombol.Pengaturan Then
            Application.DoEvents()
            PBPengaturan.Image = e.Gambar
            Application.DoEvents()
        ElseIf e.Nama = NamaTombol.Data Then
            Application.DoEvents()
            PBData.Image = e.Gambar
            Application.DoEvents()
        End If
    End Sub

    Private Sub tombol_animasiselesai(ByVal sender As Object, ByVal e As AnimasiTombolSelesaiEventArgs) Handles animate.AnimasiTombolSelesai
        memproses = e.IsProcessing
        If e.NamaTombol = NamaTombol.Bantuan Then
            If st = StatusTombol.Aktiv And sm = StatusMouse.DiluarTombol Then
                st = StatusTombol.Nonaktiv
                AnimasiTombol(NamaTombol.Bantuan, False, tombol_bantuan)
            ElseIf st = StatusTombol.MouseDown And sk = StatusKlik.KlikUp Then
                st = StatusTombol.MouseUp
                AnimasiTombol(NamaTombol.Bantuan, False, tombol_bantuan_non)
            End If
        ElseIf e.NamaTombol = NamaTombol.Keluar Then
            If st = StatusTombol.Aktiv And sm = StatusMouse.DiluarTombol Then
                st = StatusTombol.Nonaktiv
                AnimasiTombol(NamaTombol.Keluar, False, tombol_keluar)
            ElseIf st = StatusTombol.MouseDown And sk = StatusKlik.KlikUp Then
                st = StatusTombol.MouseUp
                AnimasiTombol(NamaTombol.Keluar, False, tombol_keluar_non)
            End If
        ElseIf e.NamaTombol = NamaTombol.Pengaturan Then
            If st = StatusTombol.Aktiv And sm = StatusMouse.DiluarTombol Then
                st = StatusTombol.Nonaktiv
                AnimasiTombol(NamaTombol.Pengaturan, False, tombol_pengaturan)
            ElseIf st = StatusTombol.MouseDown And sk = StatusKlik.KlikUp Then
                st = StatusTombol.MouseUp
                AnimasiTombol(NamaTombol.Pengaturan, False, tombol_pengaturan_non)
            End If
        ElseIf e.NamaTombol = NamaTombol.Data Then
            If st = StatusTombol.Aktiv And sm = StatusMouse.DiluarTombol Then
                st = StatusTombol.Nonaktiv
                AnimasiTombol(NamaTombol.Data, False, tombol_data)
            ElseIf st = StatusTombol.MouseDown And sk = StatusKlik.KlikUp Then
                st = StatusTombol.MouseUp
                AnimasiTombol(NamaTombol.Data, False, tombol_data_non)
            End If
        End If
    End Sub


    Public Sub AturTombol(ByVal status As StatusTombol, ByVal namat As NamaTombol, ByVal var As List(Of Image))
        If status = StatusTombol.Aktiv Then
            sm = StatusMouse.DidalamTombol
            If st = StatusTombol.Nonaktiv Then
                If memproses = False Then
                    st = StatusTombol.Aktiv
                End If
                AnimasiTombol(namat, True, var)
            End If
        ElseIf status = StatusTombol.Nonaktiv Then
            sm = StatusMouse.DiluarTombol
            If st = StatusTombol.Aktiv Or st = StatusTombol.MouseUp Then
                If memproses = False Then
                    st = StatusTombol.Nonaktiv
                End If
                AnimasiTombol(namat, False, var)
            End If
        ElseIf status = StatusTombol.MouseDown Then
            sk = StatusKlik.KlikDown
            If st = StatusTombol.Aktiv Or st = StatusTombol.MouseUp Then
                If memproses = False Then
                    st = StatusTombol.MouseDown
                End If
                AnimasiTombol(namat, True, var)
            End If
        Else
            sk = StatusKlik.KlikUp
            If st = StatusTombol.MouseDown Then
                If memproses = False Then
                    st = StatusTombol.MouseUp
                End If
                AnimasiTombol(namat, False, var)
            End If
        End If
    End Sub

    Private Sub PBBantuan_MouseEnter(sender As Object, e As EventArgs) Handles PBBantuan.MouseEnter
        AturTombol(StatusTombol.Aktiv, NamaTombol.Bantuan, tombol_bantuan)
    End Sub

    Private Sub PBBantuan_MouseLeave(sender As Object, e As EventArgs) Handles PBBantuan.MouseLeave
        AturTombol(StatusTombol.Nonaktiv, NamaTombol.Bantuan, tombol_bantuan)
    End Sub

    Private Sub PBBantuan_MouseDown(sender As Object, e As MouseEventArgs) Handles PBBantuan.MouseDown
        AturTombol(StatusTombol.MouseDown, NamaTombol.Bantuan, tombol_bantuan_non)
    End Sub

    Private Sub PBBantuan_MouseUp(sender As Object, e As MouseEventArgs) Handles PBBantuan.MouseUp
        AturTombol(StatusTombol.MouseUp, NamaTombol.Bantuan, tombol_bantuan_non)
    End Sub

    Private Sub PBBantuan_Click(sender As Object, e As EventArgs) Handles PBBantuan.Click
        If Pengaturan.animmenuutama = True Then
            Try
                thmain.Suspend()
            Catch ex As Exception
            End Try
        End If
    End Sub



    Private Sub PBKeluar_MouseHover(sender As Object, e As EventArgs) Handles PBKeluar.MouseEnter
        AturTombol(StatusTombol.Aktiv, NamaTombol.Keluar, tombol_keluar)
    End Sub

    Private Sub PBKeluar_MouseLeave(sender As Object, e As EventArgs) Handles PBKeluar.MouseLeave
        AturTombol(StatusTombol.Nonaktiv, NamaTombol.Keluar, tombol_keluar)
    End Sub

    Private Sub PBKeluar_MouseDown(sender As Object, e As MouseEventArgs) Handles PBKeluar.MouseDown
        AturTombol(StatusTombol.MouseDown, NamaTombol.Keluar, tombol_keluar_non)
    End Sub

    Private Sub PBKeluar_MouseUp(sender As Object, e As MouseEventArgs) Handles PBKeluar.MouseUp
        AturTombol(StatusTombol.MouseUp, NamaTombol.Keluar, tombol_keluar_non)
    End Sub

    Private Sub PBKeluar_MouseClick(sender As Object, e As MouseEventArgs) Handles PBKeluar.Click
        If KotakPesan.Show(Me, "Apa Anda yakin ingin keluar ke Desktop ?", "Keluar", KotakPesan.TombolPesan.Ya_Tidak, KotakPesan.Warna.Pertanyaan) = KotakPesan.HasilPesan.Ya Then
            Try
                thmain.Abort()
            Catch ex As Exception
            End Try
            daritombol = True
            Me.Close()
            Animasikan(Form_Back.Handle, 500, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
            Form_Back.Close()
        Else
            daritombol = False
        End If
    End Sub



    Private Sub PBPengaturan_MouseEnter(sender As Object, e As EventArgs) Handles PBPengaturan.MouseEnter
        AturTombol(StatusTombol.Aktiv, NamaTombol.Pengaturan, tombol_pengaturan)
    End Sub

    Private Sub PBPengaturan_MouseLeave(sender As Object, e As EventArgs) Handles PBPengaturan.MouseLeave
        AturTombol(StatusTombol.Nonaktiv, NamaTombol.Pengaturan, tombol_pengaturan)
    End Sub

    Private Sub PBPengaturan_MouseDown(sender As Object, e As MouseEventArgs) Handles PBPengaturan.MouseDown
        AturTombol(StatusTombol.MouseDown, NamaTombol.Pengaturan, tombol_pengaturan_non)
    End Sub

    Private Sub PBPengaturan_MouseUp(sender As Object, e As MouseEventArgs) Handles PBPengaturan.MouseUp
        AturTombol(StatusTombol.MouseUp, NamaTombol.Pengaturan, tombol_pengaturan_non)
    End Sub

    Private Sub PBPengaturan_MouseClick(sender As Object, e As MouseEventArgs) Handles PBPengaturan.Click
        If Pengaturan.animmenuutama = True Then
            Try
                thmain.Suspend()
            Catch ex As Exception
            End Try
        End If
        Form_Pengaturan.Show()
    End Sub



    Private Sub PBData_MouseEnter(sender As Object, e As EventArgs) Handles PBData.MouseEnter
        AturTombol(StatusTombol.Aktiv, NamaTombol.Data, tombol_data)
    End Sub

    Private Sub PBData_MouseLeave(sender As Object, e As EventArgs) Handles PBData.MouseLeave
        AturTombol(StatusTombol.Nonaktiv, NamaTombol.Data, tombol_data)
    End Sub

    Private Sub PBData_MouseDown(sender As Object, e As MouseEventArgs) Handles PBData.MouseDown
        AturTombol(StatusTombol.MouseDown, NamaTombol.Data, tombol_data_non)
    End Sub

    Private Sub PBData_MouseUp(sender As Object, e As MouseEventArgs) Handles PBData.MouseUp
        AturTombol(StatusTombol.MouseUp, NamaTombol.Data, tombol_data_non)
    End Sub

    Private Sub PBData_MouseClick(sender As Object, e As MouseEventArgs) Handles PBData.Click
        If Pengaturan.animmenuutama = True Then
            Try
                thmain.Suspend()
            Catch ex As Exception
            End Try
        End If
        Form_PData.Show()
    End Sub

    Private Sub PMain_MouseHover(sender As Object, e As EventArgs) Handles PMain.MouseEnter
        If darimain = False And sm = StatusMouse.DiluarTombol And free = True Then
            free = False
            sm = StatusMouse.DidalamTombol
            darimain = True
            Animasikan(PMulai.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Buka)
            Animasikan(PMain.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Tutup)
            TMainT.Enabled = True
        End If
    End Sub

    Private Sub PMain_MouseLeave(sender As Object, e As EventArgs) Handles PMulai.MouseLeave
        If darimain = True And sm = StatusMouse.DidalamTombol Then
            free = False
            sm = StatusMouse.DiluarTombol
            darimain = False
            Animasikan(PMulai.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Tutup)
            Animasikan(PMain.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Buka)
        End If
    End Sub

    Private Sub Form_Handle_MouseEnter(sender As Object, e As EventArgs) Handles PBack.MouseEnter
        free = True
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles TMainT.Tick
        If darimain = True And sm = StatusMouse.DidalamTombol And free = True Then
            free = False
            sm = StatusMouse.DiluarTombol
            darimain = False
            Animasikan(PMulai.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Tutup)
            Animasikan(PMain.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Buka)
            TMainT.Enabled = False
        End If
    End Sub

    Private Sub TFocus_Tick(sender As Object, e As EventArgs) Handles TFocus.Tick
        If BisaFokus = True Then
            'Me.Focus()
        End If
    End Sub

    Private Sub PMulai_Click(sender As Object, e As EventArgs) Handles PMulai.Click
        If Not Pertanyaan.Count > 0 Then
            KotakPesan.Show(Me, "Tidak ada pertanyaan satupun yang tersimpan" + vbNewLine + "Silakan tambahkan pertanyaan terlebih dahulu.", "Tidak Ada Pertanyaan", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Galat)
            Exit Sub
        End If
        daritombol = True
        Me.Close()
        Me.Dispose()
        Dim i As Integer = 0
        thmain.Abort()
        Form_Back.Focus()
        Form_Back.JalankanSurvey()
    End Sub
End Class