﻿Imports Transitions

Module Animate
    Public Sub TransText(control As Control, teks As String, Optional lama As Integer = 1000)
        Dim t As Transition = New Transition(New TransitionType_Linear(lama))
        t.add(control, "Text", teks)
        t.run()
    End Sub
End Module
