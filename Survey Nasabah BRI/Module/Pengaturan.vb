﻿Imports Crypt.Crypt
Imports System.IO
Imports Microsoft.Win32
Imports MessageHandle

Public Class Pengaturan
    Public Shared AlamatAppData As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Survei Nasabah BRI"
    Public Shared AlamatPengaturan As String = AlamatAppData + "\Pengaturan.ini"
    Public Shared KunciUtama As String = "@matganteng"
    Public Shared Data As List(Of String) = New List(Of String)
    Public Shared firstrun As Boolean = False
    Public Shared CRCFile As String

    'Pengaturan Utama ====================================================================================
    Public Shared tekspembuka, animmenuutama, animkemsurvey, animkempertanyaan As Boolean

    'Pengaturan Keamanan =================================================================================
    Public Shared identberkas, identacakkar As Boolean
    Public Shared kuncienk, password As String

    'Pengaturan Survey ===================================================================================
    Public Shared ototutuppass, ototutupsurvey As Boolean
    Public Shared wototutuppass, wototutupsurvey, wgayatrans, gayatrans As Integer

    'Pengaturan Laporan ==================================================================================
    Public Shared statistik As Boolean
    Public Shared alamatlaporan As String
    Public Shared modeltabel, modelisi As Integer

    'Pengaturan Pemilik ==================================================================================
    Public Shared nama, keterangan, alamat, notelp, email, website As String

    Private Shared Sub SimpanCRC(crc As String)
        Dim teks As String = ""
        Dim a As New Random()
        Dim b As Integer
        Microsoft.Win32.Registry.CurrentUser.CreateSubKey("SOFTWARE\Microsoft\Windows\Hash")
        Dim RegKey As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\Hash", True)
        For Each c As Char In crc
            teks += "*" + c
        Next
        b = a.Next(5, 10)
        For index = 0 To b
            teks += "*"
        Next
        RegKey.SetValue("CRC", "" & Encrypt(teks, "matganteng") & "")
        RegKey.Close()
    End Sub

    Private Shared Function AmbilCRC() As String
        Dim regkey As Microsoft.Win32.RegistryKey = Microsoft.Win32.Registry.CurrentUser.OpenSubKey("SOFTWARE\Microsoft\Windows\Hash", True)
        Dim hasil As String = regkey.GetValue("CRC", "gagal")
        regkey.Close()
        Dim teks As String = Decrypt(hasil, "matganteng")
        Return teks.Replace("*", "")
    End Function

    Public Shared Function GetCRC32(ByVal sFileName As String) As String
        Dim FS As FileStream = New FileStream(sFileName, FileMode.Open, FileAccess.Read, FileShare.Read, 8192)
        Try
            Dim CRC32Result As Integer = &HFFFFFFFF
            Dim Buffer(4096) As Byte
            Dim ReadSize As Integer = 4096
            Dim Count As Integer = FS.Read(Buffer, 0, ReadSize)
            Dim CRC32Table(256) As Integer
            Dim DWPolynomial As Integer = &HEDB88320
            Dim DWCRC As Integer
            Dim i As Integer, j As Integer, n As Integer

            'Create CRC32 Table
            For i = 0 To 255
                DWCRC = i
                For j = 8 To 1 Step -1
                    If (DWCRC And 1) Then
                        DWCRC = ((DWCRC And &HFFFFFFFE) \ 2&) And &H7FFFFFFF
                        DWCRC = DWCRC Xor DWPolynomial
                    Else
                        DWCRC = ((DWCRC And &HFFFFFFFE) \ 2&) And &H7FFFFFFF
                    End If
                Next j
                CRC32Table(i) = DWCRC
            Next i

            'Calcualting CRC32 Hash
            Do While (Count > 0)
                For i = 0 To Count - 1
                    n = (CRC32Result And &HFF) Xor Buffer(i)
                    CRC32Result = ((CRC32Result And &HFFFFFF00) \ &H100) And &HFFFFFF
                    CRC32Result = CRC32Result Xor CRC32Table(n)
                Next i
                Count = FS.Read(Buffer, 0, ReadSize)
            Loop
            Return Hex(Not (CRC32Result))
        Catch ex As Exception
            Return ""
        Finally
            FS.Close()
        End Try
    End Function

    Private Shared Function GetRandom() As String
        Dim g As New Random()
        Dim r As New Random
        Dim cur As Integer = r.Next(5, 15)
        Dim hasil As String = Nothing
        For index = 0 To cur
            hasil += CChar(ChrW(g.Next(48, 57)))
        Next
        Return hasil
    End Function

    Public Shared Sub SetPengaturanAsali()
        tekspembuka = True
        animmenuutama = True
        animkemsurvey = True
        animkempertanyaan = True

        identberkas = True
        identacakkar = True
        kuncienk = "ini adalah kunci enkripsi"
        password = "sedangkan ini password"

        ototutuppass = True
        ototutupsurvey = True
        wototutuppass = 10
        wototutupsurvey = 10
        wgayatrans = 300
        gayatrans = 9

        statistik = True
        alamatlaporan = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei\Laporan Survei Nasabah.pdf"
        modeltabel = 1
        modelisi = 1
    End Sub

    Public Shared Sub SimpanPengaturan()
        If Not IO.Directory.Exists(AlamatPengaturan) Then
            System.IO.Directory.CreateDirectory(AlamatAppData)
        End If

        Using berkas1 As New IO.StreamWriter(AlamatAppData + "\a.ini")
            berkas1.WriteLine(GetRandom())
            berkas1.Close()
        End Using

        Using Berkas As New IO.StreamWriter(AlamatPengaturan)
            Berkas.WriteLine("#Pengaturan Survei Bank BRI")
            Berkas.WriteLine("!Dilarang mengubah isi di dalam berkas ini secara manual, jika Anda merubahnya, maka Sistem Keamanan kami tidak akan memuat pengaturan di berkas ini.")
            Berkas.WriteLine("TerinstallPada=" + Now.Day.ToString + "-" + Now.Month.ToString + "-" + Now.Year.ToString)
            Berkas.WriteLine("CRC32=" + GetCRC32(AlamatAppData + "\a.ini"))
            Berkas.WriteLine("")
            Berkas.WriteLine("[Mulai]")
            Berkas.WriteLine(" [Utama]")
            Berkas.WriteLine("  TeksPembuka=" + tekspembuka.ToString)
            Berkas.WriteLine("  AnimMenuUtama=" + animmenuutama.ToString)
            Berkas.WriteLine("  AnimKeSurvey=" + animkemsurvey.ToString)
            Berkas.WriteLine("  AnimKePertanyaan=" + animkempertanyaan.ToString)
            Berkas.WriteLine("")
            Berkas.WriteLine(" [Keamanan]")
            Berkas.WriteLine("  IdentBerkas=" + identberkas.ToString)
            Berkas.WriteLine("  IdentAcakKar=" + identacakkar.ToString)
            Berkas.WriteLine("  KunciEnk=" + Encrypt(kuncienk + "]]" + GetRandom(), KunciUtama))
            Berkas.WriteLine("  Password=" + Encrypt(password + "]]" + GetRandom(), KunciUtama))
            Berkas.WriteLine("")
            Berkas.WriteLine(" [Survey]")
            Berkas.WriteLine("  OtoTutupPass=" + ototutuppass.ToString)
            Berkas.WriteLine("  OtoTutupSurvey=" + ototutupsurvey.ToString)
            Berkas.WriteLine("  WOtoTutupPass=" + wototutuppass.ToString)
            Berkas.WriteLine("  WOtoTutupSurvey=" + wototutupsurvey.ToString)
            Berkas.WriteLine("  WGayaTrans=" + wgayatrans.ToString)
            Berkas.WriteLine("  GayaTrans=" + gayatrans.ToString)
            Berkas.WriteLine("")
            Berkas.WriteLine(" [Laporan]")
            Berkas.WriteLine("  Statistik=" + statistik.ToString)
            Berkas.WriteLine("  AlamatLaporan=" + alamatlaporan)
            Berkas.WriteLine("  ModelTabel=" + modeltabel.ToString)
            Berkas.WriteLine("  ModelIsi=" + modelisi.ToString)
            Berkas.WriteLine("")
            Berkas.WriteLine(" [Pemilik]")
            Berkas.WriteLine("  Nama=" + nama)
            Berkas.WriteLine("  Keterangan=" + keterangan)
            Berkas.WriteLine("  Alamat=" + alamat)
            Berkas.WriteLine("  NoTelp=" + notelp)
            Berkas.WriteLine("  E-Mail=" + email)
            Berkas.WriteLine("  Website=" + website)
            Berkas.WriteLine("[Akhir]")
            Berkas.Close()
        End Using

        Try
            FileIO.FileSystem.DeleteFile(AlamatAppData + "\a.ini", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently, FileIO.UICancelOption.DoNothing)
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
        SimpanCRC(GetCRC32(AlamatPengaturan))
    End Sub

    Public Shared Sub BacaPengaturan()
        Dim sem As String
        Data.Clear()
        If Not IO.File.Exists(AlamatPengaturan) Then
            SetPengaturanAsali()
            firstrun = True
            Exit Sub
        End If
        firstrun = False
        If GetCRC32(AlamatPengaturan) <> AmbilCRC() Then
            KotakPesan.Show(Form_MenuUtama, "Kami mendeteksi adanya perubahan yang dilakukan di luar aplikasi ini terhadap berkas pengaturan kami, kami tidak akan memuat data pengaturan tersebut demi keamanan." + Environment.NewLine + Environment.NewLine + "Jika Anda mengetahui hal ini, silakan atur ulang pengaturan dengan memasukkan kunci enkripsi yang sama." + Environment.NewLine + Environment.NewLine + "Kami akan mereset semua pengaturan.", "Sistem Keamanan Terpicu", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror, 13)
            FileIO.FileSystem.DeleteFile(AlamatPengaturan, FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently, FileIO.UICancelOption.DoNothing)
            SetPengaturanAsali()
            firstrun = True
            Exit Sub
        End If

        Using Berkas As New IO.StreamReader(AlamatPengaturan)
            While Not Berkas.EndOfStream
                sem = Berkas.ReadLine
                If sem.Contains("  ") Then
                    If sem.Contains("=") And Not sem.Contains("Kunci") And Not sem.Contains("Password") Then
                        Data.Add(sem.Substring(sem.LastIndexOf("=") + 1))
                    Else
                        If sem.Contains("KunciEnk") Then
                            Dim a As String = sem.Replace("  KunciEnk=", "")
                            Dim b As String = Decrypt(a, KunciUtama)
                            Dim c As String = b.Remove(b.LastIndexOf("]]"))
                            Data.Add(c)
                        ElseIf sem.Contains("Password") Then
                            Dim a As String = sem.Replace("  Password=", "")
                            Dim b As String = Decrypt(a, KunciUtama)
                            Dim c As String = b.Remove(b.LastIndexOf("]]"))
                            Data.Add(c)
                        End If
                    End If
                End If
            End While
            Berkas.Close()
        End Using

        tekspembuka = Data(0)
        animmenuutama = Data(1)
        animkemsurvey = Data(2)
        animkempertanyaan = Data(3)

        identberkas = Data(4)
        identacakkar = Data(5)
        kuncienk = Data(6)
        password = Data(7)

        ototutuppass = Data(8)
        ototutupsurvey = Data(9)
        wototutuppass = Data(10)
        wototutupsurvey = Data(11)
        wgayatrans = Data(12)
        gayatrans = Data(13)

        statistik = Data(14)
        alamatlaporan = Data(15)
        modeltabel = Data(16)
        modelisi = Data(17)

        nama = Data(18)
        keterangan = Data(19)
        alamat = Data(20)
        notelp = Data(21)
        email = Data(22)
        website = Data(23)
    End Sub
End Class