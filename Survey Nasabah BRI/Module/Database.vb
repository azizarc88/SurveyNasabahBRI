﻿Imports Crypt.Crypt

Module Database
    Public Pertanyaan As List(Of String()) = New List(Of String())
    Public PilJawaban As List(Of String()) = New List(Of String())
    Public AlamatAppData As String = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Survei Nasabah BRI"
    Public Hari As String() = {"Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu", "Minggu"}
    Public Bulan As String() = {"", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"}
    Public Jawaban As List(Of String()) = New List(Of String())

    Public Function KonversiTanggalUni(ByRef tanggal As String) As DateTime
        Dim dt As DateTime
        Dim a As Integer = 0
        Dim t As String = tanggal.Substring(tanggal.IndexOf(" ") + 1, (tanggal.IndexOf("-") - 1) - tanggal.IndexOf(" ") - 1)
        Dim b As String = t.Substring(t.IndexOf(" ") + 1, t.LastIndexOf(" ") - t.IndexOf(" ") - 1)
        Dim j As String = tanggal.Substring(tanggal.LastIndexOf("- ") + 2)

        Dim _tanggal, _bulan, _tahun, _menit, _detik As Integer
        Dim jam As Integer = CInt(j.Substring(0, 2))
        Dim AorP As String = j.Substring(j.LastIndexOf(" ") + 1)

        _tanggal = CInt(t.Substring(0, 2))
        For Each item As String In Bulan
            If item = b Then
                _bulan = a
                Exit For
            End If
            a += 1
        Next
        _tahun = t.Substring(t.LastIndexOf(" ") + 1)
        If AorP = "PM" Then
            jam = jam + 12
        End If
        _menit = CInt(j.Substring(j.IndexOf(":") + 1, 2))
        _detik = CInt(j.Substring(j.LastIndexOf(":") + 1, 2))
        dt = New DateTime(_tahun, _bulan, _tanggal, jam, _menit, _detik)
        Return dt
    End Function

    Public Function KonversiTanggalInd(ByVal tanggal As DateTime) As String
        Dim hasil, h, t, b, th, j, m, d As String
        b = ""
        Dim s As String = "AM"
        If tanggal.DayOfWeek.ToString = "Sunday" Then
            h = "Minggu"
        ElseIf tanggal.DayOfWeek.ToString = "Monday" Then
            h = "Senin"
        ElseIf tanggal.DayOfWeek.ToString = "Tuesday" Then
            h = "Selasa"
        ElseIf tanggal.DayOfWeek.ToString = "Wednesday" Then
            h = "Rabu"
        ElseIf tanggal.DayOfWeek.ToString = "Thursday" Then
            h = "Kamis"
        ElseIf tanggal.DayOfWeek.ToString = "Friday" Then
            h = "Jum'at"
        ElseIf tanggal.DayOfWeek.ToString = "Saturday" Then
            h = "Sabtu"
        Else
            h = "GALAT " + tanggal.DayOfWeek.ToString
        End If
        t = tanggal.Day
        b = Bulan(tanggal.Month)
        th = tanggal.Year

        If tanggal.Hour > 12 Then
            j = (tanggal.Hour - 12).ToString
            s = "PM"
        Else
            j = tanggal.Hour.ToString
        End If
        If j.Length = 1 Then
            j = "0" & j
        End If

        If tanggal.Minute.ToString.Length = 1 Then
            m = "0" & tanggal.Minute.ToString
        Else
            m = tanggal.Minute.ToString
        End If

        d = tanggal.Second
        If d.Length = 1 Then
            d = "0" & d
        End If

        hasil = h + ", " + t + " " + b + " " + th + " - " + j + ":" + m + ":" + d + " " + s
        Return hasil
    End Function

    Public Sub SimpanPertanyaan(ByVal Koleksi1 As List(Of String()), ByVal Koleksi2 As List(Of String()))
        Dim r As New Random
        My.Computer.FileSystem.CreateDirectory(AlamatAppData)
        Dim a As Integer = 0
        Dim tmbol As String = Nothing
        Dim NamaBerkas As String = "Pertanyaan.dft"
        Using Berkas As New IO.StreamWriter(AlamatAppData + "\" + NamaBerkas)
            Berkas.WriteLine(Encrypt("#Daftar " + NamaBerkas.Remove(NamaBerkas.IndexOf(".")), Pengaturan.kuncienk))
            Berkas.WriteLine(Encrypt("#v 1.0 untuk SN Bank BRI versi 1.0 ke atas", Pengaturan.kuncienk))
            Berkas.WriteLine(Encrypt("!Silakan edit secara manual di sini jika ingin menambah / mengubah / menghapus daftar Pertanyaan", Pengaturan.kuncienk))
            Berkas.WriteLine(Encrypt(r.Next().ToString + r.Next().ToString + r.Next().ToString + r.Next().ToString + r.Next().ToString, Pengaturan.kuncienk))
            Berkas.WriteLine(Encrypt("[Mulai]", Pengaturan.kuncienk))
            Berkas.WriteLine(Encrypt(" [Pertanyaan]", Pengaturan.kuncienk))
            Try
                For Each Data As String() In Koleksi1
                    Berkas.WriteLine(Encrypt("  [ID]" + Data(1), Pengaturan.kuncienk))
                    Berkas.WriteLine(Encrypt("   [P]" + Data(0), Pengaturan.kuncienk))
                    For Each s As String In Koleksi2(a)
                        Berkas.WriteLine(Encrypt("    [J]" + s, Pengaturan.kuncienk))
                    Next
                    tmbol = Nothing
                    a += 1
                Next
            Catch ex As NullReferenceException
                'skip
            End Try
            Berkas.WriteLine(Encrypt("[Akhir]", Pengaturan.kuncienk))
        End Using
    End Sub

    Public Sub BacaPertanyaan()
        Dim sem As String
        Dim tbl As List(Of String) = New List(Of String)
        Pertanyaan.Clear()
        PilJawaban.Clear()
        Try
            Using Berkas As New IO.StreamReader(AlamatAppData + "\Pertanyaan.dft")
                Dim id As String = "EROR BACA"
                While Not Berkas.EndOfStream
                    sem = Decrypt(Berkas.ReadLine, Pengaturan.kuncienk)
                    If sem.Contains("    [J]") Then
                        tbl.Add(sem.Replace("    [J]", ""))
                    ElseIf sem.Contains("  [ID]") Then
                        id = sem.Replace("  [ID]", "")
                    ElseIf sem.Contains("   [P]") Then
                        Pertanyaan.Add({sem.Replace("   [P]", ""), id})
                        If Not tbl.Count = 0 Then
                            PilJawaban.Add(tbl.ToArray)
                        End If
                        tbl.Clear()
                    ElseIf sem.Contains("[Akhir]") Then
                        If Not tbl.Count = 0 Then
                            PilJawaban.Add(tbl.ToArray)
                        End If
                        tbl.Clear()
                    End If
                End While
            End Using
        Catch ex As Exception
        End Try
    End Sub

    Public Function RandomKan(Optional a As Integer = 10) As String
        Dim r As Random = New Random
        Dim i As Integer = r.Next(0, a) + (Now.Millisecond / 100)
        Dim kar As String()
        Dim hasil As String = ""
        Dim b As Integer

        kar = {"#", ".", "!", "/", "\", "#", "G", "?", "=", "'", ",", ".", "-", ":", " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", _
              "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", _
               "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"}

        For index = 0 To i
            b = r.Next(0, 76) + Now.Second
            If b > 76 Then
                b = b - 76
            End If

            hasil += kar(b)
        Next
        Return hasil
    End Function

    Public Sub TambahJawaban(ByVal namaberkas As String, ByVal pertanyaan As String, ByVal b As Button)
        Dim r As Random = New Random
        My.Computer.FileSystem.CreateDirectory(AlamatAppData + "\Jawaban")
        Dim a As Integer = 0
        Dim tmbol As String = Nothing
        If Not IO.File.Exists(AlamatAppData + "\Jawaban\" + namaberkas) Then
            Using Berkas As New IO.StreamWriter(AlamatAppData + "\Jawaban\" + namaberkas)
                Berkas.WriteLine(Encrypt("[Info]Daftar Jawaban untuk pertanyaan: " + RandomKan() + "[" + namaberkas + "]" + RandomKan(5), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt("#Pertanyaan: " + pertanyaan, Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(RandomKan(2), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt("[Jawaban]", Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(" =\=" + RandomKan(2), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(r.Next(0, 100).ToString + "[Info]: " + "[" + namaberkas + "]" + RandomKan(5), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(RandomKan(3) + "  [Waktu] [" + KonversiTanggalInd(Now), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(r.Next(0, 1000).ToString + "  [Jawaban]" + "[" + b.Name + "]" + RandomKan(5), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(" =/=" + RandomKan(3) + vbNull.ToString + vbNull.ToString + vbNull.ToString + vbNull.ToString + vbNull.ToString + vbNull.ToString + vbNull.ToString + vbNull.ToString, Pengaturan.kuncienk))
            End Using
        Else
            Using Berkas As New IO.StreamWriter(AlamatAppData + "\Jawaban\" + namaberkas, True)
                Berkas.WriteLine(Encrypt(r.Next(0, 100).ToString + "[Info]: " + "[" + namaberkas + "]" + RandomKan(5), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(r.Next(0, 1000).ToString + "  [Waktu] [" + KonversiTanggalInd(Now), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(RandomKan(3) + "  [Jawaban]" + "[" + b.Name + "]" + RandomKan(5), Pengaturan.kuncienk))
                Berkas.WriteLine(Encrypt(" =/=" + RandomKan(1), Pengaturan.kuncienk))
            End Using
        End If
    End Sub

    Public Sub BacaJawaban(namaform As Form)
        Jawaban.Clear()
        If Not IO.Directory.Exists(AlamatAppData + "\Jawaban") Then
            Exit Sub
        End If
        Dim daftar As New IO.DirectoryInfo(AlamatAppData + "\Jawaban")
        Dim djberkas As IO.FileInfo() = daftar.GetFiles("*.jb")
        Dim ada As Boolean = False
        Dim daftarjawaban As List(Of String) = New List(Of String)
        For Each p As String() In Pertanyaan
            ada = False
            For Each berkas As IO.FileInfo In djberkas
                If p(1) = berkas.Name.Replace(".jb", "") Then
                    ada = True
                End If
            Next
            If ada = True Then
                daftarjawaban.Add(p(1))
            End If
        Next
        Dim sem As String = Nothing
        Dim waktu As String = Nothing
        Dim jb As String = Nothing
        For Each id As String In daftarjawaban
            Using Berkas As New IO.StreamReader(AlamatAppData + "\Jawaban\" + id + ".jb")
                While Not Berkas.EndOfStream
                    sem = Decrypt(Berkas.ReadLine, Pengaturan.kuncienk)
                    If sem.Contains("  [Waktu]") Then
                        waktu = sem.Substring(sem.LastIndexOf("[") + 1)
                    ElseIf sem.Contains("  [Jawaban]") Then
                        jb = sem.Substring(sem.LastIndexOf("[") + 1, 2)
                    ElseIf sem.Contains(" =/=") Then
                        Jawaban.Add({id, waktu, jb})
                    ElseIf sem.Contains("[Info]") Then
                        jb = sem.Substring(sem.LastIndexOf("[") + 1, 5)
                        If Not jb = id Then
                            MessageHandle.KotakPesan.Show(namaform, "Terdapat berkas yang sudah dimodifikasi oleh seseorang dengan nama berkas: """ + id + ".jb""" + vbNewLine + vbNewLine + "Kami tidak akan memuat data di berkas ini", "Manipulasi Data", MessageHandle.KotakPesan.TombolPesan.Ok, MessageHandle.KotakPesan.Warna.Eror)
                            Exit For
                        End If
                    End If
                End While
            End Using
        Next
    End Sub
End Module
