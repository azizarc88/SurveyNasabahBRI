﻿Imports Laporan.Berkas
Imports Laporan.Berkas.PDF
Imports Laporan.ImageTool
Imports System.IO
Imports System.Text
Imports System.Security.AccessControl
Imports System.Security.Principal

Module Reports
    Dim document As Document
    Dim p1, p2, p3, p4, p5 As String
    Dim j1, j2, j3, j4, j5 As String

    <Flags()> _
    Public Enum Gaya
        Bold = Font.BOLD
        Italic = Font.ITALIC
        Underline = Font.UNDERLINE
        BoldItalic = Font.BOLDITALIC
        Reguler = Font.NORMAL
    End Enum

    Public Function Huruf(namafont As String, ukuran As Single, gaya As Gaya) As Laporan.Berkas.Font
        Dim dirWindowsFolder As DirectoryInfo = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.System))
        'Dim strFontsFolder As String = Path.Combine(dirWindowsFolder.FullName, "Fonts")
        Dim customfont As BaseFont = BaseFont.CreateFont(CurDir() & "\Huruf\" & namafont, BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim h As New Font(customfont, ukuran, gaya)
        Return h
    End Function

    Public Function GetTextTombolAll(ByVal id As String) As String()
        Dim hasil As String()
        Dim a As Integer
        For Each s As String() In Pertanyaan
            If s(1) = id Then
                Exit For
            End If
            a += 1
        Next
        hasil = PilJawaban(a)
        Return hasil
    End Function

    Public Sub AturStatistik(ByVal id As String)
        Dim daftar As List(Of String()) = New List(Of String())
        Dim datatexttombol As String() = GetTextTombolAll(id)
        Dim _1, _2, _3, _4, _5, jumlah As Integer
        For Each s As String() In Jawaban
            If id = s(0) Then
                daftar.Add(s)
            End If
        Next

        For Each d As String() In daftar
            If d(2).Contains("1") Then
                _1 += 1
            ElseIf d(2).Contains("2") Then
                _2 += 1
            ElseIf d(2).Contains("3") Then
                _3 += 1
            ElseIf d(2).Contains("4") Then
                _4 += 1
            ElseIf d(2).Contains("5") Then
                _5 += 1
            End If
        Next

        jumlah = Form_PData_HasilSurvey.GetTextTombol("B1", id, False)(0)
        If jumlah = 2 Then
            p1 = ""
            j1 = ""
            p2 = datatexttombol(0) + " :"
            j2 = _1
            p3 = ""
            j3 = ""
            p4 = datatexttombol(1) + " :"
            j4 = _2
            p5 = ""
            j5 = ""
        ElseIf jumlah = 3 Then
            p1 = ""
            j1 = ""
            p2 = datatexttombol(0) + " :"
            j2 = _1
            p3 = datatexttombol(1) + " :"
            j3 = _2
            p4 = datatexttombol(2) + " :"
            j4 = _3
            p5 = ""
            j5 = ""
        ElseIf jumlah = 4 Then
            p1 = datatexttombol(0) + " :"
            j1 = _1
            p2 = datatexttombol(1) + " :"
            j2 = _2
            p3 = ""
            j3 = ""
            p4 = datatexttombol(2) + " :"
            j4 = _3
            p5 = datatexttombol(3) + " :"
            j5 = _4
        ElseIf jumlah = 5 Then
            p1 = datatexttombol(0) + " :"
            j1 = _1
            p2 = datatexttombol(1) + " :"
            j2 = _2
            p3 = datatexttombol(2) + " :"
            j3 = _3
            p4 = datatexttombol(3) + " :"
            j4 = _4
            p5 = datatexttombol(4) + " :"
            j5 = _5
        End If
    End Sub

    Private Function Singkat(teks As String)
        Dim hasil As String = Nothing
        Dim awal As String = ""
        Dim hasil2 As String = ""
        If teks.Length > 14 Then
            hasil += teks.Chars(0).ToString + ". "
            awal = teks.Chars(0).ToString + ". "
            hasil += teks.Substring(teks.IndexOf(" ") + 1).ToString
        Else
            hasil = teks
        End If

        If hasil.Length > 14 Then
            If hasil.Contains("    :") Then
                hasil = hasil.Replace("    :", "")
            ElseIf hasil.Contains("   :") Then
                hasil = hasil.Replace("   :", "")
            ElseIf hasil.Contains("  :") Then
                hasil = hasil.Replace("  :", "")
            ElseIf hasil.Contains(" :") Then
                hasil = hasil.Replace(" :", "")
            End If
            hasil2 += awal
            hasil2 += hasil.Chars(3).ToString + ". "
            hasil2 += hasil.Substring(hasil.LastIndexOf(" ") + 1).ToString
            hasil = hasil2 + " :"
        End If
        Return hasil
    End Function

    Private Sub BuatTabelPertanyaan()
        Dim a As Integer = 0
        If Pengaturan.modeltabel = 0 Then
            a = 1
        End If
        Dim JudulTabel As New Paragraph("Daftar Pertanyaan", Huruf("CORBEL_5.TTF", 19, Gaya.Bold))
        JudulTabel.Alignment = Element.ALIGN_CENTER
        document.Add(JudulTabel)

        Dim f As New Font(FontFactory.GetFont("Arial", 14, Font.NORMAL))
        Dim HeaderCell As New PdfPCell
        Dim LeftCell As New PdfPCell
        Dim widths As Single() = New Single() {0.5, 1, 6}
        Dim table As New PdfPTable(3)
        table.TotalWidth = 495
        table.LockedWidth = True
        table.SetWidths(widths)
        table.HorizontalAlignment = 0
        table.SpacingBefore = 15
        table.SpacingAfter = 15
        table.HorizontalAlignment = Element.ALIGN_CENTER
        table.DefaultCell.HorizontalAlignment = Pengaturan.modelisi
        table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER

        HeaderCell.FixedHeight = 25
        HeaderCell.HorizontalAlignment = 1
        LeftCell.HorizontalAlignment = Element.ALIGN_CENTER

        table.DefaultCell.BorderWidthTop = a
        table.DefaultCell.BorderWidthLeft = a
        table.DefaultCell.BorderWidthRight = a
        table.DefaultCell.BorderWidthBottom = a
        HeaderCell.BorderWidthTop = a
        HeaderCell.BorderWidthLeft = a
        HeaderCell.BorderWidthRight = a
        HeaderCell.BorderWidthBottom = 1
        LeftCell.BorderWidthTop = a
        LeftCell.BorderWidthLeft = a
        LeftCell.BorderWidthRight = 1
        LeftCell.BorderWidthBottom = a

        HeaderCell.Phrase = New Phrase("No.", f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase("ID", f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase("Pertanyaan", f)
        table.AddCell(HeaderCell)

        Dim nomor As Integer = 1
        For Each p As String() In Form_Report.DaftarPertanyaan
            LeftCell.Phrase = New Phrase(nomor.ToString + ".", f)
            table.AddCell(LeftCell)
            table.AddCell(p(1))
            table.AddCell(p(0))
            nomor += 1
        Next

        document.Add(table)
    End Sub

    Private Sub BuatTabelJawaban(ByVal pertanyaan As String())
        Dim a As Integer = 0
        If Pengaturan.modeltabel = 0 Then
            a = 1
        End If
        Dim JudulTabel As New Paragraph("Pertanyaan ID " + pertanyaan(1), Huruf("CORBEL_5.TTF", 19, Gaya.Bold))
        Dim Keterangan As New Paragraph("Pertanyaan: " + pertanyaan(0), Huruf("CORBEL_5.TTF", 15, Gaya.Reguler))
        JudulTabel.Alignment = Element.ALIGN_CENTER
        JudulTabel.SpacingAfter = 8
        Keterangan.SpacingAfter = 3
        document.Add(JudulTabel)
        document.Add(Keterangan)

        Dim f As New Font(FontFactory.GetFont("Arial", 14, Font.NORMAL))
        Dim HeaderCell As New PdfPCell
        Dim LeftCell As New PdfPCell
        Dim widths As Single() = New Single() {0.5, 5, 2}
        Dim table As New PdfPTable(3)
        table.TotalWidth = 495
        table.LockedWidth = True
        table.SetWidths(widths)
        table.HorizontalAlignment = 0
        table.SpacingBefore = 5
        table.SpacingAfter = 5
        table.HorizontalAlignment = Element.ALIGN_CENTER
        table.DefaultCell.HorizontalAlignment = Pengaturan.modelisi
        LeftCell.HorizontalAlignment = Element.ALIGN_CENTER

        HeaderCell.FixedHeight = 25
        HeaderCell.HorizontalAlignment = 1

        table.DefaultCell.BorderWidthTop = a
        table.DefaultCell.BorderWidthLeft = a
        table.DefaultCell.BorderWidthRight = a
        table.DefaultCell.BorderWidthBottom = a
        HeaderCell.BorderWidthTop = a
        HeaderCell.BorderWidthLeft = a
        HeaderCell.BorderWidthRight = a
        HeaderCell.BorderWidthBottom = 1
        LeftCell.BorderWidthTop = a
        LeftCell.BorderWidthLeft = a
        LeftCell.BorderWidthRight = 1
        LeftCell.BorderWidthBottom = a

        HeaderCell.Phrase = New Phrase("No.", f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase("Waktu", f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase("Jawaban", f)
        table.AddCell(HeaderCell)

        Dim nomor As Integer = 1
        For Each p As String() In Form_Report.HasilSurvey
            If p(0) = pertanyaan(1) Then
                LeftCell.Phrase = New Phrase(nomor.ToString + ".", f)
                table.AddCell(LeftCell)
                table.AddCell(p(2))
                table.AddCell(p(3))
                nomor += 1
            End If
        Next

        document.Add(table)
        If Pengaturan.statistik = True Then
            BuatTabelStatistik(pertanyaan(1))
        End If
    End Sub

    Public Sub BuatTabelStatistik(ByVal ID As String)
        Dim Keterangan As New Paragraph("Statistik", Huruf("CORBEL_5.TTF", 15, Gaya.Reguler))
        Keterangan.SpacingBefore = 0
        Keterangan.SpacingAfter = 3
        Keterangan.Alignment = Element.ALIGN_CENTER
        document.Add(Keterangan)

        Dim f As New Font(FontFactory.GetFont("Arial", 10, Font.NORMAL))
        Dim HeaderCell As New PdfPCell
        Dim widths As Single() = New Single() {5, 5, 4, 5, 5}
        Dim table As New PdfPTable(5)
        table.TotalWidth = 515
        table.LockedWidth = True
        table.SetWidths(widths)
        table.HorizontalAlignment = 0
        table.SpacingBefore = 5
        table.SpacingAfter = 15
        table.HorizontalAlignment = Element.ALIGN_CENTER

        HeaderCell.FixedHeight = 20
        HeaderCell.HorizontalAlignment = Element.ALIGN_CENTER
        HeaderCell.VerticalAlignment = Element.ALIGN_CENTER
        HeaderCell.BorderWidthTop = 1
        HeaderCell.BorderWidthLeft = 0
        HeaderCell.BorderWidthRight = 0
        HeaderCell.BorderWidthBottom = 1
        HeaderCell.PaddingTop = 3

        AturStatistik(ID)

        HeaderCell.Phrase = New Phrase(Singkat(p1) + " " + j1, f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase(Singkat(p2) + " " + j2, f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase(Singkat(p3) + " " + j3, f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase(Singkat(p4) + " " + j4, f)
        table.AddCell(HeaderCell)
        HeaderCell.Phrase = New Phrase(Singkat(p5) + " " + j5, f)
        table.AddCell(HeaderCell)

        document.Add(table)
    End Sub

    Sub BuatPDF()
        document = New Document(PageSize.A4, 50, 50, 50, 50)

        Dim filename As String = Pengaturan.alamatlaporan
        Try
            Try
                PdfWriter.GetInstance(document, New FileStream(filename, FileMode.Create))
            Catch ex As Exception
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei")
                Dim sec As DirectorySecurity = Directory.GetAccessControl(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei")
                ' Using this instead of the "Everyone" string means we work on non-English systems.
                Dim everyone As New SecurityIdentifier(WellKnownSidType.WorldSid, Nothing)
                sec.AddAccessRule(New FileSystemAccessRule(everyone, FileSystemRights.Modify Or FileSystemRights.Synchronize, InheritanceFlags.ContainerInherit Or InheritanceFlags.ObjectInherit, PropagationFlags.None, AccessControlType.Allow))
                Directory.SetAccessControl(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei", sec)
                PdfWriter.GetInstance(document, New FileStream(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei\Laporan Survei Nasabah.pdf", FileMode.Create))
                Pengaturan.alamatlaporan = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\Laporan Survei\Laporan Survei Nasabah.pdf"
            End Try
            document.Open()

            Dim Judul As New Paragraph("LAPORAN SURVEI NASABAH", Huruf("CORBEL_5.TTF", 19, Gaya.Bold))
            Judul.IndentationLeft = 60
            Dim SubJudul As New Paragraph(Pengaturan.nama.ToUpper + " " + Pengaturan.keterangan.ToUpper, Huruf("CORBEL_5.TTF", 18, Gaya.Bold))
            SubJudul.IndentationLeft = 60
            Dim Keterangan As New Paragraph(Pengaturan.alamat + ", " + Pengaturan.notelp + ", " + Pengaturan.email, Huruf("CORBEL_5.TTF", 10, Gaya.Bold))
            Keterangan.IndentationLeft = 60
            Dim Garis As Paragraph = New Paragraph(New Chunk(New Laporan.Berkas.PDF.Gambar.LineSeparator(2, 100.0F, BaseColor.BLACK, Element.ALIGN_CENTER, 1)))
            Dim CellIsi As New PdfPCell

            Dim Gambar As Image = Image.GetInstance(CurDir() + "\Laporan\logo_bri.png")

            Judul.Alignment = Element.ALIGN_CENTER
            SubJudul.Alignment = Element.ALIGN_CENTER
            Keterangan.Alignment = Element.ALIGN_CENTER
            Garis.SpacingAfter = 15
            Gambar.Alignment = Image.UNDERLYING
            Gambar.ScaleToFit(75, 75)
            Gambar.SetAbsolutePosition(60, 712)

            document.AddAuthor("Aplikasi Survei Nasabah")
            document.AddCreator("Survei Nasabah " + Application.ProductVersion)
            document.AddTitle("Laporan Survei Nasabah")

            document.Add(Gambar)
            document.Add(Judul)
            document.Add(SubJudul)
            document.Add(Keterangan)
            document.Add(Garis)
            BuatTabelPertanyaan()
            For Each item As String() In Form_Report.DaftarPertanyaan
                BuatTabelJawaban(item)
            Next

        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        Finally
            document.Close()
        End Try
    End Sub
End Module
