﻿Imports AnimasiUI.Class_Animasi
Imports Survey_Nasabah_BRI.Form_Back

Public Class Animasi
    Private context As Threading.SynchronizationContext = Threading.SynchronizationContext.Current
    Dim maju As Boolean
    Dim nt As NamaTombol

    Public Event GambarTombolBerubah As EventHandler(Of GambarTombolBerubahEventArgs)
    Protected Overridable Sub SaatGambarTombolBerubah(ByVal e As GambarTombolBerubahEventArgs)
        RaiseEvent GambarTombolBerubah(Me, e)
    End Sub

    Public Event AnimasiTombolSelesai As EventHandler(Of AnimasiTombolSelesaiEventArgs)
    Protected Overridable Sub SaatAnimasiTombolSelesai(ByVal e As AnimasiTombolSelesaiEventArgs)
        RaiseEvent AnimasiTombolSelesai(Me, e)
    End Sub

    Public Sub AnimTombolAsync(ByVal Gambar As List(Of Image), ByVal nmt As NamaTombol, Optional ByVal _Maju As Boolean = True)
        ThreadExtensions.QueueUserWorkItem(New Action(Of List(Of Image))(AddressOf AnimasiTombol), Gambar)
        maju = _Maju
        nt = nmt
    End Sub

    Public Sub AnimasiTombol(ByVal Gambar As List(Of Image))
        Dim e As GambarTombolBerubahEventArgs
        If maju = True Then
            For Each img As Image In Gambar
                Application.DoEvents()
                e = New GambarTombolBerubahEventArgs(img, nt)
                Application.DoEvents()
                jeda(1)
                If context Is Nothing Then
                    Application.DoEvents()
                    SaatGambarTombolBerubah(e)
                    Application.DoEvents()
                Else
                    Application.DoEvents()
                    ThreadExtensions.ScSend(context, New Action(Of GambarTombolBerubahEventArgs)(AddressOf SaatGambarTombolBerubah), e)
                    Application.DoEvents()
                End If
            Next

            If context Is Nothing Then
                SaatAnimasiTombolSelesai(New AnimasiTombolSelesaiEventArgs(False, True, nt))
            Else
                ThreadExtensions.ScSend(context, New Action(Of AnimasiTombolSelesaiEventArgs)(AddressOf SaatAnimasiTombolSelesai), New AnimasiTombolSelesaiEventArgs(False, True, nt))
            End If
        Else
            Dim jumlah As Integer = Gambar.Count
            Dim a As Integer = jumlah - 1
            Do Until a = 0
                Application.DoEvents()
                e = New GambarTombolBerubahEventArgs(Gambar(a), nt)
                Application.DoEvents()
                jeda(1)
                If context Is Nothing Then
                    Application.DoEvents()
                    SaatGambarTombolBerubah(e)
                    Application.DoEvents()
                Else
                    Application.DoEvents()
                    ThreadExtensions.ScSend(context, New Action(Of GambarTombolBerubahEventArgs)(AddressOf SaatGambarTombolBerubah), e)
                    Application.DoEvents()
                End If
                a -= 1
            Loop

            If context Is Nothing Then
                SaatAnimasiTombolSelesai(New AnimasiTombolSelesaiEventArgs(False, False, nt))
            Else
                ThreadExtensions.ScSend(context, New Action(Of AnimasiTombolSelesaiEventArgs)(AddressOf SaatAnimasiTombolSelesai), New AnimasiTombolSelesaiEventArgs(False, False, nt))
            End If
        End If
    End Sub
End Class

Public Class ThreadExtensions
    Private args() As Object
    Private DelegateToInvoke As [Delegate]

    Public Shared Sub ScSend(ByVal sc As Threading.SynchronizationContext, ByVal del As [Delegate], ByVal ParamArray args() As Object)
        Application.DoEvents()
        sc.Send(New Threading.SendOrPostCallback(AddressOf ProperDelegate), New ThreadExtensions With {.args = args, .DelegateToInvoke = del})
    End Sub

    Private Shared Sub ProperDelegate(ByVal state As Object)
        Application.DoEvents()
        Dim sd As ThreadExtensions = DirectCast(state, ThreadExtensions)
        Try
            sd.DelegateToInvoke.DynamicInvoke(sd.args)
        Catch ex As Exception
        End Try
        Application.DoEvents()
    End Sub

    Public Shared Function QueueUserWorkItem(ByVal method As [Delegate], ByVal ParamArray args() As Object) As Boolean
        Return Threading.ThreadPool.QueueUserWorkItem(AddressOf ProperDelegate, New ThreadExtensions With {.args = args, .DelegateToInvoke = method})
    End Function
End Class

Public Class GambarTombolBerubahEventArgs
    Inherits EventArgs

    Private _Gambar As Image
    Private _Nama As NamaTombol

    Public Sub New(ByVal gambar As Image, ByVal nama As NamaTombol)
        _Gambar = gambar
        _Nama = nama
    End Sub

    Public ReadOnly Property Gambar() As Image
        Get
            Return _Gambar
        End Get
    End Property

    Public ReadOnly Property Nama() As String
        Get
            Return _Nama
        End Get
    End Property
End Class

Public Class AnimasiTombolSelesaiEventArgs
    Inherits EventArgs
    Private _memproses As Boolean = True
    Private _maju As Boolean = True
    Private _nt As NamaTombol

    Public Sub New(ByVal memproses As Boolean, ByVal maju As Boolean, ByVal nt As NamaTombol)
        _memproses = memproses
        _maju = maju
        _nt = nt
    End Sub

    Public ReadOnly Property IsProcessing() As Boolean
        Get
            Return _memproses
        End Get
    End Property

    Public ReadOnly Property IsMaju() As Boolean
        Get
            Return _maju
        End Get
    End Property

    Public ReadOnly Property NamaTombol() As NamaTombol
        Get
            Return _nt
        End Get
    End Property
End Class
