﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("Survei Nasabah BRI")>
<Assembly: AssemblyDescription("Aplikasi survei penilaian layanan nasabah Bank Rakyat Indonesia")>
<Assembly: AssemblyCompany("Aziz Nur Ariffianto")>
<Assembly: AssemblyProduct("Survei Nasabah BRI")>
<Assembly: AssemblyCopyright("Copyright © 2013 - 2016 Aziz Nur Ariffianto")>
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("77f6a1c8-7b33-4e2e-a3f5-042957a54b5f")>

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("4.1.33.4")>
<Assembly: AssemblyFileVersion("4.1.33.4")>
