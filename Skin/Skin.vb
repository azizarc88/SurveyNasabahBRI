﻿Public Class UserInterface
    Public Shared ikon As System.Drawing.Icon
    Public Shared Function IkonAnimasi(ByVal index As Integer) As System.Drawing.Icon
        ikon = My.Resources.ResourceManager.GetObject("_" & index)
        Return ikon
    End Function
End Class

Public Class Gambar
    Public Shared offsetbawah As Integer = 5
    Public Shared offsetatas As Integer = -3

    Public Shared Gambar As System.Drawing.Image
    Public Shared Function AmbilGambar(ByVal nama As String) As System.Drawing.Image
        Gambar = My.Resources.ResourceManager.GetObject(nama.ToLower)
        Return Gambar
    End Function

    Public Shared Function AmbilGambarAnimasi(ByVal nama As String, ByVal index As Integer) As System.Drawing.Image
        Gambar = My.Resources.ResourceManager.GetObject(nama & "_" & index)
        Return Gambar
    End Function
End Class