﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LaporanPDF")]
[assembly: AssemblyDescription("Berkas Library laporan (PDF)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Aziz Nur Ariffianto")]
[assembly: AssemblyProduct("LaporanPDF")]
[assembly: AssemblyCopyright("Copyright © 2013 - 2015 Aziz Nur Ariffianto")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("96b5f452-b9dc-4c97-b280-3b533196420f")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("3.6.2.0")]
[assembly: AssemblyFileVersion("3.6.2.0")]
