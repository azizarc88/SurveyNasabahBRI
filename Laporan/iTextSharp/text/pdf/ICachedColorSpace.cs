﻿using System;

namespace Laporan.Berkas.PDF {
    public interface ICachedColorSpace {
        PdfObject GetPdfObject(PdfWriter writer);
        bool Equals(Object obj);
        int GetHashCode();
    }
}
