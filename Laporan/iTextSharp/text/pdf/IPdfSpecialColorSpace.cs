﻿namespace Laporan.Berkas.PDF {
    public interface IPdfSpecialColorSpace {
        ColorDetails[] GetColorantDetails(PdfWriter writer);
    }
}
