using System;
using System.Collections.Generic;
using System.Text;

namespace Laporan.Berkas.PDF.Interfaces
{
	public interface IPdfStructureElement {
        PdfObject GetAttribute(PdfName name);
        void SetAttribute(PdfName name, PdfObject obj);
	}
}
