using System;

namespace Laporan.Berkas.PDF.Interfaces
{
    /**
     * Interface providing alternate description for accessible elements.
     */
    public interface IAlternateDescription {
        String Alt { get; set; }
    }
}
