﻿Imports System.Windows.Forms
Imports System.Drawing

Public Class Class_Animasi
    <Flags()> _
    Public Enum AnimateWindowFlags
        HORIZONTAL_KANAN = &H1
        HORIZONTAL_KIRI = &H2
        VERTIKAL_BAWAH = &H4
        VERTIKAL_ATAS = &H8
        TENGAH = &H10
        HILANG = &H10000
        AKTIF = &H20000
        GESER = &H40000
        MENYATU = &H80000
    End Enum

    <Flags()> _
    Public Enum Orientasi
        Vertikal = 0
        Horizontal = 1
    End Enum

    <Flags()> _
    Public Enum Pengubah
        Dikurangi = 0
        Ditambah = 1
    End Enum

    <Flags()> _
    Public Enum GayaAnimasi
        Geser_Horizontal_Kiri = 0
        Geser_Horizontal_Kanan = 1
        Geser_Vertikal_Atas = 2
        Geser_Vertikal_Bawah = 3
        Perluas_Horizontal_Kiri = 4
        Perluas_Horizontal_Kanan = 5
        Perluas_Vertikal_Atas = 6
        Perluas_Vertikal_Bawah = 7
        Tengah = 8
        Menyatu = 9
    End Enum

    <Flags()> _
    Public Enum OpsiGaya
        Buka = 0
        Tutup = 1
    End Enum

    Public Shared Sub Jeda(ByVal waktu As Integer)
        Application.DoEvents()
        System.Threading.Thread.Sleep(waktu)
        Application.DoEvents()
    End Sub

    Public Shared Sub GeserKontrol(ByVal control As System.Windows.Forms.Control, ByVal NilaiAkhir As Integer, ByVal Pengubah As Pengubah, ByVal Perubah As Orientasi, Optional ByVal NilaiPerubah As Integer = 1, Optional ByVal Jeda As Integer = 2)
        Dim x, y, delay, batasdelay, NilaiAwal As Integer

        If Perubah = Orientasi.Horizontal Then
            NilaiAwal = control.Location.X
            x = NilaiAwal
            y = control.Location.Y
            If Pengubah = Pengubah.Ditambah Then
                batasdelay = ((80 / 100) * (NilaiAkhir - NilaiAwal)) + NilaiAwal
                Do Until x = NilaiAkhir
                    x = x + NilaiPerubah
                    control.Location = New Point(x, y)
                    Application.DoEvents()
                    If y >= batasdelay Then
                        delay = 10
                    Else
                        delay = Jeda
                    End If
                    System.Threading.Thread.Sleep(delay)
                Loop
            ElseIf Pengubah = Pengubah.Dikurangi Then
                batasdelay = ((20 / 100) * (NilaiAwal - NilaiAkhir)) + NilaiAkhir
                Do Until x = NilaiAkhir
                    x = x - NilaiPerubah
                    control.Location = New Point(x, y)
                    Application.DoEvents()
                    If y <= batasdelay Then
                        delay = 10
                    Else
                        delay = Jeda
                    End If
                    System.Threading.Thread.Sleep(delay)
                Loop
            End If
        ElseIf Perubah = Orientasi.Vertikal Then
            NilaiAwal = control.Location.Y
            y = NilaiAwal
            x = control.Location.X
            If Pengubah = Pengubah.Ditambah Then
                batasdelay = ((80 / 100) * (NilaiAkhir - NilaiAwal)) + NilaiAwal
                Do Until y = NilaiAkhir
                    y = y + NilaiPerubah
                    control.Location = New Point(x, y)
                    Application.DoEvents()
                    If y >= batasdelay Then
                        delay = 10
                    Else
                        delay = Jeda
                    End If
                    System.Threading.Thread.Sleep(delay)
                Loop
            ElseIf Pengubah = Pengubah.Dikurangi Then
                batasdelay = ((20 / 100) * (NilaiAwal - NilaiAkhir)) + NilaiAkhir
                Do Until y = NilaiAkhir
                    y = y - NilaiPerubah
                    control.Location = New Point(x, y)
                    Application.DoEvents()
                    If y <= batasdelay Then
                        delay = 10
                    Else
                        delay = Jeda
                    End If
                    System.Threading.Thread.Sleep(delay)
                Loop
            End If
        End If
    End Sub

    Public Shared Function Animasikan(ByVal kontrol As IntPtr, ByVal t As Integer, ByVal gaya As GayaAnimasi, ByVal opsi As OpsiGaya)
        Dim waktu As String
        Dim anim As AnimateWindowFlags = Nothing
        If gaya = 0 Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KIRI
        ElseIf gaya = 1 Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.HORIZONTAL_KANAN
        ElseIf gaya = 2 Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_ATAS
        ElseIf gaya = 3 Then
            anim = AnimateWindowFlags.GESER Or AnimateWindowFlags.VERTIKAL_BAWAH
        ElseIf gaya = 4 Then
            anim = AnimateWindowFlags.HORIZONTAL_KIRI
        ElseIf gaya = 5 Then
            anim = AnimateWindowFlags.HORIZONTAL_KANAN
        ElseIf gaya = 6 Then
            anim = AnimateWindowFlags.VERTIKAL_ATAS
        ElseIf gaya = 7 Then
            anim = AnimateWindowFlags.VERTIKAL_BAWAH
        ElseIf gaya = 8 Then
            anim = AnimateWindowFlags.TENGAH
        ElseIf gaya = 9 Then
            anim = AnimateWindowFlags.MENYATU
        End If

        If opsi = 1 Then
            anim += AnimateWindowFlags.HILANG
        End If

        waktu = t + "0"
        Return AnimateWindow(kontrol, waktu, anim)
    End Function

    Public Shared Sub AnimasiHuruf(ByVal teks As String, ByVal objek As Label, Optional OpsiGaya As OpsiGaya = OpsiGaya.Buka, Optional ByVal _1 As Integer = 50, Optional ByVal _2 As Integer = 150)
        Dim r As New Random
        If OpsiGaya = Class_Animasi.OpsiGaya.Buka Then
            For Each huruf As Char In teks
                Application.DoEvents()
                objek.Text += huruf
                Application.DoEvents()
                If huruf = "," Then
                    System.Threading.Thread.Sleep(r.Next(1500, 2500))
                End If
                Application.DoEvents()
                System.Threading.Thread.Sleep(r.Next(_1, _2))
            Next
        ElseIf OpsiGaya = Class_Animasi.OpsiGaya.Tutup Then
            For Each huruf As Char In teks
                Application.DoEvents()
                Try
                    objek.Text = objek.Text.Remove(objek.Text.Length - 1, 1)
                Catch ex As Exception

                End Try
                Application.DoEvents()
                If huruf = "," Then
                    System.Threading.Thread.Sleep(r.Next(1500, 2500))
                End If
                Application.DoEvents()
                System.Threading.Thread.Sleep(r.Next(_1, _2))
            Next
        End If
    End Sub
End Class