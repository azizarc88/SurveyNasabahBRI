﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("AnimasiUI")> 
<Assembly: AssemblyDescription("File penanganan animasi")> 
<Assembly: AssemblyCompany("Aziz Nur Ariffianto")> 
<Assembly: AssemblyProduct("AnimasiUI")> 
<Assembly: AssemblyCopyright("Copyright © 2013 - 2015 Aziz Nur Ariffianto")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("66ded5fe-58fc-4ed1-8b0b-eeb6d5c3f7f3")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.2.5.22")> 
<Assembly: AssemblyFileVersion("3.2.5.22")> 
