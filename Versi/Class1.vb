﻿Imports System.IO

Public Class Berkas
    Shared alamat As String = CurDir() + "\" + "Survei Nasabah BRI.exe"

    Public Shared Function AmbilNama() As String
        Return FileVersionInfo.GetVersionInfo(alamat).ProductName
    End Function

    Public Shared Function AmbilVersi() As String
        Return FileVersionInfo.GetVersionInfo(alamat).FileVersion
    End Function

    Public Shared Function AmbilCopyright() As String
        Return FileVersionInfo.GetVersionInfo(alamat).LegalCopyright
    End Function
End Class
