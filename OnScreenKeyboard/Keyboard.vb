﻿Imports AnimasiUI.Class_Animasi
Imports MouseKeyboardLibrary

Public Class Keyboard
    Public Shared AlamatAppData As String = System.Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\Izarc Software\Survei Nasabah BRI"
    Dim focused As Boolean = False
    Dim shifton As Boolean = False

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOPMOST As Integer = &H8
            Const WS_EX_NOACTIVATE As Integer = &H8000000
            Const WS_EX_WINDOWEDGE As Integer = &H100
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_NOACTIVATE Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST Or WS_EX_WINDOWEDGE
            Return Result
        End Get
    End Property

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Try
            FileIO.FileSystem.DeleteFile(AlamatAppData + "\key.ini", FileIO.UIOption.OnlyErrorDialogs, FileIO.RecycleOption.DeletePermanently)
        Catch ex As Exception

        End Try
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Vertikal_Bawah, OpsiGaya.Tutup)
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Location = New Point((My.Computer.Screen.Bounds.Size.Width / 2) - (Me.Size.Width / 2), (My.Computer.Screen.Bounds.Size.Height - Me.Size.Height))
        Animasikan(Me.Handle, 150, GayaAnimasi.Geser_Vertikal_Atas, OpsiGaya.Buka)
        IO.Directory.CreateDirectory(AlamatAppData)
        Using berkas1 As New IO.StreamWriter(AlamatAppData + "\key.ini")
            berkas1.WriteLine(Me.Location.X)
            berkas1.WriteLine(Me.Location.Y)
            berkas1.Close()
        End Using
        Timer1.Enabled = True
        Shift(False)
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        If Not focused Then
            Me.Focus()
            focused = True
        End If
        If Not IO.File.Exists(AlamatAppData + "\key.ini") Then
            Me.Close()
            Form1.Close()
        End If
    End Sub

    Private Sub Shift(aktiv As Boolean)
        shifton = aktiv
        If aktiv Then
            Button1.Text = "!"
            Button2.Text = "@"
            Button3.Text = "#"
            Button4.Text = "$"
            Button5.Text = "%"
            Button6.Text = "^"
            Button7.Text = "&&"
            Button8.Text = "*"
            Button9.Text = "("
            Button10.Text = ")"
            Button11.Text = "_"
            Button12.Text = "+"
            Button14.Text = "Q"
            Button15.Text = "W"
            Button16.Text = "E"
            Button17.Text = "R"
            Button18.Text = "T"
            Button19.Text = "Y"
            Button20.Text = "U"
            Button21.Text = "I"
            Button22.Text = "O"
            Button23.Text = "P"
            Button27.Text = "A"
            Button28.Text = "S"
            Button29.Text = "D"
            Button30.Text = "F"
            Button31.Text = "G"
            Button32.Text = "H"
            Button33.Text = "J"
            Button34.Text = "K"
            Button35.Text = "L"
            Button37.Text = "Z"
            Button38.Text = "X"
            Button39.Text = "C"
            Button40.Text = "V"
            Button41.Text = "B"
            Button42.Text = "N"
            Button43.Text = "M"
            Button44.Text = "?"
        Else
            Button1.Text = "1"
            Button2.Text = "2"
            Button3.Text = "3"
            Button4.Text = "4"
            Button5.Text = "5"
            Button6.Text = "6"
            Button7.Text = "7"
            Button8.Text = "8"
            Button9.Text = "9"
            Button10.Text = "0"
            Button11.Text = "-"
            Button12.Text = "="
            Button14.Text = "q"
            Button15.Text = "w"
            Button16.Text = "e"
            Button17.Text = "r"
            Button18.Text = "t"
            Button19.Text = "y"
            Button20.Text = "u"
            Button21.Text = "i"
            Button22.Text = "o"
            Button23.Text = "p"
            Button27.Text = "a"
            Button28.Text = "s"
            Button29.Text = "d"
            Button30.Text = "f"
            Button31.Text = "g"
            Button32.Text = "h"
            Button33.Text = "j"
            Button34.Text = "k"
            Button35.Text = "l"
            Button37.Text = "z"
            Button38.Text = "x"
            Button39.Text = "c"
            Button40.Text = "v"
            Button41.Text = "b"
            Button42.Text = "n"
            Button43.Text = "m"
            Button44.Text = "/"
            KeyboardSimulator.KeyUp(Keys.LShiftKey)
        End If
    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click, Button9.Click, Button8.Click, Button7.Click, Button6.Click, Button51.Click, Button5.Click, Button49.Click, Button48.Click, Button47.Click, Button46.Click, Button45.Click, Button44.Click, Button43.Click, Button42.Click, Button41.Click, Button40.Click, Button4.Click, Button39.Click, Button38.Click, Button37.Click, Button36.Click, Button35.Click, Button34.Click, Button33.Click, Button32.Click, Button31.Click, Button30.Click, Button3.Click, Button29.Click, Button28.Click, Button27.Click, Button26.Click, Button25.Click, Button24.Click, Button23.Click, Button22.Click, Button21.Click, Button20.Click, Button2.Click, Button19.Click, Button18.Click, Button17.Click, Button16.Click, Button15.Click, Button14.Click, Button13.Click, Button12.Click, Button10.Click, Button1.Click, BSpasi.Click
        Dim b As Button = sender
        If shifton Then
            KeyboardSimulator.KeyDown(Keys.LShiftKey)
        End If

        If b.Text = "=" Or b.Text = "+" Then
            KeyboardSimulator.KeyDown(Keys.Oemplus)
            KeyboardSimulator.KeyUp(Keys.Oemplus)
        ElseIf b.Text = "Tab" Then
            KeyboardSimulator.KeyDown(Keys.Tab)
            KeyboardSimulator.KeyUp(Keys.Tab)
        ElseIf b.Text = "Shift" Then
            If b.BackColor = Color.WhiteSmoke Then
                Shift(True)
                b.BackColor = Color.DarkGray
            Else
                Shift(False)
                b.BackColor = Color.WhiteSmoke
            End If
        ElseIf b.Text = "Home" Then
            KeyboardSimulator.KeyDown(Keys.Home)
            KeyboardSimulator.KeyUp(Keys.Home)
        ElseIf b.Text = "" Then
            KeyboardSimulator.KeyDown(Keys.Space)
            KeyboardSimulator.KeyUp(Keys.Space)
        ElseIf b.Text = "End" Then
            KeyboardSimulator.KeyDown(Keys.End)
            KeyboardSimulator.KeyUp(Keys.End)
        ElseIf b.Text = "Enter" Then
            KeyboardSimulator.KeyDown(Keys.Enter)
            KeyboardSimulator.KeyUp(Keys.Enter)
        ElseIf b.Text = "<=" Then
            KeyboardSimulator.KeyDown(Keys.Back)
            KeyboardSimulator.KeyUp(Keys.Back)
        ElseIf b.Text = "/" Or b.Text = "?" Then
            KeyboardSimulator.KeyDown(Keys.OemQuestion)
            KeyboardSimulator.KeyUp(Keys.OemQuestion)
        ElseIf b.Text = "_" Or b.Text = "-" Then
            KeyboardSimulator.KeyDown(Keys.OemMinus)
            KeyboardSimulator.KeyUp(Keys.OemMinus)
        ElseIf b.Text = "!" Then
            KeyboardSimulator.KeyDown(Asc("1"))
            KeyboardSimulator.KeyUp(Asc("1"))
        ElseIf b.Text = "@" Then
            KeyboardSimulator.KeyDown(Asc("2"))
            KeyboardSimulator.KeyUp(Asc("2"))
        ElseIf b.Text = "#" Then
            KeyboardSimulator.KeyDown(Asc("3"))
            KeyboardSimulator.KeyUp(Asc("3"))
        ElseIf b.Text = "$" Then
            KeyboardSimulator.KeyDown(Asc("4"))
            KeyboardSimulator.KeyUp(Asc("4"))
        ElseIf b.Text = "%" Then
            KeyboardSimulator.KeyDown(Asc("5"))
            KeyboardSimulator.KeyUp(Asc("5"))
        ElseIf b.Text = "^" And b.Name = "Button6" Then
            KeyboardSimulator.KeyDown(Asc("6"))
            KeyboardSimulator.KeyUp(Asc("6"))
        ElseIf b.Text = "&&" Then
            KeyboardSimulator.KeyDown(Asc("7"))
            KeyboardSimulator.KeyUp(Asc("7"))
        ElseIf b.Text = "*" Then
            KeyboardSimulator.KeyDown(Asc("8"))
            KeyboardSimulator.KeyUp(Asc("8"))
        ElseIf b.Text = "(" Then
            KeyboardSimulator.KeyDown(Asc("9"))
            KeyboardSimulator.KeyUp(Asc("9"))
        ElseIf b.Text = ")" Then
            KeyboardSimulator.KeyDown(Asc("0"))
            KeyboardSimulator.KeyUp(Asc("0"))
        ElseIf b.Text = "<" Then
            KeyboardSimulator.KeyDown(Keys.Left)
            KeyboardSimulator.KeyUp(Keys.Left)
        ElseIf b.Text = ">" Then
            KeyboardSimulator.KeyDown(Keys.Right)
            KeyboardSimulator.KeyUp(Keys.Right)
        ElseIf b.Text = "v" And b.Name = "Button47" Then
            KeyboardSimulator.KeyDown(Keys.Down)
            KeyboardSimulator.KeyUp(Keys.Down)
        ElseIf b.Text = "^" And b.Name = "Button45" Then
            KeyboardSimulator.KeyDown(Keys.Up)
            KeyboardSimulator.KeyUp(Keys.Up)
        ElseIf b.Text = "Tutup" Then
            Me.Close()
            Form1.Close()
        Else
            KeyboardSimulator.KeyDown(Asc(b.Text.ToUpper))
            KeyboardSimulator.KeyUp(Asc(b.Text.ToUpper))
        End If

        If shifton Then
            KeyboardSimulator.KeyUp(Keys.LShiftKey)
        End If
    End Sub
End Class
