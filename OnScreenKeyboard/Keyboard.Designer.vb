﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Keyboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Button10 = New System.Windows.Forms.Button()
        Me.Button12 = New System.Windows.Forms.Button()
        Me.Button13 = New System.Windows.Forms.Button()
        Me.Button14 = New System.Windows.Forms.Button()
        Me.Button15 = New System.Windows.Forms.Button()
        Me.Button16 = New System.Windows.Forms.Button()
        Me.Button17 = New System.Windows.Forms.Button()
        Me.Button18 = New System.Windows.Forms.Button()
        Me.Button19 = New System.Windows.Forms.Button()
        Me.Button20 = New System.Windows.Forms.Button()
        Me.Button21 = New System.Windows.Forms.Button()
        Me.Button22 = New System.Windows.Forms.Button()
        Me.Button23 = New System.Windows.Forms.Button()
        Me.Button24 = New System.Windows.Forms.Button()
        Me.Button25 = New System.Windows.Forms.Button()
        Me.Button26 = New System.Windows.Forms.Button()
        Me.Button27 = New System.Windows.Forms.Button()
        Me.Button28 = New System.Windows.Forms.Button()
        Me.Button29 = New System.Windows.Forms.Button()
        Me.Button30 = New System.Windows.Forms.Button()
        Me.Button31 = New System.Windows.Forms.Button()
        Me.Button32 = New System.Windows.Forms.Button()
        Me.Button33 = New System.Windows.Forms.Button()
        Me.Button34 = New System.Windows.Forms.Button()
        Me.Button35 = New System.Windows.Forms.Button()
        Me.Button36 = New System.Windows.Forms.Button()
        Me.Button37 = New System.Windows.Forms.Button()
        Me.Button38 = New System.Windows.Forms.Button()
        Me.Button39 = New System.Windows.Forms.Button()
        Me.Button40 = New System.Windows.Forms.Button()
        Me.Button41 = New System.Windows.Forms.Button()
        Me.Button42 = New System.Windows.Forms.Button()
        Me.Button43 = New System.Windows.Forms.Button()
        Me.Button44 = New System.Windows.Forms.Button()
        Me.Button46 = New System.Windows.Forms.Button()
        Me.Button45 = New System.Windows.Forms.Button()
        Me.Button47 = New System.Windows.Forms.Button()
        Me.Button48 = New System.Windows.Forms.Button()
        Me.Button49 = New System.Windows.Forms.Button()
        Me.BSpasi = New System.Windows.Forms.Button()
        Me.Button51 = New System.Windows.Forms.Button()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.KeluarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Button11 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ContextMenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.White
        Me.Button1.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Location = New System.Drawing.Point(100, 16)
        Me.Button1.Margin = New System.Windows.Forms.Padding(7)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(70, 41)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "1"
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.Color.White
        Me.Button2.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Location = New System.Drawing.Point(184, 16)
        Me.Button2.Margin = New System.Windows.Forms.Padding(7)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(70, 41)
        Me.Button2.TabIndex = 0
        Me.Button2.Text = "2"
        Me.Button2.UseVisualStyleBackColor = False
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.White
        Me.Button3.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Location = New System.Drawing.Point(268, 16)
        Me.Button3.Margin = New System.Windows.Forms.Padding(7)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(70, 41)
        Me.Button3.TabIndex = 0
        Me.Button3.Text = "3"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Button4
        '
        Me.Button4.BackColor = System.Drawing.Color.White
        Me.Button4.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button4.Location = New System.Drawing.Point(352, 16)
        Me.Button4.Margin = New System.Windows.Forms.Padding(7)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(70, 41)
        Me.Button4.TabIndex = 0
        Me.Button4.Text = "4"
        Me.Button4.UseVisualStyleBackColor = False
        '
        'Button5
        '
        Me.Button5.BackColor = System.Drawing.Color.White
        Me.Button5.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button5.Location = New System.Drawing.Point(436, 16)
        Me.Button5.Margin = New System.Windows.Forms.Padding(7)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(70, 41)
        Me.Button5.TabIndex = 0
        Me.Button5.Text = "5"
        Me.Button5.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.Color.White
        Me.Button6.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button6.Location = New System.Drawing.Point(520, 16)
        Me.Button6.Margin = New System.Windows.Forms.Padding(7)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(70, 41)
        Me.Button6.TabIndex = 0
        Me.Button6.Text = "6"
        Me.Button6.UseVisualStyleBackColor = False
        '
        'Button7
        '
        Me.Button7.BackColor = System.Drawing.Color.White
        Me.Button7.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button7.Location = New System.Drawing.Point(604, 16)
        Me.Button7.Margin = New System.Windows.Forms.Padding(7)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(70, 41)
        Me.Button7.TabIndex = 0
        Me.Button7.Text = "7"
        Me.Button7.UseVisualStyleBackColor = False
        '
        'Button8
        '
        Me.Button8.BackColor = System.Drawing.Color.White
        Me.Button8.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button8.Location = New System.Drawing.Point(688, 16)
        Me.Button8.Margin = New System.Windows.Forms.Padding(7)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(70, 41)
        Me.Button8.TabIndex = 0
        Me.Button8.Text = "8"
        Me.Button8.UseVisualStyleBackColor = False
        '
        'Button9
        '
        Me.Button9.BackColor = System.Drawing.Color.White
        Me.Button9.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button9.Location = New System.Drawing.Point(772, 16)
        Me.Button9.Margin = New System.Windows.Forms.Padding(7)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(70, 41)
        Me.Button9.TabIndex = 0
        Me.Button9.Text = "9"
        Me.Button9.UseVisualStyleBackColor = False
        '
        'Button10
        '
        Me.Button10.BackColor = System.Drawing.Color.White
        Me.Button10.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button10.Location = New System.Drawing.Point(856, 16)
        Me.Button10.Margin = New System.Windows.Forms.Padding(7)
        Me.Button10.Name = "Button10"
        Me.Button10.Size = New System.Drawing.Size(70, 41)
        Me.Button10.TabIndex = 0
        Me.Button10.Text = "0"
        Me.Button10.UseVisualStyleBackColor = False
        '
        'Button12
        '
        Me.Button12.BackColor = System.Drawing.Color.White
        Me.Button12.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button12.Location = New System.Drawing.Point(940, 16)
        Me.Button12.Margin = New System.Windows.Forms.Padding(7)
        Me.Button12.Name = "Button12"
        Me.Button12.Size = New System.Drawing.Size(70, 41)
        Me.Button12.TabIndex = 0
        Me.Button12.Text = "="
        Me.Button12.UseVisualStyleBackColor = False
        '
        'Button13
        '
        Me.Button13.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.Button13.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button13.Location = New System.Drawing.Point(16, 71)
        Me.Button13.Margin = New System.Windows.Forms.Padding(7)
        Me.Button13.Name = "Button13"
        Me.Button13.Size = New System.Drawing.Size(70, 41)
        Me.Button13.TabIndex = 0
        Me.Button13.Text = "Tab"
        Me.Button13.UseVisualStyleBackColor = False
        '
        'Button14
        '
        Me.Button14.BackColor = System.Drawing.Color.White
        Me.Button14.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button14.Location = New System.Drawing.Point(100, 71)
        Me.Button14.Margin = New System.Windows.Forms.Padding(7)
        Me.Button14.Name = "Button14"
        Me.Button14.Size = New System.Drawing.Size(70, 41)
        Me.Button14.TabIndex = 0
        Me.Button14.Text = "Q"
        Me.Button14.UseVisualStyleBackColor = False
        '
        'Button15
        '
        Me.Button15.BackColor = System.Drawing.Color.White
        Me.Button15.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button15.Location = New System.Drawing.Point(184, 71)
        Me.Button15.Margin = New System.Windows.Forms.Padding(7)
        Me.Button15.Name = "Button15"
        Me.Button15.Size = New System.Drawing.Size(70, 41)
        Me.Button15.TabIndex = 0
        Me.Button15.Text = "W"
        Me.Button15.UseVisualStyleBackColor = False
        '
        'Button16
        '
        Me.Button16.BackColor = System.Drawing.Color.White
        Me.Button16.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button16.Location = New System.Drawing.Point(268, 71)
        Me.Button16.Margin = New System.Windows.Forms.Padding(7)
        Me.Button16.Name = "Button16"
        Me.Button16.Size = New System.Drawing.Size(70, 41)
        Me.Button16.TabIndex = 0
        Me.Button16.Text = "E"
        Me.Button16.UseVisualStyleBackColor = False
        '
        'Button17
        '
        Me.Button17.BackColor = System.Drawing.Color.White
        Me.Button17.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button17.Location = New System.Drawing.Point(352, 71)
        Me.Button17.Margin = New System.Windows.Forms.Padding(7)
        Me.Button17.Name = "Button17"
        Me.Button17.Size = New System.Drawing.Size(70, 41)
        Me.Button17.TabIndex = 0
        Me.Button17.Text = "R"
        Me.Button17.UseVisualStyleBackColor = False
        '
        'Button18
        '
        Me.Button18.BackColor = System.Drawing.Color.White
        Me.Button18.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button18.Location = New System.Drawing.Point(436, 71)
        Me.Button18.Margin = New System.Windows.Forms.Padding(7)
        Me.Button18.Name = "Button18"
        Me.Button18.Size = New System.Drawing.Size(70, 41)
        Me.Button18.TabIndex = 0
        Me.Button18.Text = "T"
        Me.Button18.UseVisualStyleBackColor = False
        '
        'Button19
        '
        Me.Button19.BackColor = System.Drawing.Color.White
        Me.Button19.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button19.Location = New System.Drawing.Point(520, 71)
        Me.Button19.Margin = New System.Windows.Forms.Padding(7)
        Me.Button19.Name = "Button19"
        Me.Button19.Size = New System.Drawing.Size(70, 41)
        Me.Button19.TabIndex = 0
        Me.Button19.Text = "Y"
        Me.Button19.UseVisualStyleBackColor = False
        '
        'Button20
        '
        Me.Button20.BackColor = System.Drawing.Color.White
        Me.Button20.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button20.Location = New System.Drawing.Point(604, 71)
        Me.Button20.Margin = New System.Windows.Forms.Padding(7)
        Me.Button20.Name = "Button20"
        Me.Button20.Size = New System.Drawing.Size(70, 41)
        Me.Button20.TabIndex = 0
        Me.Button20.Text = "U"
        Me.Button20.UseVisualStyleBackColor = False
        '
        'Button21
        '
        Me.Button21.BackColor = System.Drawing.Color.White
        Me.Button21.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button21.Location = New System.Drawing.Point(688, 71)
        Me.Button21.Margin = New System.Windows.Forms.Padding(7)
        Me.Button21.Name = "Button21"
        Me.Button21.Size = New System.Drawing.Size(70, 41)
        Me.Button21.TabIndex = 0
        Me.Button21.Text = "I"
        Me.Button21.UseVisualStyleBackColor = False
        '
        'Button22
        '
        Me.Button22.BackColor = System.Drawing.Color.White
        Me.Button22.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button22.Location = New System.Drawing.Point(772, 71)
        Me.Button22.Margin = New System.Windows.Forms.Padding(7)
        Me.Button22.Name = "Button22"
        Me.Button22.Size = New System.Drawing.Size(70, 41)
        Me.Button22.TabIndex = 0
        Me.Button22.Text = "O"
        Me.Button22.UseVisualStyleBackColor = False
        '
        'Button23
        '
        Me.Button23.BackColor = System.Drawing.Color.White
        Me.Button23.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button23.Location = New System.Drawing.Point(856, 71)
        Me.Button23.Margin = New System.Windows.Forms.Padding(7)
        Me.Button23.Name = "Button23"
        Me.Button23.Size = New System.Drawing.Size(70, 41)
        Me.Button23.TabIndex = 0
        Me.Button23.Text = "P"
        Me.Button23.UseVisualStyleBackColor = False
        '
        'Button24
        '
        Me.Button24.BackColor = System.Drawing.Color.FromArgb(CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer), CType(CType(235, Byte), Integer))
        Me.Button24.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button24.Location = New System.Drawing.Point(940, 71)
        Me.Button24.Margin = New System.Windows.Forms.Padding(7)
        Me.Button24.Name = "Button24"
        Me.Button24.Size = New System.Drawing.Size(70, 41)
        Me.Button24.TabIndex = 0
        Me.Button24.Text = "<="
        Me.Button24.UseVisualStyleBackColor = False
        '
        'Button25
        '
        Me.Button25.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button25.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button25.Location = New System.Drawing.Point(16, 126)
        Me.Button25.Margin = New System.Windows.Forms.Padding(7)
        Me.Button25.Name = "Button25"
        Me.Button25.Size = New System.Drawing.Size(112, 41)
        Me.Button25.TabIndex = 0
        Me.Button25.Text = "Shift"
        Me.Button25.UseVisualStyleBackColor = False
        '
        'Button26
        '
        Me.Button26.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button26.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button26.Location = New System.Drawing.Point(898, 126)
        Me.Button26.Margin = New System.Windows.Forms.Padding(7)
        Me.Button26.Name = "Button26"
        Me.Button26.Size = New System.Drawing.Size(112, 41)
        Me.Button26.TabIndex = 0
        Me.Button26.Text = "Enter"
        Me.Button26.UseVisualStyleBackColor = False
        '
        'Button27
        '
        Me.Button27.BackColor = System.Drawing.Color.White
        Me.Button27.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button27.Location = New System.Drawing.Point(142, 126)
        Me.Button27.Margin = New System.Windows.Forms.Padding(7)
        Me.Button27.Name = "Button27"
        Me.Button27.Size = New System.Drawing.Size(70, 41)
        Me.Button27.TabIndex = 0
        Me.Button27.Text = "A"
        Me.Button27.UseVisualStyleBackColor = False
        '
        'Button28
        '
        Me.Button28.BackColor = System.Drawing.Color.White
        Me.Button28.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button28.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button28.Location = New System.Drawing.Point(226, 126)
        Me.Button28.Margin = New System.Windows.Forms.Padding(7)
        Me.Button28.Name = "Button28"
        Me.Button28.Size = New System.Drawing.Size(70, 41)
        Me.Button28.TabIndex = 0
        Me.Button28.Text = "S"
        Me.Button28.UseVisualStyleBackColor = False
        '
        'Button29
        '
        Me.Button29.BackColor = System.Drawing.Color.White
        Me.Button29.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button29.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button29.Location = New System.Drawing.Point(310, 126)
        Me.Button29.Margin = New System.Windows.Forms.Padding(7)
        Me.Button29.Name = "Button29"
        Me.Button29.Size = New System.Drawing.Size(70, 41)
        Me.Button29.TabIndex = 0
        Me.Button29.Text = "D"
        Me.Button29.UseVisualStyleBackColor = False
        '
        'Button30
        '
        Me.Button30.BackColor = System.Drawing.Color.White
        Me.Button30.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button30.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button30.Location = New System.Drawing.Point(394, 126)
        Me.Button30.Margin = New System.Windows.Forms.Padding(7)
        Me.Button30.Name = "Button30"
        Me.Button30.Size = New System.Drawing.Size(70, 41)
        Me.Button30.TabIndex = 0
        Me.Button30.Text = "F"
        Me.Button30.UseVisualStyleBackColor = False
        '
        'Button31
        '
        Me.Button31.BackColor = System.Drawing.Color.White
        Me.Button31.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button31.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button31.Location = New System.Drawing.Point(478, 126)
        Me.Button31.Margin = New System.Windows.Forms.Padding(7)
        Me.Button31.Name = "Button31"
        Me.Button31.Size = New System.Drawing.Size(70, 41)
        Me.Button31.TabIndex = 0
        Me.Button31.Text = "G"
        Me.Button31.UseVisualStyleBackColor = False
        '
        'Button32
        '
        Me.Button32.BackColor = System.Drawing.Color.White
        Me.Button32.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button32.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button32.Location = New System.Drawing.Point(562, 126)
        Me.Button32.Margin = New System.Windows.Forms.Padding(7)
        Me.Button32.Name = "Button32"
        Me.Button32.Size = New System.Drawing.Size(70, 41)
        Me.Button32.TabIndex = 0
        Me.Button32.Text = "H"
        Me.Button32.UseVisualStyleBackColor = False
        '
        'Button33
        '
        Me.Button33.BackColor = System.Drawing.Color.White
        Me.Button33.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button33.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button33.Location = New System.Drawing.Point(646, 126)
        Me.Button33.Margin = New System.Windows.Forms.Padding(7)
        Me.Button33.Name = "Button33"
        Me.Button33.Size = New System.Drawing.Size(70, 41)
        Me.Button33.TabIndex = 0
        Me.Button33.Text = "J"
        Me.Button33.UseVisualStyleBackColor = False
        '
        'Button34
        '
        Me.Button34.BackColor = System.Drawing.Color.White
        Me.Button34.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button34.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button34.Location = New System.Drawing.Point(730, 126)
        Me.Button34.Margin = New System.Windows.Forms.Padding(7)
        Me.Button34.Name = "Button34"
        Me.Button34.Size = New System.Drawing.Size(70, 41)
        Me.Button34.TabIndex = 0
        Me.Button34.Text = "K"
        Me.Button34.UseVisualStyleBackColor = False
        '
        'Button35
        '
        Me.Button35.BackColor = System.Drawing.Color.White
        Me.Button35.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button35.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button35.Location = New System.Drawing.Point(814, 126)
        Me.Button35.Margin = New System.Windows.Forms.Padding(7)
        Me.Button35.Name = "Button35"
        Me.Button35.Size = New System.Drawing.Size(70, 41)
        Me.Button35.TabIndex = 0
        Me.Button35.Text = "L"
        Me.Button35.UseVisualStyleBackColor = False
        '
        'Button36
        '
        Me.Button36.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button36.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button36.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button36.Location = New System.Drawing.Point(16, 181)
        Me.Button36.Margin = New System.Windows.Forms.Padding(7)
        Me.Button36.Name = "Button36"
        Me.Button36.Size = New System.Drawing.Size(112, 41)
        Me.Button36.TabIndex = 0
        Me.Button36.Text = "Home"
        Me.Button36.UseVisualStyleBackColor = False
        '
        'Button37
        '
        Me.Button37.BackColor = System.Drawing.Color.White
        Me.Button37.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button37.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button37.Location = New System.Drawing.Point(226, 181)
        Me.Button37.Margin = New System.Windows.Forms.Padding(7)
        Me.Button37.Name = "Button37"
        Me.Button37.Size = New System.Drawing.Size(70, 41)
        Me.Button37.TabIndex = 0
        Me.Button37.Text = "Z"
        Me.Button37.UseVisualStyleBackColor = False
        '
        'Button38
        '
        Me.Button38.BackColor = System.Drawing.Color.White
        Me.Button38.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button38.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button38.Location = New System.Drawing.Point(310, 181)
        Me.Button38.Margin = New System.Windows.Forms.Padding(7)
        Me.Button38.Name = "Button38"
        Me.Button38.Size = New System.Drawing.Size(70, 41)
        Me.Button38.TabIndex = 0
        Me.Button38.Text = "X"
        Me.Button38.UseVisualStyleBackColor = False
        '
        'Button39
        '
        Me.Button39.BackColor = System.Drawing.Color.White
        Me.Button39.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button39.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button39.Location = New System.Drawing.Point(394, 181)
        Me.Button39.Margin = New System.Windows.Forms.Padding(7)
        Me.Button39.Name = "Button39"
        Me.Button39.Size = New System.Drawing.Size(70, 41)
        Me.Button39.TabIndex = 0
        Me.Button39.Text = "C"
        Me.Button39.UseVisualStyleBackColor = False
        '
        'Button40
        '
        Me.Button40.BackColor = System.Drawing.Color.White
        Me.Button40.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button40.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button40.Location = New System.Drawing.Point(478, 181)
        Me.Button40.Margin = New System.Windows.Forms.Padding(7)
        Me.Button40.Name = "Button40"
        Me.Button40.Size = New System.Drawing.Size(70, 41)
        Me.Button40.TabIndex = 0
        Me.Button40.Text = "V"
        Me.Button40.UseVisualStyleBackColor = False
        '
        'Button41
        '
        Me.Button41.BackColor = System.Drawing.Color.White
        Me.Button41.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button41.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button41.Location = New System.Drawing.Point(562, 181)
        Me.Button41.Margin = New System.Windows.Forms.Padding(7)
        Me.Button41.Name = "Button41"
        Me.Button41.Size = New System.Drawing.Size(70, 41)
        Me.Button41.TabIndex = 0
        Me.Button41.Text = "B"
        Me.Button41.UseVisualStyleBackColor = False
        '
        'Button42
        '
        Me.Button42.BackColor = System.Drawing.Color.White
        Me.Button42.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button42.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button42.Location = New System.Drawing.Point(646, 181)
        Me.Button42.Margin = New System.Windows.Forms.Padding(7)
        Me.Button42.Name = "Button42"
        Me.Button42.Size = New System.Drawing.Size(70, 41)
        Me.Button42.TabIndex = 0
        Me.Button42.Text = "N"
        Me.Button42.UseVisualStyleBackColor = False
        '
        'Button43
        '
        Me.Button43.BackColor = System.Drawing.Color.White
        Me.Button43.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button43.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button43.Location = New System.Drawing.Point(730, 181)
        Me.Button43.Margin = New System.Windows.Forms.Padding(7)
        Me.Button43.Name = "Button43"
        Me.Button43.Size = New System.Drawing.Size(70, 41)
        Me.Button43.TabIndex = 0
        Me.Button43.Text = "M"
        Me.Button43.UseVisualStyleBackColor = False
        '
        'Button44
        '
        Me.Button44.BackColor = System.Drawing.Color.White
        Me.Button44.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button44.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button44.Location = New System.Drawing.Point(142, 181)
        Me.Button44.Margin = New System.Windows.Forms.Padding(7)
        Me.Button44.Name = "Button44"
        Me.Button44.Size = New System.Drawing.Size(70, 41)
        Me.Button44.TabIndex = 0
        Me.Button44.Text = "/"
        Me.Button44.UseVisualStyleBackColor = False
        '
        'Button46
        '
        Me.Button46.BackColor = System.Drawing.Color.WhiteSmoke
        Me.Button46.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button46.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button46.Location = New System.Drawing.Point(898, 181)
        Me.Button46.Margin = New System.Windows.Forms.Padding(7)
        Me.Button46.Name = "Button46"
        Me.Button46.Size = New System.Drawing.Size(112, 41)
        Me.Button46.TabIndex = 0
        Me.Button46.Text = "End"
        Me.Button46.UseVisualStyleBackColor = False
        '
        'Button45
        '
        Me.Button45.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Button45.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button45.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button45.Location = New System.Drawing.Point(814, 181)
        Me.Button45.Margin = New System.Windows.Forms.Padding(7)
        Me.Button45.Name = "Button45"
        Me.Button45.Size = New System.Drawing.Size(70, 41)
        Me.Button45.TabIndex = 0
        Me.Button45.Text = "^"
        Me.Button45.UseVisualStyleBackColor = False
        '
        'Button47
        '
        Me.Button47.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Button47.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button47.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button47.Location = New System.Drawing.Point(814, 236)
        Me.Button47.Margin = New System.Windows.Forms.Padding(7)
        Me.Button47.Name = "Button47"
        Me.Button47.Size = New System.Drawing.Size(70, 41)
        Me.Button47.TabIndex = 0
        Me.Button47.Text = "v"
        Me.Button47.UseVisualStyleBackColor = False
        '
        'Button48
        '
        Me.Button48.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Button48.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button48.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button48.Location = New System.Drawing.Point(898, 236)
        Me.Button48.Margin = New System.Windows.Forms.Padding(7)
        Me.Button48.Name = "Button48"
        Me.Button48.Size = New System.Drawing.Size(70, 41)
        Me.Button48.TabIndex = 0
        Me.Button48.Text = ">"
        Me.Button48.UseVisualStyleBackColor = False
        '
        'Button49
        '
        Me.Button49.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Button49.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button49.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button49.Location = New System.Drawing.Point(730, 236)
        Me.Button49.Margin = New System.Windows.Forms.Padding(7)
        Me.Button49.Name = "Button49"
        Me.Button49.Size = New System.Drawing.Size(70, 41)
        Me.Button49.TabIndex = 0
        Me.Button49.Text = "<"
        Me.Button49.UseVisualStyleBackColor = False
        '
        'BSpasi
        '
        Me.BSpasi.BackColor = System.Drawing.Color.White
        Me.BSpasi.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.BSpasi.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BSpasi.Location = New System.Drawing.Point(142, 236)
        Me.BSpasi.Margin = New System.Windows.Forms.Padding(7)
        Me.BSpasi.Name = "BSpasi"
        Me.BSpasi.Size = New System.Drawing.Size(574, 41)
        Me.BSpasi.TabIndex = 0
        Me.BSpasi.UseVisualStyleBackColor = False
        '
        'Button51
        '
        Me.Button51.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.Button51.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Button51.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button51.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button51.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.Button51.Location = New System.Drawing.Point(58, 236)
        Me.Button51.Margin = New System.Windows.Forms.Padding(7)
        Me.Button51.Name = "Button51"
        Me.Button51.Size = New System.Drawing.Size(70, 41)
        Me.Button51.TabIndex = 0
        Me.Button51.Text = "Tutup"
        Me.Button51.UseVisualStyleBackColor = False
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.AutoSize = False
        Me.ContextMenuStrip1.Font = New System.Drawing.Font("Century Gothic", 10.0!)
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.KeluarToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(153, 28)
        '
        'KeluarToolStripMenuItem
        '
        Me.KeluarToolStripMenuItem.AutoSize = False
        Me.KeluarToolStripMenuItem.Name = "KeluarToolStripMenuItem"
        Me.KeluarToolStripMenuItem.Size = New System.Drawing.Size(152, 24)
        Me.KeluarToolStripMenuItem.Text = "Keluar"
        '
        'Button11
        '
        Me.Button11.BackColor = System.Drawing.Color.White
        Me.Button11.FlatAppearance.BorderColor = System.Drawing.Color.Gainsboro
        Me.Button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button11.Location = New System.Drawing.Point(16, 16)
        Me.Button11.Margin = New System.Windows.Forms.Padding(7)
        Me.Button11.Name = "Button11"
        Me.Button11.Size = New System.Drawing.Size(70, 41)
        Me.Button11.TabIndex = 0
        Me.Button11.Text = "-"
        Me.Button11.UseVisualStyleBackColor = False
        '
        'Timer1
        '
        Me.Timer1.Interval = 250
        '
        'Keyboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(14.0!, 30.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(1026, 294)
        Me.ControlBox = False
        Me.Controls.Add(Me.Button24)
        Me.Controls.Add(Me.Button12)
        Me.Controls.Add(Me.Button10)
        Me.Controls.Add(Me.Button9)
        Me.Controls.Add(Me.Button8)
        Me.Controls.Add(Me.Button7)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Button5)
        Me.Controls.Add(Me.Button4)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button26)
        Me.Controls.Add(Me.Button46)
        Me.Controls.Add(Me.Button36)
        Me.Controls.Add(Me.Button25)
        Me.Controls.Add(Me.Button13)
        Me.Controls.Add(Me.Button23)
        Me.Controls.Add(Me.Button22)
        Me.Controls.Add(Me.Button21)
        Me.Controls.Add(Me.Button20)
        Me.Controls.Add(Me.Button19)
        Me.Controls.Add(Me.Button18)
        Me.Controls.Add(Me.Button17)
        Me.Controls.Add(Me.Button16)
        Me.Controls.Add(Me.Button15)
        Me.Controls.Add(Me.Button35)
        Me.Controls.Add(Me.Button34)
        Me.Controls.Add(Me.Button33)
        Me.Controls.Add(Me.Button32)
        Me.Controls.Add(Me.Button31)
        Me.Controls.Add(Me.Button30)
        Me.Controls.Add(Me.Button29)
        Me.Controls.Add(Me.Button28)
        Me.Controls.Add(Me.Button49)
        Me.Controls.Add(Me.Button48)
        Me.Controls.Add(Me.Button47)
        Me.Controls.Add(Me.Button45)
        Me.Controls.Add(Me.Button43)
        Me.Controls.Add(Me.Button42)
        Me.Controls.Add(Me.Button41)
        Me.Controls.Add(Me.Button40)
        Me.Controls.Add(Me.Button39)
        Me.Controls.Add(Me.Button38)
        Me.Controls.Add(Me.BSpasi)
        Me.Controls.Add(Me.Button51)
        Me.Controls.Add(Me.Button44)
        Me.Controls.Add(Me.Button37)
        Me.Controls.Add(Me.Button27)
        Me.Controls.Add(Me.Button14)
        Me.Controls.Add(Me.Button11)
        Me.Controls.Add(Me.Button1)
        Me.Font = New System.Drawing.Font("Century Gothic", 18.0!)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(7)
        Me.Name = "Keyboard"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Keyboard"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Button10 As System.Windows.Forms.Button
    Friend WithEvents Button12 As System.Windows.Forms.Button
    Friend WithEvents Button13 As System.Windows.Forms.Button
    Friend WithEvents Button14 As System.Windows.Forms.Button
    Friend WithEvents Button15 As System.Windows.Forms.Button
    Friend WithEvents Button16 As System.Windows.Forms.Button
    Friend WithEvents Button17 As System.Windows.Forms.Button
    Friend WithEvents Button18 As System.Windows.Forms.Button
    Friend WithEvents Button19 As System.Windows.Forms.Button
    Friend WithEvents Button20 As System.Windows.Forms.Button
    Friend WithEvents Button21 As System.Windows.Forms.Button
    Friend WithEvents Button22 As System.Windows.Forms.Button
    Friend WithEvents Button23 As System.Windows.Forms.Button
    Friend WithEvents Button24 As System.Windows.Forms.Button
    Friend WithEvents Button25 As System.Windows.Forms.Button
    Friend WithEvents Button26 As System.Windows.Forms.Button
    Friend WithEvents Button27 As System.Windows.Forms.Button
    Friend WithEvents Button28 As System.Windows.Forms.Button
    Friend WithEvents Button29 As System.Windows.Forms.Button
    Friend WithEvents Button30 As System.Windows.Forms.Button
    Friend WithEvents Button31 As System.Windows.Forms.Button
    Friend WithEvents Button32 As System.Windows.Forms.Button
    Friend WithEvents Button33 As System.Windows.Forms.Button
    Friend WithEvents Button34 As System.Windows.Forms.Button
    Friend WithEvents Button35 As System.Windows.Forms.Button
    Friend WithEvents Button36 As System.Windows.Forms.Button
    Friend WithEvents Button37 As System.Windows.Forms.Button
    Friend WithEvents Button38 As System.Windows.Forms.Button
    Friend WithEvents Button39 As System.Windows.Forms.Button
    Friend WithEvents Button40 As System.Windows.Forms.Button
    Friend WithEvents Button41 As System.Windows.Forms.Button
    Friend WithEvents Button42 As System.Windows.Forms.Button
    Friend WithEvents Button43 As System.Windows.Forms.Button
    Friend WithEvents Button44 As System.Windows.Forms.Button
    Friend WithEvents Button46 As System.Windows.Forms.Button
    Friend WithEvents Button45 As System.Windows.Forms.Button
    Friend WithEvents Button47 As System.Windows.Forms.Button
    Friend WithEvents Button48 As System.Windows.Forms.Button
    Friend WithEvents Button49 As System.Windows.Forms.Button
    Friend WithEvents BSpasi As System.Windows.Forms.Button
    Friend WithEvents Button51 As System.Windows.Forms.Button
    Friend WithEvents Button11 As System.Windows.Forms.Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents KeluarToolStripMenuItem As ToolStripMenuItem
End Class
