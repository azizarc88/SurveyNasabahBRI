﻿Public Class Form1
    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOPMOST As Integer = &H8
            Const WS_EX_NOACTIVATE As Integer = &H8000000
            Const WS_EX_WINDOWEDGE As Integer = &H100
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_NOACTIVATE Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST Or WS_EX_WINDOWEDGE
            Return Result
        End Get
    End Property

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        'Me.Hide()
        Keyboard.Show()
        Timer1.Enabled = False
    End Sub

    Private Sub Form1_Enter(sender As Object, e As EventArgs) Handles MyBase.Shown
        'Me.Hide()
    End Sub

    Private Sub Form1_Deactivate(sender As Object, e As EventArgs) Handles MyBase.Deactivate

    End Sub

    Private Sub Form1_Paint(sender As Object, e As PaintEventArgs) Handles MyBase.Paint
        Me.Hide()
    End Sub
End Class