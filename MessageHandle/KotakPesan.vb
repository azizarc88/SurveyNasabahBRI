﻿Imports AnimasiUI.Class_Animasi

Public Class KotakPesan
    Public Shared hasil As HasilPesan

    <Flags()> _
    Public Enum HasilPesan
        Ok = 0
        Batal = 1
        Ya = 2
        Tidak = 3
        Ulangi = 4
        Abaikan = 5
    End Enum

    <Flags()> _
    Public Enum TombolPesan
        Ok = 0
        Ok_Batal = 1
        Ya_Tidak = 2
        Ya_Tidak_Batal = 3
        Ulangi_Batal = 4
        Batal_Ulangi_Abaikan = 5
    End Enum

    <Flags()> _
    Public Enum Warna
        Peringatan = 0
        Informasi = 1
        Galat = 2
        Eror = 3
        Sukses = 4
        Pertanyaan = 5
    End Enum

    Public Shared Function AturWarna(ByVal warna As Warna) As Color
        If warna = KotakPesan.Warna.Eror Then
            Return Color.Firebrick
        ElseIf warna = KotakPesan.Warna.Galat Then
            Return Color.Tomato
        ElseIf warna = KotakPesan.Warna.Informasi Then
            Return Color.YellowGreen
        ElseIf warna = KotakPesan.Warna.Peringatan Then
            Return Color.DarkOrange
        ElseIf warna = KotakPesan.Warna.Pertanyaan Then
            Return Color.CadetBlue
        ElseIf warna = KotakPesan.Warna.Sukses Then
            Return Color.LightSkyBlue
        End If
    End Function

    Public Shared Sub AturTombol(ByVal f As Form, ByVal tombol As TombolPesan, ByVal b1 As Button, ByVal b2 As Button, ByVal b3 As Button)
        If tombol = TombolPesan.Batal_Ulangi_Abaikan Then
            b1.Text = "Batal"
            b2.Text = "Ulangi"
            b3.Text = "Abaikan"

            b1.Visible = True
            b2.Visible = True
            b3.Visible = True

            f.AcceptButton = b2
            b2.Focus()
        ElseIf tombol = TombolPesan.Ok Then
            b3.Text = "Ok"

            b3.Visible = True
            f.AcceptButton = b3
            b3.Focus()
        ElseIf tombol = TombolPesan.Ok_Batal Then
            b1.Text = "Ok"
            b3.Text = "Batal"

            b1.Visible = True
            b3.Visible = True
            f.AcceptButton = b1
            b1.Focus()
        ElseIf tombol = TombolPesan.Ulangi_Batal Then
            b1.Text = "Ulangi"
            b3.Text = "Batal"

            b1.Visible = True
            b3.Visible = True
            f.AcceptButton = b1
            b1.Focus()
        ElseIf tombol = TombolPesan.Ya_Tidak Then
            b1.Text = "Ya"
            b3.Text = "Tidak"

            b1.Visible = True
            b3.Visible = True
            f.AcceptButton = b1
            b1.Focus()
        ElseIf tombol = TombolPesan.Ya_Tidak_Batal Then
            b1.Text = "Ya"
            b2.Text = "Tidak"
            b3.Text = "Batal"

            b1.Visible = True
            b2.Visible = True
            b3.Visible = True
            f.AcceptButton = b1
            b1.Focus()
        End If
    End Sub

    Public Shared Function Show(ByVal namaform As Form, ByVal pesan As String) As HasilPesan
        Dim kotakpesan As New M2
        Dim ff As New Form_Fade

        kotakpesan.Text = pesan
        kotakpesan.LPesan.Text = pesan

        ff.Show()
        kotakpesan.ShowDialog()
        ff.Close()
        namaform.Focus()
        Return hasil
    End Function

    Public Shared Function Show(ByVal namaform As Form, ByVal pesan As String, ByVal judul As String, ByVal tombol As TombolPesan, ByVal warna As Warna, Optional ByVal ukuranteks As Integer = 15) As HasilPesan
        Dim kotakpesan As New M1
        Dim ff As New Form_Fade

        AturTombol(kotakpesan, tombol, kotakpesan.B1, kotakpesan.B2, kotakpesan.B3)
        kotakpesan.Text = judul
        kotakpesan.LJudul.Text = judul
        kotakpesan.LPesan.Font = New Font("Century Gothic", ukuranteks)
        kotakpesan.LPesan.Text = pesan
        kotakpesan.Panel1.BackColor = AturWarna(warna)

        ff.Show()
        kotakpesan.ShowDialog()
        ff.Close()
        namaform.Focus()
        Return hasil
    End Function
End Class
