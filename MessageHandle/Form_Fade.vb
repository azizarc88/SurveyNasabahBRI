﻿Imports AnimasiUI.Class_Animasi

Public Class Form_Fade

    Protected Overrides ReadOnly Property CreateParams() _
      As System.Windows.Forms.CreateParams
        Get
            Const WS_EX_TOOLWINDOW As Integer = &H80
            Const WS_EX_TOPMOST As Integer = &H8
            Const WS_EX_WINDOWEDGE As Integer = &H100
            Dim Result As CreateParams
            Result = MyBase.CreateParams
            Result.ExStyle = Result.ExStyle Or WS_EX_TOOLWINDOW Or WS_EX_TOPMOST Or WS_EX_WINDOWEDGE
            Return Result
        End Get
    End Property

    Private Sub Form_Fade_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Animasikan(Me.Handle, 200, GayaAnimasi.Tengah, OpsiGaya.Tutup)
    End Sub

    Private Sub Form_Fade_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.Size = My.Computer.Screen.Bounds.Size
        Me.Location = New Point(0, 0)
        Animasikan(Me.Handle, 150, GayaAnimasi.Tengah, OpsiGaya.Buka)
    End Sub
End Class