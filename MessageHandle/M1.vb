﻿Imports MessageHandle.KotakPesan
Imports AnimasiUI.Class_Animasi

Public Class M1
    Public Function Hasilkan(ByVal b As Button) As TombolPesan
        Dim _hasil As TombolPesan
        If b.Text = "Batal" Then
            _hasil = HasilPesan.Batal
        ElseIf b.Text = "Abaikan" Then
            _hasil = HasilPesan.Abaikan
        ElseIf b.Text = "Ok" Then
            _hasil = HasilPesan.Ok
        ElseIf b.Text = "Tidak" Then
            _hasil = HasilPesan.Tidak
        ElseIf b.Text = "Ulangi" Then
            _hasil = HasilPesan.Ulangi
        ElseIf b.Text = "Ya" Then
            _hasil = HasilPesan.Ya
        End If
        Return _hasil
    End Function

    Private Sub B1_Click(sender As Object, e As EventArgs) Handles B1.Click
        KotakPesan.hasil = Hasilkan(B1)
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Me.Close()
    End Sub

    Private Sub B2_Click(sender As Object, e As EventArgs) Handles B2.Click
        KotakPesan.hasil = Hasilkan(B2)
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Me.Close()
    End Sub

    Private Sub B3_Click(sender As Object, e As EventArgs) Handles B3.Click
        KotakPesan.hasil = Hasilkan(B3)
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Tutup)
        Me.Close()
    End Sub

    Private Sub M1_Load(sender As Object, e As EventArgs) Handles Me.Load
        Me.Focus()
        Animasikan(Me.Handle, 250, GayaAnimasi.Menyatu, OpsiGaya.Buka)
        Me.Focus()
    End Sub
End Class
