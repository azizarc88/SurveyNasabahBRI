﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class M1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(M1))
        Me.LJudul = New System.Windows.Forms.Label()
        Me.B3 = New System.Windows.Forms.Button()
        Me.B1 = New System.Windows.Forms.Button()
        Me.B2 = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.LPesan = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'LJudul
        '
        Me.LJudul.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LJudul.Font = New System.Drawing.Font("Moire", 20.0!)
        Me.LJudul.Location = New System.Drawing.Point(12, 7)
        Me.LJudul.Name = "LJudul"
        Me.LJudul.Size = New System.Drawing.Size(545, 33)
        Me.LJudul.TabIndex = 2
        Me.LJudul.Text = "Judul"
        Me.LJudul.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'B3
        '
        Me.B3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.B3.BackColor = System.Drawing.Color.PowderBlue
        Me.B3.CausesValidation = False
        Me.B3.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.B3.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.B3.FlatAppearance.BorderSize = 2
        Me.B3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.B3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.B3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B3.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.B3.Location = New System.Drawing.Point(435, 242)
        Me.B3.Name = "B3"
        Me.B3.Size = New System.Drawing.Size(122, 37)
        Me.B3.TabIndex = 4
        Me.B3.Text = "Tidak"
        Me.B3.UseVisualStyleBackColor = False
        Me.B3.Visible = False
        '
        'B1
        '
        Me.B1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.B1.BackColor = System.Drawing.Color.PowderBlue
        Me.B1.CausesValidation = False
        Me.B1.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.B1.FlatAppearance.BorderSize = 2
        Me.B1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.B1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.B1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B1.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.B1.Location = New System.Drawing.Point(12, 242)
        Me.B1.Name = "B1"
        Me.B1.Size = New System.Drawing.Size(122, 37)
        Me.B1.TabIndex = 4
        Me.B1.Text = "Ya"
        Me.B1.UseVisualStyleBackColor = False
        Me.B1.Visible = False
        '
        'B2
        '
        Me.B2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.B2.BackColor = System.Drawing.Color.PowderBlue
        Me.B2.CausesValidation = False
        Me.B2.FlatAppearance.BorderColor = System.Drawing.Color.SteelBlue
        Me.B2.FlatAppearance.BorderSize = 2
        Me.B2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DodgerBlue
        Me.B2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.AliceBlue
        Me.B2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.B2.Font = New System.Drawing.Font("Century Gothic", 12.0!)
        Me.B2.Location = New System.Drawing.Point(140, 242)
        Me.B2.Name = "B2"
        Me.B2.Size = New System.Drawing.Size(122, 37)
        Me.B2.TabIndex = 4
        Me.B2.Text = "Tidak"
        Me.B2.UseVisualStyleBackColor = False
        Me.B2.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(-5, -7)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(612, 10)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 12
        Me.PictureBox1.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(-5, 288)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(612, 10)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 12
        Me.PictureBox2.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(58, 44)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(452, 1)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 12
        Me.PictureBox3.TabStop = False
        '
        'LPesan
        '
        Me.LPesan.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LPesan.BackColor = System.Drawing.Color.Transparent
        Me.LPesan.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.LPesan.Font = New System.Drawing.Font("Century Gothic", 15.0!)
        Me.LPesan.Location = New System.Drawing.Point(12, 55)
        Me.LPesan.Name = "LPesan"
        Me.LPesan.Size = New System.Drawing.Size(545, 174)
        Me.LPesan.TabIndex = 13
        Me.LPesan.Text = "Label1"
        Me.LPesan.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.LightSkyBlue
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Location = New System.Drawing.Point(-5, 232)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(585, 59)
        Me.Panel1.TabIndex = 14
        '
        'M1
        '
        Me.AcceptButton = Me.B1
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.AliceBlue
        Me.CancelButton = Me.B3
        Me.ClientSize = New System.Drawing.Size(569, 291)
        Me.ControlBox = False
        Me.Controls.Add(Me.B1)
        Me.Controls.Add(Me.B2)
        Me.Controls.Add(Me.B3)
        Me.Controls.Add(Me.LPesan)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.LJudul)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "M1"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.TopMost = True
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LJudul As System.Windows.Forms.Label
    Friend WithEvents B3 As System.Windows.Forms.Button
    Friend WithEvents B1 As System.Windows.Forms.Button
    Friend WithEvents B2 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents LPesan As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

End Class
