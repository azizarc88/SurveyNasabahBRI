﻿Imports MessageHandle

Public Class Crypt
    Public Shared aman As String

    Shared kar As String() = {"#", ".", "!", "/", "\", "[", "]", "?", "=", "'", ",", ".", "-", ":", " ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", _
              "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", _
               "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", _
               "`", "~", "@", "$", "%", "^", "&", "*", "(", ")", "_", "+", "{", "}", ";", """", "|", "<", ">"}

    Private Shared Function Konversi(karakter As Char, Optional inc As Integer = 3) As Integer
        If aman <> "@matganteng" Then
            KotakPesan.Show(Nothing, "Anda tidak bisa menggunakan librari ini tanpa seizin Aziz Nur Ariffianto :).", "Crypt.dll", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror)
            Exit Function
        End If

        Dim hasil As Integer = 0
        Dim a As Integer = 1

        For Each k As Char In kar
            If karakter = k Then
                hasil = a
                Exit For
            End If
            a += inc
        Next

        Return hasil
    End Function

    Private Shared Function Deversi(nilai As Integer, Optional inc As Integer = 3) As Char
        If aman <> "@matganteng" Then
            KotakPesan.Show(Nothing, "Anda tidak bisa menggunakan librari ini tanpa seizin Aziz Nur Ariffianto :).", "Crypt.dll", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror)
            Exit Function
        End If

        Dim hasil As Char = ""
        Dim a As Integer = 1

        For Each k As Char In kar
            If nilai = a Then
                hasil = k
                Exit For
            End If
            a += inc
        Next

        Return hasil
    End Function

    Public Shared Function Encrypt(teks As String, kunci As String)
        If aman <> "@matganteng" Then
            KotakPesan.Show(Nothing, "Anda tidak bisa menggunakan librari ini tanpa seizin Aziz Nur Ariffianto :).", "Crypt.dll", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror)
            Exit Function
        End If

        Dim k As String = ""
        Dim ki As List(Of Integer) = New List(Of Integer)
        Dim ti As List(Of Integer) = New List(Of Integer)
        Dim hi As List(Of Integer) = New List(Of Integer)
        Dim h As String = ""
        Dim cc As Char() = kunci.ToCharArray
        Dim b As Integer = 0
        Dim v As Integer = 0

        For Each c As Char In teks
            k += cc(b)
            b += 1
            If b = kunci.Length Then
                b = 0
            End If
        Next

        For Each c As String In k
            ki.Add(Konversi(c))
        Next

        For Each c As String In teks
            ti.Add(Konversi(c))
        Next

        Dim i As Integer
        For Each c As Integer In ti
            i = c + ki(v) + 25
            If i > 255 Then
                hi.Add(i Mod 255)
            Else
                hi.Add(i)
            End If
            v += 1
        Next

        For Each c As Integer In hi
            h += Chr(c)
        Next
        Return h
    End Function

    Public Shared Function Decrypt(teks As String, kunci As String)
        If aman <> "@matganteng" Then
            KotakPesan.Show(Nothing, "Anda tidak bisa menggunakan librari ini tanpa seizin Aziz Nur Ariffianto :).", "Crypt.dll", KotakPesan.TombolPesan.Ok, KotakPesan.Warna.Eror)
            Exit Function
        End If

        Dim k As String = ""
        Dim ki As List(Of Integer) = New List(Of Integer)
        Dim ti As List(Of Integer) = New List(Of Integer)
        Dim hi As List(Of Integer) = New List(Of Integer)
        Dim h As String = ""
        Dim cc As Char() = kunci.ToCharArray
        Dim b As Integer = 0
        Dim v As Integer = 0

        For Each c As Char In teks
            k += cc(b)
            b += 1
            If b = kunci.Length Then
                b = 0
            End If
        Next

        For Each c As String In k
            ki.Add(Konversi(c))
        Next

        For Each c As String In teks
            ti.Add(Asc(c))
        Next

        Dim i As Integer
        For Each c As Integer In ti
            i = c - ki(v) - 25
            If i < 0 Then
                hi.Add(i + 255)
            Else
                hi.Add(i)
            End If
            v += 1
        Next

        For Each c As Integer In hi
            h += Deversi(c)
        Next
        Return h
    End Function
End Class
