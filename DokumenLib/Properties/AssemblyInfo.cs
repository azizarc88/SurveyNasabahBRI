﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("DokumenLib")]
[assembly: AssemblyDescription("File library pembuatan dokumen report")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Aziz Nur Ariffianto")]
[assembly: AssemblyProduct("DokumenLib")]
[assembly: AssemblyCopyright("Copyright © 2013 - 2015 Aziz Nur Ariffianto")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("640e0969-5726-48bd-9855-ef80f796f2f1")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("4.2.34.58")]
[assembly: AssemblyFileVersion("4.2.34.58")]
